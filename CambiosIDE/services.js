angular.module('IDE.services', [])
.factory('GetFiles', function($http) {

    var obtainfiles = function(exp,gpo,tarea) {
        return $http.get('http://localhost:8000/api/getproject/'+exp+'/'+gpo+'/'+tarea).then(function(response){
        	var resp =response.data.records;
       			return String(resp[0].project);
   			}, function(response){
   				var resp = "Ocurrio un problema"
       			return resp;
   			});    
    };
     var obtaincode = function(exp,gpo,tarea,filename) {
        return $http.get('http://localhost:8000/api/getfile/'+exp+'/'+gpo+'/'+tarea+'/'+filename).then(function(response){
        	var resp =response.data.records;
        	return String(resp[0].code);
   			}, function(response){
   				var resp = "Ocurrio un problema"
       			return resp;
   			});    
    };

     var createfile = function(exp,gpo,tarea,filename) {
        return $http.get('http://localhost:8000/api/createfile/'+exp+'/'+gpo+'/'+tarea+'/'+filename).then(function(response){
        		return response.data;
   			}, function(response){
       			return response.data;
   			});    
    };
    var deletefile = function(exp,gpo,tarea,filename) {
        return $http.get('http://localhost:8000/api/deletefile/'+exp+'/'+gpo+'/'+tarea+'/'+filename).then(function(response){
        		return response.data;
   			}, function(response){
       			return response.data;
   			});    
    };
    var renamefile = function(exp,gpo,tarea,filename,newFileName) {
        return $http.get('http://localhost:8000/api/renamefile/'+exp+'/'+gpo+'/'+tarea+'/'+filename+'/'+newFileName).then(function(response){
        		return response.data;
   			}, function(response){
       			return response.data;
   			});    
    };



    return { obtainfiles: obtainfiles,  obtaincode:obtaincode, createfile: createfile, deletefile: deletefile, renamefile: renamefile};
})
.factory('Metricas',function () 
{ 

    var data = 
    {
        ContadorOperadoresDistintos : 0,
        ContadorOperandosDistintos : 0,
        SumaContadorOperadores : 0,
        SumaContadorOperandos : 0,
        vocabulario : 0,
        longitud : 0,
        volumen : 0,
        volumenPotencial : 0,
        nivel : 0,
        dificultad : 0,
        inteligencia : 0,
        esfuerzo : 0,
        tiempoTotalProduccion : 0,
        tiempoPruebas : 0,
        tiempoCorreciones : 0,
        tiempoTotalProducto : 0,
        LOCbaseOriginal : 0,
        LOCagregado : 0,
        LOCmodificado : 0,
        LOCsuprimido : 0,
        //locReutilizado : 0,
        //nuevaReutilizacion : 0,
        LocNuevoCambiante : 0,
        LOCbaseActual : 0,
        defectos : 0,
        defectosCorregidos : 0,
        defectosNoCorregidos : 0,
        defectosPorTamano : 0,
        defectosPorHora : 0,
        defectosCorregidosHora : 0,
        defectosPorHoraEnPruebas : 0,
        defectoscorregidosTotalCorrecciones : 0,
        densidadDeDefectos : 0,
        ProyeccionFuturosDefectos : 0,
        complejidadCiclo : 0,
    };

    return {
        getContadorOperadoresDistintos: function () 
        {
            return data.ContadorOperadoresDistintos;
        },
        setContadorOperadoresDistintos: function (newValue) 
        {
            data.ContadorOperadoresDistintos = newValue;
        },
         getContadorOperandosDistintos: function () 
        {
            return data.ContadorOperandosDistintos;
        },
        setContadorOperandosDistintos: function (newValue) 
        {
            data.ContadorOperandosDistintos = newValue;
        },
        getSumaContadorOperadores: function () 
        {
            return data.SumaContadorOperadores;
        },
        setSumaContadorOperadores: function (newValue) 
        {
            data.SumaContadorOperadores = newValue;
        },
        getSumaContadorOperandos: function () 
        {
            return data.SumaContadorOperandos;
        },
        setSumaContadorOperandos: function (newValue) 
        {
            data.SumaContadorOperandos = newValue;
        },
         getVocabulario: function () 
        {
            return data.vocabulario;
        },
        setVocabulario: function (newValue) 
        {
            data.vocabulario = newValue;
        },
        getLongitud: function () 
        {
            return data.longitud;
        },
        setLongitud: function (newValue) 
        {
            data.longitud = newValue;
        },
         getVolumen: function () 
        {
            return data.volumen;
        },
        setVolumen: function (newValue) 
        {
            data.volumen = newValue;
        },
         getVolumenPotencial: function () 
        {
            return data.volumenPotencial;
        },
        setVolumenPotencial: function (newValue) 
        {
            data.volumenPotencial = newValue;
        },
         getNivel: function () 
        {
            return data.nivel;
        },
        setNivel: function (newValue) 
        {
            data.nivel = newValue;
        },
         getDificultad: function () 
        {
            return data.dificultad;
        },
        setDificultad: function (newValue) 
        {
            data.dificultad = newValue;
        },
        getInteligencia: function () 
        {
            return data.inteligencia;
        },
        setInteligencia: function (newValue) 
        {
            data.inteligencia = newValue;
        },
        getEsfuerzo: function () 
        {
            return data.esfuerzo;
        },
        setEsfuerzo: function (newValue) 
        {
            data.esfuerzo = newValue;
        },
        getTiempoTotalProduccion: function () 
        {
            return data.tiempoTotalProduccion;
        },
        setTiempoTotalProduccion: function (newValue) 
        {
            data.tiempoTotalProduccion = newValue;
        },
        getTiempoPruebas: function () 
        {
            return data.tiempoPruebas;
        },
        setTiempoPruebas: function (newValue) 
        {
            data.tiempoPruebas = newValue;
        },
        getTiempoCorreciones: function () 
        {
            return data.tiempoCorreciones;
        },
        setTiempoCorreciones: function (newValue) 
        {
            data.tiempoCorreciones = newValue;
        },
        getTiempoTotalProducto: function () 
        {
            return data.tiempoTotalProducto;
        },
        setTiempoTotalProducto: function (newValue) 
        {
            data.tiempoTotalProducto = newValue;
        },
        getLOCbaseOriginal: function () 
        {
            return data.LOCbaseOriginal;
        },
        setLOCbaseOriginal: function (newValue) 
        {
            data.LOCbaseOriginal = newValue;
        },
        getLOCagregado: function () 
        {
            return data.LOCagregado;
        },
        setLOCagregado: function (newValue) 
        {
            data.LOCagregado = newValue;
        },
        getLOCmodificado: function () 
        {
            return data.LOCmodificado;
        },
        setLOCmodificado: function (newValue) 
        {
            data.LOCmodificado = newValue;
        },
        getLOCsuprimido: function () 
        {
            return data.LOCsuprimido;
        },
        setLOCsuprimido: function (newValue) 
        {
            data.LOCsuprimido = newValue;
        },
        /*
        getLocReutilizado: function () 
        {
            return data.locReutilizado;
        },
        setLocReutilizado: function (newValue) 
        {
            data.locReutilizado = newValue;
        },
        getNuevaReutilizacion: function () 
        {
            return data.nuevaReutilizacion;
        },
        setNuevaReutilizacion: function (newValue) 
        {
            data.nuevaReutilizacion = newValue;
        },
        */
        getLocNuevoCambiante: function () 
        {
            return data.LocNuevoCambiante;
        },
        setLocNuevoCambiante: function (newValue) 
        {
            data.LocNuevoCambiante = newValue;
        },
        getLOCbaseActual: function () 
        {
            return data.LOCbaseActual;
        },
        setLOCbaseActual: function (newValue) 
        {
            data.LOCbaseActual = newValue;
        },
        getDefectos: function () 
        {
            return data.defectos;
        },
        setDefectos: function (newValue) 
        {
            data.defectos = newValue;
        },
        getDefectosCorregidos: function () 
        {
            return data.defectosCorregidos;
        },
        setDefectosCorregidos: function (newValue) 
        {
            data.defectosCorregidos = newValue;
        },
        getDefectosNoCorregidos: function () 
        {
            return data.defectosNoCorregidos;
        },
        setDefectosNoCorregidos: function (newValue) 
        {
            data.defectosNoCorregidos = newValue;
        },
        getDefectosPorTamano: function () 
        {
            return data.defectosPorTamano;
        },
        setDefectosPorTamano: function (newValue) 
        {
            data.defectosPorTamano = newValue;
        },
        getDefectosPorHora: function () 
        {
            return data.defectosPorHora;
        },
        setDefectosPorHora: function (newValue) 
        {
            data.defectosPorHora = newValue;
        },
        getDefectosCorregidosHora: function () 
        {
            return data.defectosCorregidosHora;
        },
        setDefectosCorregidosHora: function (newValue) 
        {
            data.defectosCorregidosHora = newValue;
        },
        getDefectosPorHoraEnPruebas: function () 
        {
            return data.defectosPorHoraEnPruebas;
        },
        setDefectosPorHoraEnPruebas: function (newValue) 
        {
            data.defectosPorHoraEnPruebas = newValue;
        },
        getDefectoscorregidosTotalCorrecciones: function () 
        {
            return data.defectoscorregidosTotalCorrecciones;
        },
        setDefectoscorregidosTotalCorrecciones: function (newValue) 
        {
            data.defectoscorregidosTotalCorrecciones = newValue;
        },
        getDensidadDeDefectos: function () 
        {
            return data.densidadDeDefectos;
        },
        setDensidadDeDefectos: function (newValue) 
        {
            data.densidadDeDefectos = newValue;
        },
        getProyeccionFuturosDefectos: function () 
        {
            return data.ProyeccionFuturosDefectos;
        },
        setProyeccionFuturosDefectos: function (newValue) 
        {
            data.ProyeccionFuturosDefectos = newValue;
        },
        getComplejidadCiclo: function () 
        {
            return data.complejidadCiclo;
        },
        setComplejidadCiclo: function (newValue) 
        {
            data.complejidadCiclo = newValue;
        },
    };
})