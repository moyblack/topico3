angular.module('IDE.contIde', [])

.controller('ctrlIDE', function($scope, $http, $timeout, GetFiles, Metricas, $interval,) 
{
  
   $scope.Acethemes = ["chrome","clouds","crimson_editor","dawn","dreamweaver","eclipse","github","solarized_light","textmate","tomorrow","xcode",
   "kuroir","katzenmilch","ambiance","chaos","clouds_midnight","cobalt","idle_fingers","kr_theme","merbivore","merbivore_soft","mono_industrial",
    "monokai","pastel_on_dark","solarized_dark","terminal","tomorrow_night","tomorrow_night_blue","tomorrow_night_bright","tomorrow_night_eighties","twilight",
    "vibrant_ink",
  ];
  $scope.themes = [{id:0, theme:"chrome"},{id:1, theme:"clouds"},{id:2, theme:"crimson_editor"},{id:3, theme:"dawn"},{id:4, theme:"dreamweaver"},
    {id:5, theme:"eclipse"},{id:6, theme:"github"},{id:7, theme:"solarized_light"},{id:8, theme:"textmate"},{id:9, theme:"tomorrow"},{id:10, theme:"xcode"},
    {id:11, theme:"kuroir"},{id:12, theme:"katzenmilch"},{id:13, theme:"ambiance"},{id:14, theme:"chaos"},{id:15, theme:"cobalt"},{id:16, theme:"idle_fingers"},
    {id:17, theme:"kr_theme"},{id:18, theme:"merbivore"},{id:19, theme:"merbivore_soft"},{id:20, theme:"mono_industrial"},{id:21, theme:"clouds_midnight"},
    {id:22, theme:"monokai"},{id:23, theme:"pastel_on_dark"},{id:24, theme:"solarized_dark"},{id:25, theme:"terminal"},{id:26, theme:"tomorrow_night"},
    {id:27, theme:"tomorrow_night_blue"},{id:28, theme:"tomorrow_night_bright"},{id:29, theme:"tomorrow_night_eighties"},{id:30, theme:"twilight"},
    {id:31, theme:"vibrant_ink"},
  ];
    $scope.startT = 0;
    $interval(function () {
        $scope.startT = $scope.startT++;
        Metricas.setTiempoTotalProduccion($scope.startT);
    }, 60000);
  
    //cosas que necesito
    $scope.currentTheme = 14;
    $scope.usuario = "Leo";
    $scope.exp = "246524";

    
    $scope.operandos = []; // -> arrayOperandos
    $scope.operandos2 = []; // -> arrayOperandos
    var _session = null;

    $scope.tokenInformation = "";
    $scope.aceLoaded = function (_editor) 

    {
     var _session = _editor.getSession();
    var _renderer = _editor.renderer;

    // Options
     _editor.$blockScrolling = Infinity;
    _editor.setShowPrintMargin(false);
    // Events
     _editor.on('mousedown', function(e) {
      var row = e.getDocumentPosition().row;
      var col = e.getDocumentPosition().column;
      var token = _editor.session.getTokenAt(row, col);
     

    });

     //detecta cualquier cambio en el editor
     _editor.on("change", function(e) 
     {
       $scope.uptdateCodigo(String(_editor.session.getValue()));
       Metricas.setLOCbaseActual((_editor.session.getLength()));
      var codigo = _editor.getValue();
      var currline = _editor.getSelectionRange().start.row;
      var lineaActual = _editor.session.getLine(currline);
      //////////////////////////////////////////////////////////////////////////////
      /*** 
        Contador de operaciones
      */
      $scope.aCO = {}; // -> arrayContadorOperadores

      //guarda las variables
      lineaActual = lineaActual.replace(/^\s+|\s+$/g, "");
      if(lineaActual.charAt(0).trim() != "#")
      { // si no es un comentario
        if(~lineaActual.indexOf("=") && !~lineaActual.indexOf("=="))
        { // detecta la asignación de un valor
          var variable = lineaActual.split("=")[0].trim(); // obtiene todo lo que está antes de un signo de igual
          var varTemp = variable.split(" ", -1); //-> obtiene cualquier cosa antes de un signo de igual
          varTemp = varTemp.pop();
          var a = $scope.operandos.indexOf(varTemp);  //busca la variable en el arreglo existente
            $scope.operandos2.push(varTemp);
            if(!~a && varTemp != "")                 // si no existe aún la variable, la agrega
                $scope.operandos.push(varTemp);
        }else{
          //console.log("No variables found");
        }
      }
      var contador;
      //esto busca las veces que se ha usado una variable guardada y las cuenta
      var aux = "";
      var cantidadOperandos = 0;
      $scope.ContadorOperandos = []; 

      for(i = 0; i < $scope.operandos.length; i++ ){
        aux = $scope.operandos[i];
        cantidadOperandos = countInstances(codigo, aux);
        $scope.ContadorOperandos.push(cantidadOperandos);
      }

        //enter
        var key = e.which;
        var text = String.fromCharCode(key);
        switch(key) 
        {
            case 13: //Tab
                console.log('equisdedededede'); // manually add tab
                //Code to store this event for replay later
                break;
        }
      $scope.aCO['ContadorFindMas']        = countInstances(codigo,"+");
      $scope.aCO['ContadorFindMenos']      = countInstances(codigo,"-");
      $scope.aCO['ContadorFindPor']        = countInstances(codigo,"*");
      $scope.aCO['ContadorFindEntre']      = countInstances(codigo,"/");
      $scope.aCO['ContadorFindPotencia']   = countInstances(codigo,"**");
      $scope.aCO['ContadorFindModulo']     = countInstances(codigo,"%");
      
      $scope.aCO['ContadorFindOR']         = countInstances(codigo," or ");
      $scope.aCO['ContadorFindAND']        = countInstances(codigo," and ");
      $scope.aCO['ContadorFindNOT']        = countInstances(codigo," not ");
      
      $scope.aCO['ContadorFindCompara']    = countInstances(codigo,"==");
      $scope.aCO['ContadorFindAsigna']     = countInstances(codigo,"=");
      $scope.aCO['ContadorFindMayor']      = countInstances(codigo,">");
      $scope.aCO['ContadorFindMenor']      = countInstances(codigo,"<");
      $scope.aCO['ContadorFindDiferente']  = countInstances(codigo,"!=");
      $scope.aCO['ContadorFindMayorIgual'] = countInstances(codigo,">=");
      $scope.aCO['ContadorFindMenorIgual'] = countInstances(codigo,"<=");

      imprimir();

      function countInstances(str, value) { 
        return (str.split(value).length - 1);
      }
      
      // tiempo transcurrido
       Metricas.setTiempoTotalProduccion($scope.startT);
      
      //Complejidad Ciclomática
      if(lineaActual.charAt(0).trim() != "#")
      {
        var complejidadIf    = countInstances(codigo, "if");
        var complejidadElse  = countInstances(codigo, "else");
        var complejidadFor   = countInstances(codigo, "for");
        var complejidadWhile = countInstances(codigo, "while");
      }
      $scope.complejidadCiclo = complejidadIf + complejidadWhile + complejidadElse + complejidadFor;


      
      //los if son para evitar divisiones entre 0
      $scope.defectosNoCorregidos = $scope.defectos - $scope.defectosCorregidos;
      if(Metricas.getLOCbaseActual() != 0){
        $scope.defectosPorTamano = $scope.defectos / Metricas.getLOCbaseActual();
        $scope.densidadDeDefectos = 1000 * $scope.defectos / Metricas.getLOCbaseActual(); 
      }
      
      if($scope.elapsed + $scope.elapsedOriginal != 0)
        $scope.defectosPorHora = $scope.defectos/($scope.elapsed + $scope.elapsedOriginal) * 60;
      
      if($scope.elapsed + $scope.elapsedOriginal != 0)
        $scope.defectosCorregidosHora = $scope.defectosCorregidos/($scope.elapsed + $scope.elapsedOriginal) *60;
      
      if($scope.tiempoPruebas != 0)
        $scope.defectosPorHoraEnPruebas = $scope.defectos/$scope.tiempoPruebas*60;
      
      if($scope.tiempoCorreciones != 0)
        $scope.defectosCorregidosEnCorrecciones = $scope.defectosCorregidos/$scope.tiempoCorreciones*60;


      $scope.tiempoTotalProducto =  ($scope.elapsed + $scope.elapsedOriginal) + $scope.tiempoPruebas + $scope.tiempoCorreciones;


    
    
      

        function imprimir()
        {
          //reset de valores 
          Metricas.setSumaContadorOperadores(0);
          Metricas.setContadorOperadoresDistintos(0);
          Metricas.setSumaContadorOperandos(0);
          Metricas.setContadorOperandosDistintos(0);
          Metricas.setVocabulario(0);
          Metricas.setLongitud(0);
          Metricas.setVolumen(0);

          if($scope.aCO['ContadorFindCompara']   >=1) $scope.aCO['ContadorFindAsigna'] -= 2*$scope.aCO['ContadorFindCompara'];
          if($scope.aCO['ContadorFindDiferente'] >=1) $scope.aCO['ContadorFindAsigna'] -= $scope.aCO['ContadorFindDiferente'];
          if($scope.aCO['ContadorFindPotencia']  >=1) $scope.aCO['ContadorFindPor']    -= 2*$scope.aCO['ContadorFindPotencia'];
          if($scope.aCO['ContadorFindMayorIgual']>=1){$scope.aCO['ContadorFindAsigna'] -= $scope.aCO['ContadorFindMayorIgual'];
                                               $scope.aCO['ContadorFindMayor']  -= $scope.aCO['ContadorFindMayorIgual'];}
          if($scope.aCO['ContadorFindMenorIgual']>=1){$scope.aCO['ContadorFindAsigna'] -= $scope.aCO['ContadorFindMenorIgual'];
                                               $scope.aCO['ContadorFindMenor']  -= $scope.aCO['ContadorFindMenorIgual'];}

          if(Metricas.getLOCbaseOriginal()== Metricas.getLOCbaseActual()){
            Metricas.setLOCagregado(0);
            Metricas.setLOCsuprimido(0);
          }else  if(Metricas.getLOCbaseOriginal()< Metricas.getLOCbaseActual())
              Metricas.setLOCagregado((Metricas.getLOCbaseActual() - Metricas.getLOCbaseOriginal()));
            else
              Metricas.setLOCsuprimido((Metricas.getLOCbaseOriginal() - Metricas.getLOCbaseActual()));
          for( var contador in $scope.aCO ){
              Metricas.setSumaContadorOperadores((Metricas.getSumaContadorOperadores() + $scope.aCO[contador]));
              if($scope.aCO[contador] != 0) Metricas.setContadorOperadoresDistintos((Metricas.getContadorOperadoresDistintos()+ 1));
          }

          for(i = 0; i < $scope.ContadorOperandos.length; i++ ){
            Metricas.setSumaContadorOperandos((Metricas.getSumaContadorOperandos() + $scope.ContadorOperandos[i]));
            if($scope.ContadorOperandos[i] != 0) Metricas.setContadorOperandosDistintos((Metricas.getContadorOperandosDistintos()+ 1));
          }

          
          Metricas.setVocabulario(obtenerVocabulario());
          Metricas.setLongitud(obtenerLongitud());
          Metricas.setVolumen(obtenerVolumen());
          Metricas.setVolumenPotencial(obtenerVolumenPotencial());
          Metricas.setNivel(obtenerNivel());
          Metricas.setDificultad(obtenerDificultad());
          Metricas.setInteligencia(obtenerInteligencia());
          Metricas.setEsfuerzo(obtenerEsfuerzo());
          Metrica.setLOCTotal(Metricas.getLOCbaseOriginal() - Metricas.getLOCsuprimido());


          


          console.log("Total de operadores  = " +$scope.SumaContadorOperadores );     //n2
          console.log("Operadores distintos = " +$scope.ContadorOperadoresDistintos );//n1
          console.log("Total de operandos   = " +$scope.SumaContadorOperandos );      //N2
          console.log("Operandos distintos  = " +$scope.ContadorOperandosDistintos ); //N1

          /*console.log("LOC baseOriginal: " + $scope.LOCbaseOriginal);
          console.log("LOC baseActual:   " + $scope.LOCbaseActual);
          console.log("LOC agregado:     " + $scope.LOCagregado);
          console.log("LOC suprimido:    " + $scope.LOCsuprimido);*/
          /*
          console.log("Vocabulario          = " +$scope.vocabulario);
          console.log("longitud             = " +$scope.longitud);
          console.log("volumen              = " +$scope.volumen);
          console.log("volumen potencial    = " +$scope.volumenPotencial);
          console.log("Nivel                = " +$scope.nivel);
          console.log("Dificultad           = " +$scope.dificultad);
          console.log("Inteligencia         = " +$scope.inteligencia);
          console.log("Esfuerzo             = " +$scope.esfuerzo);

          */

  
        }



        //Vocabulario del programa 
        // Número de los diferentes operandos y operadores usados para construir un programa
        // n1 + N1
        function obtenerVocabulario(){
          return Metricas.getContadorOperadoresDistintos() + Metricas.getContadorOperandosDistintos();
        }
        
        //Logitud del programa
        //La longitud en términos del número total de operadores y operandos utilizados durante la implementación 
        // n2 + N2
        function obtenerLongitud(){  
          return Metricas.getSumaContadorOperadores() + Metricas.getSumaContadorOperandos();
        }

        //Volumen del algoritmo
        //Calcula el volumen de un algoritmo.
        // V = N log2 n -> N: longitud, n: vocabulario
        function obtenerVolumen(){
          return $scope.longitud * ( Math.log($scope.vocabulario) / Math.log(2)); // 16.25 con N=7= n=5
        }

        //Volumen potencial 
        //Volumen minimo potencial para un programa. 
        // V = (n1 + n2) log 2 (n1 +n2) 
          //n1: mimnimo potencial de operadores          -> número de diferentes operadores????
          //n2: diferentes parámetros de entrada/salida  -> ???s

        function obtenerVolumenPotencial(){
          return (($scope.ContadorOperadoresDistintos + $scope.SumaContadorOperandos)) * ( Math.log($scope.ContadorOperadoresDistintos + $scope.SumaContadorOperandos) / Math.log(2)); // 16.25 con N=7= n=5
          //return $scope.ContadorOperadoresDistintos + $scope.SumaContadorOperandos;
        }

        //Nivel de programa
        //Da una idea del nivel de detalle con que ha sido codificado.
        //Nivel =(2/n1 )(n2/𝑁_2) -> n1: #operadores distintos, n2: #operandos distintos, N2: total de operandos
        function obtenerNivel(){
          return  (2 / $scope.ContadorOperadoresDistintos) * ($scope.ContadorOperandosDistintos / $scope.SumaContadorOperandos);
        }

        //Dificultad del programa
        //Lo inverso al nivel de programa se le conoce como dificultad de programa.
        //D=1/L -> L: nivel de programa
        function obtenerDificultad(){
          return (1 / $scope.nivel);
        }

        //Inteligencia contenida en el programa
        //Permite estimar la complejidad desde el punto de vista del tiempo de programación y la depuración.
        // I = LV -> L: nivel, V: volumen
        function obtenerInteligencia(){
          return $scope.nivel * $scope.volumen;
        }

        //Esfuerzo
        //E puede ser usado como una medida de la claridad del programa.
        //E=V/L -> V: volumen, L: nivel
        function obtenerEsfuerzo(){
          return $scope.volumen / $scope.nivel;
        }

      });
    }
  
  $scope.aceChanged = function (_editor) {

  }

  var editor = function() {
    this.theme = $scope.Acethemes[$scope.currentTheme];
    this.mode = 'python';
    this.opacity = 100;
    this.useWrapMode = true;
    this.gutter = true;
    this.splitMode = false;
    this.fontS =16;
    
    this.snippets = true;
    this.bAuto = false;
    this.lAuto = true;
    this.minL = Infinity;
    this.hihlightAL = true;
    this.behaveE = true;
    $scope.showToken = true;

  };
  $scope.editor = new editor();


  $scope.actTheme = function()
  {
    $scope.editor.theme = $scope.Acethemes[$scope.selectedTheme.id];
  }

  $scope.split = function()
  {
    if ($scope.editor.splitMode)
    {
        $scope.editor.splitMode = false;
    }
    else
    {
        $scope.editor.splitMode = true;
    }
    
  }

  $scope.letraG = function()
  {
    $scope.editor.fontS = 24;
  }

  $scope.letraN = function()
  {
    $scope.editor.fontS = 16;
  }

  $scope.letraC =  function()
  {
    $scope.editor.fontS = 10;
  }
   $scope.STInfo = function(tokenInfo)
   {
     $scope.$apply(function () {
            $scope.tokenInformation = tokenInfo;
        });  
   }

  $scope.saveCode = function()  
  {
    var code = $scope.codigo;
    alert(code);
    var variablesToSend = {c: $scope.codigo};
    $http.post('http://localhost:8000/api/savecode/'+$scope.exp+'/'+$scope.gpo+'/'+$scope.tarea+'/'+ String($scope.files[$scope.tab - 1]), variablesToSend ).then(function(response){
       console.log(response);
      sweetAlert("Exito", "Codigo Guardado", "success");
   }, function(response){
      console.log(response);
      sweetAlert("Error", "Ocurrio un problema", "error");
   });
    }

    $scope.saveMetricas = function()  
  {
    var variablesToSend = {
                            met1: $scope.met1 =1,
                            met2: $scope.met2 =1,
                            met3: $scope.met3=1,
                            met4: $scope.met4=1,
                            met5: $scope.met5=1,
                            met6: $scope.met6=1,
                            met7: $scope.met7=1,
                            met8: $scope.met8=1,
                            met9: $scope.met9=1,
                            met10: $scope.met10=1,
                            met11: $scope.met11=1,
                            met12: $scope.met12=1,
                            met13: $scope.met13=1,
                            met14: $scope.met14=1,
                            met15: $scope.met15=1,
                            met16: $scope.met16=1,
                            met17: $scope.met17=1,
                            met18: $scope.met18=1,
                            met19: $scope.met19=1,
                            met20: $scope.met20=1,
                            met21: $scope.met21=1,
                            met22: $scope.met22=1,
                            met23: $scope.met23=1,
                            met24: $scope.met24=1,
                            met25: $scope.met25=1,
                            met26: $scope.met26=1,
                            met27: $scope.met27=1,
                            met28: $scope.met28=1,
                            met29: $scope.met29=1,
                            met30: $scope.met30=1,
                            met31: $scope.met31=1,
                            met32: $scope.met32=1,
                            met33: $scope.met33=1,
                            met34: $scope.met34=1,
                          };

    $http.post('http://localhost:8000/ide/insertmetricas/'+$scope.exp+'/'+$scope.gpo+'/'+$scope.tarea, variablesToSend ).then(function(response){
       console.log(response);
      sweetAlert("Exito", "Variables Guardadas", "success");
   }, function(response){
      console.log(response);
      sweetAlert("Error", "Ocurrio un problema", "error");
   });
    }

    $scope.init = function()
    {  
        $scope.exp = 1;
        $scope.gpo = 1234
        $scope.tarea = 37;
        $scope.getFiles();
        $scope.blockedit =true;
        $scope.puedeeditar = true;
        $scope.shwAdmin = false;
        $scope.currentTheme = 14;

    }
    $scope.getFiles = function()
    {
        var recieved_data = GetFiles.obtainfiles($scope.exp,$scope.gpo, $scope.tarea);

        recieved_data.then(
            function(fulfillment){
                if(String(fulfillment) != "Ocurrio un problema")
                        {
                            var files = fulfillment;
                            $scope.files = files.split(",");
                            $scope.numFiles = $scope.files.length;
                            $scope.tab=1;
                            $scope.getCodes();
                        }
                        else
                        {
                            sweetAlert("Aviso", String(fulfillment), "error");
                        }
            }, function(){
                 sweetAlert("Error", "Ocurrio un problema", "error");
        });
    }
    $scope.getCodes = function()
    {
        var recieved_code = GetFiles.obtaincode($scope.exp,$scope.gpo, $scope.tarea, String($scope.files[$scope.tab - 1]));
                recieved_code.then(
                    function(fulfillment){
                        if(String(fulfillment) != "Ocurrio un problema")
                        {
                            $scope.codigo = fulfillment;
                            var lines = $scope.codigo.split("\n");
                            Metricas.setLOCbaseOriginal((Metricas.getLOCbaseOriginal() + lines.length));
                        }
                        else
                        {
                            sweetAlert("Aviso", String(fulfillment), "error");
                        }
                        
                    });
    }
    $scope.changeTab = function (value)
    {
        $scope.tab = value;
        var recieved_code = GetFiles.obtaincode($scope.exp,$scope.gpo, $scope.tarea, String($scope.files[$scope.tab - 1]));
                recieved_code.then(
                    function(fulfillment){
                            $scope.codigo = fulfillment;
                    });
    }
    $scope.uptdateCodigo = function (code)
    {
        $scope.codigo = code;
    }
    $scope.nuevoArchivo = function(filename)
    {
        var recieved_data = GetFiles.createfile($scope.exp,$scope.gpo, $scope.tarea, filename);
                recieved_data.then(
                    function(fulfillment){
                        if(String(fulfillment) != "Archivo Creado")
                        {
                           sweetAlert("Aviso", String(fulfillment), "error");
                        }
                        else
                        {
                            $scope.getFiles();
                        }
                        
                    });

        $scope.newFileName ='';
    }
    $scope.eliminarArchivo = function(filename)
    {
        var recieved_data = GetFiles.deletefile($scope.exp,$scope.gpo, $scope.tarea, filename);
                recieved_data.then(
                    function(fulfillment){
                        if(String(fulfillment) != "Archivo Borrado")
                        {
                           sweetAlert("Aviso", String(fulfillment), "error");
                        }
                        else
                        {
                            $scope.getFiles();
                        }
                        
                    });
    }
    $scope.editarNombre = function(fileName)
    {
        $scope.tempFilename = fileName;
               alert($scope.tempFilename);
    }
    $scope.guardarNombre = function(fileName)
    {
       var recieved_data = GetFiles.renamefile($scope.exp,$scope.gpo, $scope.tarea,  $scope.tempFilename,fileName);

                recieved_data.then(
                    function(fulfillment){
                        if(String(fulfillment) != "Nombre de Archivo Cambiado")
                        {
                           sweetAlert("Aviso", String(fulfillment), "error");
                        }
                        else
                        {
                            $scope.getFiles();
                        }
                        
                    });
        $scope.tempFilename ='';
    }
    $scope.showAdmin = function()
    {
        $scope.shwAdmin = true;
    }
    $scope.SalirAdminProyecto = function()
    {
        $scope.shwAdmin = false;
    }

});