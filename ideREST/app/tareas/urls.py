"""ide URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from app.tareas import views
urlpatterns = [
    url(r'^tareas/getTareasByAlumno$', views.get_tareas_by_alumno, name="get_tareas_by_alumno"),
    url(r'^tareas/getAllTareasMaestro$', views.getAllTareasMaestro, name="getAllTareasMaestro"),
    url(r'^tareas/detalles_tarea/(?P<id_tarea>[0-9]+)/(?P<id_grupo>[0-9]+)/(?P<tipo>[\w\-]+)$', views.detalles_tarea, name="detalles_tarea"),
    url(r'^tareas/getAllTareas$', views.getAllTareas, name="tareas_getAllTareas"),
    url(r'^tareas/insertTarea/(?P<id_grupo>[0-9]+)$', views.insertTarea, name="tareas_insertTarea"),
    url(r'^tareas/deleteTarea/(?P<id_grupo>[0-9]+)/(?P<id_tarea>[0-9]+)$', views.deleteTarea, name="tareas_deleteTarea"),
    url(r'^tareas/get_tareas_alumno_grupo/(?P<gpo>[0-9]+)$', views.get_tareas_alumno_grupo, name="get_tareas_alumno_grupo"),
    url(r'^tareas/get_tareas_grupo/(?P<id_grupo>[0-9]+)$', views.get_tareas_grupo, name="get_tareas_grupo"),
    url(r'^tareas/calificarTarea/(?P<id_grupo>[0-9]+)/(?P<id_tarea>[0-9]+)/(?P<id_alumno>[0-9]+)$', views.calificarTarea, name="tareas_calificarTarea"),
    url(r'^tareas/entregarTarea$', views.entregarTarea, name="tareas_entregaTarea"),
    url(r'^tareas/get_detalles_entrega/(?P<id_grupo>[0-9]+)/(?P<id_tarea>[0-9]+)$', views.get_detalles_entrega, name="tareas_get_detalles_entrega"),
    url(r'^tareas/configurarIde$', views.configurarIde, name="tareas_configurarIde"),
    
    url(r'^tareas/modificarTarea/(?P<id_grupo>[0-9]+)/(?P<id_tarea>[0-9]+)$', views.modificarTarea, name="tareas_modificarTarea"),
    
    url(r'^tareas/getConfigIde$', views.getConfigIde, name="tareas_getConfigIde"),
    
    url(r'^tareas/modificarIde$', views.modificarIde, name="tareas_modificarIde"),
    
    url(r'^tareas/getTarea$', views.getTarea, name="tareas_getTarea"),
    url(r'^tareas/removeArchivo$', views.removeArchivo, name="tareas_removeArchivo"),
    url(r'^metricas/getAllMetricas/(?P<tipo>[\w\-]+)/(?P<id_tipo>[0-9]+)/(?P<tipoMetrica>[\w\-]+)$', views.getAllMetricas, name="getAllMetricas"),
    url(r'^metricas/getDetallesMetrica/(?P<id_metrica>[0-9]+)/(?P<tipo>[\w\-]+)/(?P<id_tipo>[0-9]+)$', views.getDetallesMetrica, name="getDetallesMetrica"),
    url(r'^tareas/filtrarTareasPorAlumno$', views.filtrarTareasPorAlumno, name="filtrarTareasPorAlumno"),
    url(r'^tareas/get_config_ide$', views.get_config_ide, name="get_config_ide"),

    url(r'^tareas/mandarDatosIde/(?P<id_tarea>[0-9]+)/(?P<id_grupo>[0-9]+)$', views.mandarDatosIde, name="mandarDatosIde"),
    url(r'^tareas/mostrarCalificacionTarea/(?P<id_tarea>[0-9]+)/(?P<id_grupo>[0-9]+)$', views.mostrarCalificacionTarea, name="mostrarCalificacionTarea"),
]
