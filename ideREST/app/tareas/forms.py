#!/usr/bin/python
# -*- coding: latin-1 -*-
from django import forms
from django.utils import timezone
import datetime

class NuevaTarea(forms.Form):
    nombreTarea = forms.CharField(
        required = True,
        label='Nombre de la Tarea',
        widget = forms.TextInput(
            attrs = {'placeholder':'Nombre Tarea'}
        )
    )
    descripcionTarea = forms.CharField(
        required = True,
        label='Descripcion de la Tarea',
        widget = forms.TextInput(
            attrs = {'placeholder':'Descripcion Tarea'}
        )
    )

    fechaInicio = forms.DateTimeField(
        required = True,
        widget =  forms.DateTimeInput(
            attrs = {'value': str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))}
            )
    )
    fechaFin = forms.DateTimeField(
        required = True,
        widget =  forms.DateTimeInput(
            attrs = {'value': str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))}
            )
    )
    file_field = forms.FileField(
        required=False,
        label='Archivos de la Tarea',
        widget=forms.ClearableFileInput(attrs={'multiple': True})
        )
    enableSnippets = forms.BooleanField(
        required=False,
        label='Habilitar Snippets',
        )
    enableBasicAutocompletion = forms.BooleanField(
        required=False,
        label='Habilitar AutoCompletar',
        )
    enableLiveAutocompletion = forms.BooleanField(
        required=False,
        label='Habilitar AutoCompletar en Vivo',
        )
    infoToken = forms.BooleanField(
        required=False,
        label='Habilitar Información de Código',
        )
    maxL = forms.DecimalField(
        required = False,
        label='Lineas Maximas',
        widget = forms.TextInput(
            attrs = {'placeholder':' Definir Lineas Maximas', 'value':0}
        )
    )
    highlightActiveLine = forms.BooleanField(
        required=False,
        label='Remarcar Linea Activa',
        )
    behavioursEnabled = forms.BooleanField(
        required=False,
        label='Habilitar Cerrado automatico de Llaves',
        )
class ModTarea(forms.Form):
    nombreTarea = forms.CharField(
        required = True,
        label='Nombre de la Tarea',
        widget = forms.TextInput(
            attrs = {'placeholder':'Nombre Tarea'}
        )
    )
    descripcionTarea = forms.CharField(
        required = True,
        label='Descripcion de la Tarea',
        widget = forms.TextInput(
            attrs = {'placeholder':'Descripcion Tarea'}
        )
    )
    fechaFin = forms.DateTimeField(
        required = True,
        widget =  forms.DateTimeInput(
            attrs = {'value': str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))}
            )
    )
    file_field = forms.FileField(
        required=False,
        label='Archivos de la Tarea',
        widget=forms.ClearableFileInput(attrs={'multiple': True})
        )
    enableSnippets = forms.BooleanField(
        required=False,
        label='Habilitar Snippets',
        )
    enableBasicAutocompletion = forms.BooleanField(
        required=False,
        label='Habilitar AutoCompletar',
        )
    enableLiveAutocompletion = forms.BooleanField(
        required=False,
        label='Habilitar AutoCompletar en Vivo',
        )
    infoToken = forms.BooleanField(
        required=False,
        label='Habilitar Información de Código',
        )
    maxL = forms.DecimalField(
        required = False,
        label='Lineas Maximas',
        widget = forms.TextInput(
            attrs = {'placeholder':' Definir Lineas Maximas', 'value':0}
        )
    )
    highlightActiveLine = forms.BooleanField(
        required=False,
        label='Remarcar Linea Activa',
        )
    behavioursEnabled = forms.BooleanField(
        required=False,
        label='Habilitar Cerrado automatico de Llaves',
        )
class CalificarTarea(forms.Form):
    calificacion = forms.CharField(
        required = True,
        label='Calificacion de la Tarea:',
        widget = forms.TextInput(
            attrs = {'placeholder':'Calificacion Tarea', 'type':'number', 'step':"0.1"}
        )
    )
    observaciones = forms.CharField(
        required = True,
        label='Observación de la Tarea',
        widget = forms.TextInput(
            attrs = {'placeholder':'Observacion Tarea'}
        )
    )