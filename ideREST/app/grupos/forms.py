#!/usr/bin/python
# -*- coding: latin-1 -*-
from django import forms
class UnirseGrupo(forms.Form):
    clave = forms.CharField(
        required = True,
        label='Clave del grupo:',
        widget = forms.TextInput(
            attrs = {'placeholder':'Clave del Grupo'}
        )
    )
class AgregarAlumno(forms.Form):
    expediente = forms.CharField(
        required = True,
        label='Expediente del alumno:',
        widget = forms.TextInput(
            attrs = {'placeholder':'Expediente del Grupo', 'type':'number'}
        )
    )
class PerfilPhotoForm(forms.Form):
    file = forms.ImageField()

class AjustesGrupo(forms.Form):
    nombre = forms.CharField(
        required = True,
        label='Nombre del Grupo:',
        widget = forms.TextInput(
            attrs = {'placeholder':'Nombre del Grupo'}
        )
    )
    detalles = forms.CharField(
        required = True,
        label='Descripcion del Grupo:',
        widget = forms.TextInput(
            attrs = {'placeholder':'Descripcion del Grupo'}
        )
    )
class NuevoGrupo(forms.Form):
    nombre = forms.CharField(
        required = True,
        label='Nombre del Grupo:',
        widget = forms.TextInput(
            attrs = {'placeholder':'Nombre del Grupo'}
        )
    )
    detalles = forms.CharField(
        required = True,
        label='Descripcion del Grupo:',
        widget = forms.TextInput(
            attrs = {'placeholder':'Descripcion del Grupo'}
        )
    )