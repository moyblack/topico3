# -*- coding: utf-8 -*-
import json
import os
from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.core import serializers
from app.sistema.models import Usuarios, Usuario_Tareas
from app.sistema.models import Grupos
from app.sistema.models import Usuario_Grupos, Tarea
from django.core.exceptions import ObjectDoesNotExist
import uuid
from app.sistema.views import enviarCorreo
from IDE.settings import BASE_DIR
from django.db.models import Q
from app.grupos.forms import UnirseGrupo, AjustesGrupo, NuevoGrupo, AgregarAlumno

def unirseGrupo(request):
    if request.user.is_authenticated():
        form = UnirseGrupo()
        return render(request, "views/partials/unirseGrupo.html", {'form': form})
    else:
        return redirect('/')

#obtener todos los grupos de la base de datos
def getAllGrupos(request):
    if request.method == "GET":
        grupos = Grupos.objects.all()
        gruposToJson = {}
        cont =0
        for i in grupos:
            gruposToJson[cont] = grupos.values()[cont]
            cont += 1

        return JsonResponse(gruposToJson)
    else:
        return JsonResponse({"mensaje": "no se hizo por get"})

def getGrupos(request):
    """
        Obtiene los grupos a los que pertenece el alumno
        Obtiene las tareas que tiene dicho grupo y muestra las pendientes
        que tiene dicho alumno
    """
    if request.user.is_authenticated():
        username = request.user.username
        usuario = User.objects.get(username = username)
        idUsuario = usuario.id
        idsGrupo = []
        usuario_grupo = Usuario_Grupos.objects.filter(id_usuario_id = idUsuario)
        for data in usuario_grupo:
            datos_grupo = Grupos.objects.filter(pk=data.id_grupo_id).first()
            if datos_grupo is not None:
                if datos_grupo.activo == True:
                    maestro = User.objects.filter(pk=datos_grupo.usuario_id).first()
                    nombre = maestro.first_name + " " + maestro.last_name
                    tareas_grupo = Usuario_Tareas.objects.filter(
                        id_grupo_id=data.id_grupo_id, id_usuario_id=idUsuario, estatus="pendiente").count()
                    tareas_curso = Usuario_Tareas.objects.filter(
                        id_grupo_id=data.id_grupo_id, id_usuario_id=idUsuario, estatus="curso").count()
                    idsGrupo.append([datos_grupo.pk, datos_grupo.grupos, tareas_grupo, tareas_curso, nombre])
        return render(request, "views/partials/grupos_alumno.html", {'idsGrupo': idsGrupo})
    else:
        return redirect('/')

def detalles_grupo_cve(request):
    """
        Obtiene la clave del grupo y retorna los datos
        de dicho grupo como del responsable del grupo
    """
    if request.method == 'POST':
            form = UnirseGrupo(request.POST)
            if form.is_valid():
                datos_grupo = []
                clave = form.cleaned_data["clave"]
                grupo = Grupos.objects.filter(pass_grupo=clave).first()
                if grupo is not None:
                    datos_maestro = User.objects.filter(pk=grupo.usuario_id).first()
                    datos_grupo.append([grupo.pk, grupo.grupos,
                        datos_maestro.first_name, datos_maestro.last_name])
                    return render(request, "views/partials/unirseGrupo.html", {'form': form, 'datos':datos_grupo})
                else:
                    return render(request, "views/partials/unirseGrupo.html", {'form': form, 'mensajeError':'No Existe'})
            else:
                return render(request, "views/partials/unirseGrupo.html", {'form': form})
    else:
        return redirect('/grupos/unirseGrupo')

def alumnos_grupo(request, id_grupo):
    """
        Obtiene los alumnos que pertenecen a un grupo
        retorna los detalles de un alumno
    """
    if request.user.is_authenticated():
        alumno_array = []
        grupo = id_grupo
        alumno = Usuario_Grupos.objects.filter(id_grupo_id=grupo)
        for data in alumno:
            alumno_aux = []
            detalles_alumno = User.objects.filter(pk=data.id_usuario_id).first()
            alumno_aux.append(detalles_alumno.username)
            first_name = detalles_alumno.first_name
            first_name = first_name.split(" ")
            last_name = detalles_alumno.last_name
            last_name = last_name.split(" ")
            alumno_aux.append(first_name[0])
            alumno_aux.append(last_name[0])
            expediente_alumno = Usuarios.objects.filter(user_id=detalles_alumno.pk).first()
            alumno_aux.append(expediente_alumno.expediente)
            alumno_aux.append(expediente_alumno.user_id)
            alumno_array.append(alumno_aux)
        return render(request, "views/partials/miembrosGrupo.html", {'datos': alumno_array, 'id_grupo':id_grupo})
    else:
        return redirect('/')

def get_alumnos_maestro(request):
    """
        Obtiene los alumnos que pertenecen a profesor
        retorna los detalles de un alumno
    """
    alumno_array = []
    grupo = {}
    array_aux = []
    maestro = request.GET["id_usuario"]
    grupos = Grupos.objects.filter(usuario_id=maestro, activo=1)
    cont = 0
    if grupos.exists():
        for i in grupos:
            grupo[cont] = grupos.values()[cont]
            usuarios_aux = Usuario_Grupos.objects.filter(id_grupo_id=grupo[cont]['id'])
            for datos in usuarios_aux:
                if datos.id_usuario_id not in array_aux:
                    usuario = User.objects.get(pk = datos.id_usuario_id)
                    alumno_array.append({"id":datos.id_usuario_id, "nombre":usuario.first_name+' '+usuario.last_name, "email":usuario.username})
                    array_aux.append(datos.id_usuario_id)
            cont += 1
    return JsonResponse(alumno_array, safe=False)

def eliminar_alumno_grupo(request,id_grupo):
    """
        Elimina a un alumno de un grupo en especifico
    """
    if request.user.is_authenticated():
        username = request.user.username
        usuario = User.objects.get(username = username )
        usuario = usuario.id
        idgrupo = id_grupo
        try:
            grupo = Usuario_Grupos.objects.get(id_grupo_id=idgrupo, id_usuario_id=usuario)
            nombreGrupo = Grupos.objects.get(id =  idgrupo)
            user = User.objects.get(id = grupo.id_usuario_id)
            grupo.delete()
            asunto = "IDE: Te han eliminado del grupo"
            mensaje = "Hola este correo es para informar que te han eliminado del grupo "+nombreGrupo.grupos
            remitente = [user.username]
            correo = enviarCorreo(asunto , mensaje , remitente)
            return redirect('/')
        except:
            return redirect('/')
    else:
        return redirect('/')
def eliminar_alumno_grupoM(request,id_grupo, id_alumno):
    """
        Elimina a un alumno de un grupo en especifico
    """
    if request.user.is_authenticated():
        usuario = id_alumno
        idgrupo = id_grupo
        try:
            grupo = Usuario_Grupos.objects.get(id_grupo_id=idgrupo, id_usuario_id=usuario)
            nombreGrupo = Grupos.objects.get(id = idgrupo )
            user = User.objects.get(id = grupo.id_usuario_id)
            grupo.delete()
            asunto = "IDE: Te han eliminado del grupo"
            mensaje = "Hola este correo es para informar que te han eliminado del grupo "+nombreGrupo.grupos
            remitente = [user.username]
            correo = enviarCorreo(asunto , mensaje , remitente)
            return redirect('/grupos/get_alumnos_grupo/'+id_grupo)
        except:
            return redirect('/grupos/get_alumnos_grupo/'+id_grupo)
    else:
        return redirect('/')

def agregar_participante_grupo(request, grupo):
    """
        Valida que el usuario exista en el sistema
        Registra al usuario al grupo si no se encuentra ya en este
    """
    if request.user.is_authenticated():
        username = request.user.username
        user = User.objects.get(username = username )
        usuario = Usuarios.objects.filter(user_id=user.id).first()
        existe = Usuario_Grupos.objects.filter(id_grupo_id=grupo, id_usuario_id=usuario.user_id).first()
        if existe is None:
            relacion = Usuario_Grupos(id_grupo_id=grupo, id_usuario_id=usuario.user_id)
            relacion.save()
            """
            para cada tarea que tiene ese grupo, crear un registro en la
            tabla Usuario_Tareas para asignarle la tarea al alumno
            """
            try:
                tareasDeGrupo = Tarea.objects.filter(grupo_id = grupo)
                for tarea in tareasDeGrupo:
                    existe = Usuario_Tareas.objects.filter(id_tarea_id= tarea.id , id_usuario_id=usuario.user_id , id_grupo_id=grupo).first()
                    if existe is None:
                        usuarioTarea = Usuario_Tareas(calificacion = 0 , observaciones = "", estatus='pendiente' , fecha_entrega=tarea.fecha_fin , id_tarea_id= tarea.id , id_usuario_id=usuario.user_id , id_grupo_id=grupo)
                        usuarioTarea.save()
                #enviar correo de notificacion
                user = User.objects.get(id = usuario.user_id)
                grupoC = Grupos.objects.get(id = grupo)
                asunto = "IDE: te han agregado a un grupo"
                mensaje = "Hola este correo es para informarte que te haz unido al grupo "+grupoC.grupos+" no responder a esta correo"
                remitente = [user.username]
                correo = enviarCorreo(asunto , mensaje , remitente)
            except ObjectDoesNotExist:
                form = UnirseGrupo()
                return render(request, "views/partials/unirseGrupo.html", {'form': form,'mensajeUnirse':'Usuario agregado al grupo'})
            form = UnirseGrupo()
            return render(request, "views/partials/unirseGrupo.html", {'form': form,'mensajeUnirse':'Usuario agregado al grupo'})
        else:
            form = UnirseGrupo()
            return render(request, "views/partials/unirseGrupo.html", {'form': form,'mensajeUnirse':'Usuario ya existe en el grupo'})
    else:
        return redirect('/')

def agregar_alumno_grupo(request, grupo):
    """
        Valida que el usuario exista en el sistema
        Registra al usuario al grupo si no se encuentra ya en este
    """
    if request.method == 'POST':
        if request.user.is_authenticated():
            form = AgregarAlumno(request.POST)
            if form.is_valid():   

                expediente = form.cleaned_data["expediente"]
                usuario = Usuarios.objects.filter(expediente=expediente).first()
                if usuario is None:
                    return render(request, "views/partials/agregarMiembroM.html", {'form': form,'mensajeUnirse':'Usuario no existe','id_grupo':grupo})
                else:
                    existe = Usuario_Grupos.objects.filter(id_grupo_id=grupo, id_usuario_id=usuario.user_id).first()
                    if usuario is None:
                        relacion = Usuario_Grupos(id_grupo_id=grupo, id_usuario_id=usuario.user_id)
                        relacion.save()
                        """
                        para cada tarea que tiene ese grupo, crear un registro en la
                        tabla Usuario_Tareas para asignarle la tarea al alumno
                        """
                        try:
                            tareasDeGrupo = Tarea.objects.filter(grupo_id = grupo)
                            for tarea in tareasDeGrupo:
                                existe = Usuario_Tareas.objects.filter(id_tarea_id= tarea.id , id_usuario_id=usuario.user_id , id_grupo_id=grupo).first()
                                if existe is None:
                                    usuarioTarea = Usuario_Tareas(calificacion = 0 , observaciones = "", estatus='pendiente' , fecha_entrega=tarea.fecha_fin , id_tarea_id= tarea.id , id_usuario_id=usuario.user_id , id_grupo_id=grupo)
                                    usuarioTarea.save()
                            #enviar correo de notificacion
                            user = User.objects.get(id = usuario.user_id)
                            grupoC = Grupos.objects.get(id = grupo)
                            asunto = "IDE: te han agregado a un grupo"
                            mensaje = "Hola este correo es para informarte que te haz unido al grupo "+grupoC.grupos+" no responder a esta correo"
                            remitente = [user.username]
                            correo = enviarCorreo(asunto , mensaje , remitente)
                        except ObjectDoesNotExist:
                            form = AgregarAlumno()
                            return render(request, "views/partials/agregarMiembroM.html", {'form': form,'mensajeUnirse':'Usuario agregado al grupo', 'id_grupo':grupo})
                        form = AgregarAlumno()
                        return render(request, "views/partials/agregarMiembroM.html", {'form': form,'mensajeUnirse':'Usuario agregado al grupo','id_grupo':grupo})
                    else:
                        form = AgregarAlumno()
                        return render(request, "views/partials/agregarMiembroM.html", {'form': form,'mensajeUnirse':'Usuario ya existe en el grupo','id_grupo':grupo})
            else:
                return render(request, 'views/partials/agregarMiembroM.html', {'form': form, 'id_grupo':grupo})
        else:
            return redirect('/')
    else:
        form = AgregarAlumno()
        return render(request, 'views/partials/agregarMiembroM.html', {'form': form, 'id_grupo':grupo})

def insertGrupo(request):
    """
        Inserta un nuevo grupo a la base de datosUsuario
        genera automaticamente la clave del grupos
        llamando la funcion generar_clave_grupo
    """
    if request.method == 'POST':
        if request.user.is_authenticated():
            username = request.user.username
            user = User.objects.get(username = username )
            form = NuevoGrupo(request.POST)
            if form.is_valid():   
                nombre = form.cleaned_data["nombre"]
                descripcion = form.cleaned_data["detalles"]
                pass_grupo = generar_clave_grupo()
                usuario_id = user.id
                try:
                    grupo = Grupos(grupos = nombre , descripcion = descripcion , pass_grupo = pass_grupo , usuario_id = usuario_id)
                    grupo.save()
                    nuevo = os.path.join(BASE_DIR, 'static/grupos/'+str(grupo.id))
                    os.makedirs(nuevo, 0777)
                    return redirect('/')
                except ObjectDoesNotExist:
                    return render(request, 'views/partials/crearGrupo.html', {'form': form})
            else:
                return render(request, 'views/partials/crearGrupo.html', {'form': form})
        else:
            return redirect('/')
    else:
        form = NuevoGrupo()
        return render(request, 'views/partials/crearGrupo.html', {'form': form})

def generar_clave_grupo():
    """
        Genera la clave automaticamente del grupo
        si la clave ya existe vuelve a generar otra nueva
    """
    size = 6
    random = str(uuid.uuid4())
    random = random.upper()
    random = random.replace("-","")
    random = random[0:size]
    clave = Grupos.objects.filter(pass_grupo=random).first()
    if clave is None:
        return random
    else:
        generar_clave_grupo()

def datos_grupo(request, id_grupo):
    """
        Obtiene los datos del grupo en especifico
    """
    if request.user.is_authenticated():
        username = request.user.username
        usuario = User.objects.get(username = username )
        detalles = Grupos.objects.filter(usuario_id=usuario.id, pk=id_grupo).first()
        diccionario = {"nombre":detalles.grupos, "id_grupo":id_grupo,
            "detalles":detalles.descripcion, "clave":detalles.pass_grupo}
        return render(request, "views/partials/datosGrupo.html", {'diccionario': diccionario}) 
    else:
        return redirect('/')

def datos_grupoM(request, id_grupo):
    """
        Obtiene los datos del grupo en especifico
    """
    if request.user.is_authenticated():
        form = AjustesGrupo()
        username = request.user.username
        usuario = User.objects.get(username = username )
        detalles = Grupos.objects.filter(usuario_id=usuario.id, pk=id_grupo).first()
        diccionario = {"nombre":detalles.grupos, "id_grupo":id_grupo,
            "detalles":detalles.descripcion, "clave":detalles.pass_grupo}
        return render(request, "views/partials/ajustesGrupos.html", {'diccionario': diccionario, 'id_grupo':id_grupo, 'form':form}) 
    else:
        return redirect('/')

def get_grupo_maestros(request):
    """
        Obtiene los grupos a los que pertenece un maestro
        Retorna dichos datos en un arreglo
    """
    grupos = []
    usuario = request.GET["id_user"]
    grupo = Grupos.objects.filter(usuario_id=usuario)
    for data in grupo:
        nombre_grupo = data.grupos
        if len(nombre_grupo) > 14:
            nombre_grupo = nombre_grupo[:14] + "..."
        if data.activo == True:
            grupos.append([data.pk, nombre_grupo])
    return JsonResponse(grupos, safe=False)

def delete_grupo(request, id_grupo):
    """
        Controlador que elimina un grupo
        cambia el estado de activo a False
    """
    if request.method == "GET":
        try:
            instance = Grupos.objects.get(id = id_grupo)
            instance.activo = False
            instance.save()
            return redirect('/')
        except ObjectDoesNotExist:
            return redirect('/')

def renovar_clave_grupo(request):
    """
        Controlador que utiliza la funcion generar_clave_grupo
        para obtener una nueva clave disponible para el grupo
    """
    clave = generar_clave_grupo()
    return HttpResponse(clave)

def get_alumno_evaluacion(request):
        idUser = request.GET["id_user"]
        totalGrupos = Grupos.objects.filter(usuario_id=idUser).count()
        grupos = Grupos.objects.filter(usuario_id=idUser, activo=1)
        grupo = {}
        datosGrupos = {}
        array_aux = []
        array_final= []
        tareasCreadas = 0
        alumnosInscritos = 0
        evalPendientes = 0
        cont = 0
        if grupos.exists():
            for i in grupos:
                grupo[cont] = grupos.values()[cont]
                usuarios_aux = Usuario_Grupos.objects.filter(id_grupo_id=grupo[cont]['id'])
                for datos in usuarios_aux:
                    if datos.id_usuario_id not in array_aux:
                        array_aux.append(datos.id_usuario_id)
                        alumno = User.objects.filter(pk=datos.id_usuario_id).first()
                        array_final.append([alumno.pk, alumno.username, alumno.first_name, alumno.last_name])
                cont += 1
        return JsonResponse(array_final, safe=False)

def update_grupo(request, id_grupo):
    """
        Controlador que actualiza la informacion del grupo
    """
    if request.method == 'POST':
            form = AjustesGrupo(request.POST)
            if form.is_valid():
                nombre_grupo = form.cleaned_data["nombre"]  
                descripcion_grupo = form.cleaned_data["detalles"]
                try:
                    grupo = Grupos.objects.get(id = id_grupo)
                    grupo.grupos = nombre_grupo
                    grupo.descripcion = descripcion_grupo
                    grupo.save()
                    return redirect('/grupos/get_datos_grupoM/'+id_grupo)
                except ObjectDoesNotExist:
                    return redirect('/grupos/get_datos_grupoM/'+id_grupo)
            else:
                return redirect('/grupos/get_datos_grupoM/'+id_grupo)

def cambiarResponsableDeGrupo(request):
    if request.method=="GET":
        id_grupo = request.GET["id_grupo"]
        id_responsable = request.GET["id_responsable"]
        try:
            grupo = Grupos.objects.get(id = id_grupo)
            grupo.usuario_id = id_responsable
            grupo.save()
            return JsonResponse({"mensaje" : "se ha modificado el grupo"})
        except ObjectDoesNotExist:
            return JsonResponse({'mensaje':'el grupo no existe'})

    else:
        return JsonResponse({"mensaje":"no se hizo por get"})


def filtrarGruposPorAlumno(request):
    if request.method == "GET":
        idUsuario = request.GET["idUsuario"]
        criterioBusqueda = request.GET["criterioBusqueda"]

        arrayGrupos = []
        #obtener los grupos a los que esta inscrito el usuario_id
        gruposInscritos = Usuario_Grupos.objects.filter(id_usuario_id = idUsuario)
        gruposFiltrados = Grupos.objects.filter(grupos__startswith = criterioBusqueda)

        for i in gruposInscritos:
            for x in gruposFiltrados:
                if i.id_grupo_id == x.id:
                    arrayGrupos.append([x.id , x.grupos])
        return JsonResponse(arrayGrupos , safe=False)
    else:
        return JsonResponse({"mensaje" : "no se hizo por get"})

def getAllGruposAlumno(request):
    """
        Obtiene los grupos a los que pertenece el alumno
    """
    idUsuario = request.GET['id_user']
    idsGrupo = []
    usuario_grupo = Usuario_Grupos.objects.filter(id_usuario_id = idUsuario)
    for data in usuario_grupo:
        datos_grupo = Grupos.objects.filter(pk=data.id_grupo_id).first()
        if datos_grupo is not None:
            if datos_grupo.activo == True:
                idsGrupo.append([datos_grupo.pk, datos_grupo.grupos])
        else:
            return HttpResponse("GRUPO_NO_EXISTE")
    return JsonResponse(idsGrupo, safe=False)

def filtrarGruposPorMaestro(request):
    if request.method == "GET":
        idUsuario = request.GET["idUsuario"]
        criterioBusqueda = request.GET["criterioBusqueda"]
        arrayGrupos = []
        #obtener los grupos a los que esta inscrito el usuario_id
        gruposFiltrados = Grupos.objects.filter(grupos__startswith = criterioBusqueda, usuario_id=idUsuario)
        for i in gruposFiltrados:
            if i.activo == True:
                arrayGrupos.append([i.id , i.grupos])
        return JsonResponse(arrayGrupos , safe=False)
    else:
        return JsonResponse({"mensaje" : "no se hizo por get"})
