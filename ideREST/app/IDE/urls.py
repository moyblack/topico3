from django.conf.urls import url
from .views import IDEView, insertMetricas, entregar, salir, getPastMetricas,getProyeccionDefectos, setNewTheme

urlpatterns = [
    url(r'^main/(?P<exp>[0-9]+)/(?P<gpo>[0-9]+)/(?P<tarea>[0-9]+)/(?P<tema>[0-9]+)$', IDEView, name='ide'),
    url(r'^insertmetricas/(?P<exp>[0-9]+)/(?P<gpo>[0-9]+)/(?P<tarea>[0-9]+)$', insertMetricas, name='insertMetricas'),
    url(r'^entregar/(?P<exp>[0-9]+)/(?P<gpo>[0-9]+)/(?P<tarea>[0-9]+)$', entregar, name='entregar'),
    url(r'^salir/(?P<exp>[0-9]+)/(?P<gpo>[0-9]+)/(?P<tarea>[0-9]+)$', salir, name='salir'),
    url(r'^getPastMetricas/(?P<exp>[0-9]+)/(?P<gpo>[0-9]+)/(?P<tarea>[0-9]+)$', getPastMetricas, name='getPastMetricas'),
    url(r'^getProyeccionDefectos/(?P<exp>[0-9]+)/(?P<gpo>[0-9]+)/(?P<tarea>[0-9]+)$', getProyeccionDefectos, name='getProyeccionDefectos'),
    url(r'^setNewTheme/(?P<exp>[0-9]+)/(?P<newTheme>[0-9]+)$', setNewTheme, name='setNewTheme'),
]
