from django.views.generic import TemplateView
from django.shortcuts import render
from django.conf import settings
from app.sistema import models
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponseBadRequest, HttpResponse, JsonResponse
from django.db.models import Sum
import json
import urllib2
import urllib

def IDEView(request, exp, gpo, tarea, tema):
    if request.user.is_authenticated():
        return render(request, "views/partials/ide.html",{'exp':exp, 'gpo':gpo, 'tarea':tarea, 'tema':tema})
    else:
        return redirect('/')

@csrf_exempt
def insertMetricas(request, exp, gpo, tarea):
    if request:
        in_data= json.loads(request.body)
        met1=  in_data['body'][0]['met1']
        met2 = in_data['body'][0]['met2']
        met3 = in_data['body'][0]['met3']
        met4 = in_data['body'][0]['met4']
        met5 = in_data['body'][0]['met5']
        met6 = in_data['body'][0]['met6']
        met7 = in_data['body'][0]['met7']
        met8 = in_data['body'][0]['met8']
        met9 = in_data['body'][0]['met9']
        met10 = in_data['body'][0]['met10']
        met11 = in_data['body'][0]['met11']
        met12 = in_data['body'][0]['met12']
        met13= in_data['body'][0]['met13']
        met14 = in_data['body'][0]['met14']
        met15 = in_data['body'][0]['met15']
        met16 = in_data['body'][0]['met16']
        met17 = in_data['body'][0]['met17']
        met18 = in_data['body'][0]['met18']
        met19 = in_data['body'][0]['met19']
        met20 = in_data['body'][0]['met20']
        met21 = in_data['body'][0]['met21']
        met22 = in_data['body'][0]['met22']
        met23 = in_data['body'][0]['met23']
        met24 = in_data['body'][0]['met24']
        met25 = in_data['body'][0]['met25']
        met26 = in_data['body'][0]['met26']
        met27 = in_data['body'][0]['met27']
        met28 = in_data['body'][0]['met28']
        met29 = in_data['body'][0]['met29']
        met30 = in_data['body'][0]['met30']
        met31 = in_data['body'][0]['met31']
        met32 = in_data['body'][0]['met32']
        met33 = in_data['body'][0]['met33']
        met34 = in_data['body'][0]['met34']
        met35 = in_data['body'][0]['met35']
        arrayMet = [met1, met2, met3, met4, met5, met6, met7,met8,met9,met10,met11,met12,met13,met14,met15,met16,met17,met18,met19,met20,
                    met21,met22,met23,met24,met25,met26,met27,met28,met29,met30,met31,met32,met33,met34, met35]
        for x in range(0, len(arrayMet)):
            segura = x + 1
            p = models.Metricas.objects.filter(id_usuario_id=exp, id_grupo_id=gpo, id_tarea_id=tarea, id_metrica_id = segura).first()
            if p is not None:
                p.calificacion = arrayMet[x]
                p.save()
            else:
                p = models.Metricas.objects.create(id_usuario_id=exp, id_grupo_id=gpo, id_tarea_id=tarea, id_metrica_id =segura, calificacion=arrayMet[x])
        return HttpResponse('variables ingresadas', status=200)

def entregar(request, exp, gpo, tarea):
    if request:
        p = models.Usuario_Tareas.objects.filter(id_usuario_id=exp, id_grupo_id=gpo, id_tarea_id=tarea).first()
        if p is not None:
            p.estatus = "entregado"
            p.save()
            return HttpResponse('Estatus actualizado', status=200)
        else:
            return HttpResponse('Error', status=501)

def salir(request, exp, gpo, tarea):
    if request:
        p = models.Usuario_Tareas.objects.filter(id_usuario_id=exp, id_grupo_id=gpo, id_tarea_id=tarea).first()
        if p is not None:
            p.estatus = "curso"
            p.save()
            return HttpResponse('Estatus actualizado', status=200)
        else:
            return HttpResponse('Error', status=501)
def getPastMetricas(request, exp, gpo, tarea):
    if request:
        arrayMetricas = []
        j = [];
        p =  models.Metricas.objects.filter(id_usuario_id=exp, id_grupo_id=gpo, id_tarea_id=tarea)
        if p is not None:
            for i in p:
                arrayMetricas.append(i.calificacion)
            for index, item in enumerate(arrayMetricas):
                j.append(json.loads('{"'+str(index)+'" : "'+str(item)+'"}'))
            return JsonResponse({"mensaje" :j} , safe=False)
        else:
            return JsonResponse({"mensaje" : "unregistered"})
    else:
        return HttpResponse('Error', status=501)

def getProyeccionDefectos(request, exp, gpo, tarea):
    if request:
        sumLocs = 0;
        sumDefects = 0;
        j = [];
        c =  models.Metricas.objects.filter(id_usuario_id=exp, id_grupo_id=gpo, id_tarea_id=tarea).count()
        p =  models.Metricas.objects.filter(id_usuario_id=exp, id_grupo_id=gpo, id_tarea_id=tarea)
        if c > 0:
            for i in p:
                if i.id_metrica_id == 24:
                    sumLocs = sumLocs + i.calificacion
                if i.id_metrica_id == 25:
                     sumDefects= sumDefects + i.calificacion
            j.append(json.loads('{"sumLocs" : "'+str(sumLocs)+'", "sumDefects" : "'+str(sumDefects)+'"}'))
            return JsonResponse({"mensaje" :j} , safe=False)
        else:
            return JsonResponse({"mensaje" : "missing"})
    else:
        return HttpResponse('Error', status=501)

def setNewTheme(request, exp, newTheme):
    if request:
        datos = models.Usuarios.objects.filter(user_id=exp).first()
        if datos is not None:
            datos.tema_ide = newTheme
            datos.save()
            return HttpResponse("OK")
        else:
            return HttpResponse('Error', status=501)
    else:
        return HttpResponse('Error', status=501)

