from django import forms
from django.contrib.auth.models import User
from app.sistema.models import Usuarios

Instituciones = (
    ('Universidad Autonoma De Queretaro', 'Universidad Autonoma De Queretaro'),
    ('Instituto Tecnologico de Estudios Superiores de Monterrey', 'Instituto Tecnologico de Estudios Superiores de Monterrey'),
)

class RegistroMaestroForm(forms.Form):
    email = forms.CharField(
        required = True,
        widget = forms.EmailInput(
            attrs = {'placeholder':'Email'}
        )
    )
    password = forms.CharField(
        required = True,
        widget = forms.PasswordInput(
            attrs = {'placeholder':'Password'}
        )
    )
    first_name = forms.CharField(
        required = True,
        label='Nombre(s)',
        widget = forms.TextInput(
            attrs = {'placeholder':'Nombre(s)'}
        )
    )
    last_name = forms.CharField(
        required = True,
        label='Apellido(s)',
        widget = forms.TextInput(
            attrs = {'placeholder':'Apellido(s)'}
        )
    )
    expediente = forms.CharField(
        required = True,
        widget = forms.TextInput(
            attrs = {'placeholder': 'Expediente'}
        )
    )
    institucion = forms.ChoiceField(
        required = True,
        widget = forms.Select(),
        choices = Instituciones
    )
class HacerMaestroForm(forms.Form):
    email = forms.CharField(
        required = True,
        widget = forms.EmailInput(
            attrs = {'placeholder':'Email'}
        )
    )