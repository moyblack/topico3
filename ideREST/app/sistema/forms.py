#!/usr/bin/python
# -*- coding: latin-1 -*-
from django import forms
from django.contrib.auth.models import User
from app.sistema.models import Usuarios

class LoginForm(forms.ModelForm):

    email = forms.CharField(
        required = True,
        widget = forms.EmailInput(
            attrs = {'placeholder':'Email', 'class':'form-control Rectangle-7-escritorio'}
        )
    )
    password = forms.CharField(
        required = True,
        widget = forms.PasswordInput(
            attrs = {'placeholder':'Password', 'class':'form-control Rectangle-7-escritorio'}
        )
    )
    class Meta:
        model = User
        fields = ('email', 'password')


Instituciones = (
    ('Universidad Autonoma De Queretaro', 'Universidad Autónoma De Querétaro'),
    ('Instituto Tecnologico de Estudios Superiores de Monterrey', 'Instituto Tecnologico de Estudios Superiores de Monterrey'),
)
Semestre = (
    ('Octavo', 'Octavo'),
    ('Septimo','Septimo'),
    ('Sexto', 'Sexto'),
    ('Quinto','Quinto'),
    ('Cuarto','Cuarto'),
    ('Tercero', 'Tercero'),
    ('Segundo', 'Segundo'),
    ('Primero','Primero'),
)
Temas = (
    ('1', '1'),
    ('2', '2'),
    ('3', '3'),
    ('4', '4'),
    ('5', '5'),
    ('6', '6'),
    ('7', '7'),
    ('8', '8'),
    ('9', '9'),
    ('10', '10'),
    ('11', '11'),
    ('12', '12'),
    ('13', '13'),
    ('14', '14'),
    )

class RegistroUForm(forms.ModelForm):

    email = forms.CharField(
        required = True,
        widget = forms.EmailInput(
            attrs = {'placeholder':'Email'}
        )
    )
    password = forms.CharField(
        required = True,
        widget = forms.PasswordInput(
            attrs = {'placeholder':'Password'}
        )
    )
    first_name = forms.CharField(
        required = True,
        label='Nombre(s)',
        widget = forms.TextInput(
            attrs = {'placeholder':'Nombre(s)'}
        )
    )
    last_name = forms.CharField(
        required = True,
        label='Apellido(s)',
        widget = forms.TextInput(
            attrs = {'placeholder':'Apellido(s)'}
        )
    )
    class Meta:
        model = User
        fields = ('email', 'password', 'first_name', 'last_name')

class RegistroIForm(forms.ModelForm):

    institucion = forms.ChoiceField(
        required = True,
        widget = forms.Select(),
        choices = Instituciones
    )
    semestre = forms.ChoiceField(
        required = True,
        widget =  forms.Select(),
        choices = Semestre
    )
    expediente = forms.CharField(
        required = True,
        widget = forms.TextInput(
            attrs = {'placeholder': 'Expediente'}
        )
    )
    class Meta:
        model = Usuarios
        fields = ('institucion', 'semestre', 'expediente')

class PerfilForm(forms.Form):
    institucion = forms.ChoiceField(
        required = True,
        widget = forms.Select(),
        choices = Instituciones
    )
    first_name = forms.CharField(
        required = True,
        label='Nombre(s)',
        widget = forms.TextInput(
            attrs = {'placeholder':'Nombre(s)'}
        )
    )
    last_name = forms.CharField(
        required = True,
        label='Apellido(s)',
        widget = forms.TextInput(
            attrs = {'placeholder':'Apellido(s)'}
        )
    )
    semestre = forms.ChoiceField(
        required = True,
        widget =  forms.Select(),
        choices = Semestre
    )
    password = forms.CharField(
        required = False,
        initial="",
        widget = forms.PasswordInput(
            attrs = {'placeholder':'Password'}
        )
    )
    tema = forms.ChoiceField(
        required = True,
        widget =  forms.Select(),
        choices = Temas
    )

class PerfilPhotoForm(forms.Form):
    file = forms.ImageField()