# -*- coding: utf-8 -*-
import json
import shutil
import os
import image
from django.core.files.base import ContentFile
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.core import serializers
from app.sistema.models import Usuarios
from django.core.exceptions import ObjectDoesNotExist
from django import forms
from django.core.mail import send_mail
from django.core.mail import EmailMessage
from django.core.mail import EmailMultiAlternatives
from django.conf import settings
from IDE.settings import BASE_DIR


from app.sistema.forms import LoginForm, RegistroUForm, RegistroIForm, PerfilForm, PerfilPhotoForm
from app.sistema.models import Usuarios, Usuario_Tareas
from app.sistema.models import Grupos
from app.sistema.models import Usuario_Grupos, Tarea, Grupos

def bienvenido_view(request):
    """
        bienvenido_view despliega el template de la plantilla de correo
    """
    return render(request, "views/partials/bienvenido.html")

def recuperar_view(request):
    """
        recuperar_view despliega el template de la plantilla de correo
    """
    return render(request, "views/partials/recuperarContrasea.html")

def login_view(request):
    """
        login_view despliega el template de login
    """
    if request.user.is_authenticated():
        username = request.user.username
        usuario = User.objects.get(username = username )
        datosUsuario = Usuarios.objects.get(user_id  = usuario.id)
        if datosUsuario.tipoUser_id == 1:
            return redirect('/inicio_alumno')
        elif datosUsuario.tipoUser_id == 2:
            return redirect('/inicio_maestro')
        elif datosUsuario.tipoUser_id == 3:
            return redirect('/inicio_admin')
    else:
        form = LoginForm()
        return render(request, "views/partials/login.html", {'form': form})
    
def login_viewE(request, mensaje, form):
    """
        login_view despliega el template de login
    """
    if mensaje is not None:
        return render(request, "views/partials/login.html", {'form': form,'mensaje': mensaje})
    else:
        mensaje = ""
        return render(request, "views/partials/login.html", {'form': form, 'mensaje': mensaje})
def registro_viewE(request, mensaje, form1, form2):
    """
        registro_view despliega el template de registro
    """
    if mensaje is not None:
        return render(request, "views/partials/registro.html", {'form1': form1, 'form2':form2, 'mensaje': mensaje})
    else:
        mensaje = ""
        return render(request, "views/partials/registro.html", {'form1': form1, 'form2':form2, 'mensaje': mensaje})
def registro_view(request):
    """
        registro_view despliega el template de registro
    """
    form1 = RegistroUForm()
    form2 = RegistroIForm()
    return render(request, "views/partials/registro.html", {'form1': form1, 'form2':form2})
def maestro_view(request):
    """
        maestro_view despliega el template de maestro
    """
    if request.user.is_authenticated():
        username = request.user.username
        usuario = User.objects.get(username = username )
        datosUsuario = Usuarios.objects.get(user_id  = usuario.id)
        gruposm = []
        grupo = Grupos.objects.filter(usuario_id=usuario.id)
        for data in grupo:
            nombre_grupo = data.grupos
            if data.activo == True:
                gruposm.append([data.pk, nombre_grupo])   
        idUser = usuario.id
        totalGrupos = Grupos.objects.filter(usuario_id=idUser).count()
        grupos = Grupos.objects.filter(usuario_id=idUser, activo=1)
        grupo = {}
        datosGrupos = {}
        array_aux = []
        tareasCreadas = 0
        alumnosInscritos = 0
        evalPendientes = 0
        cont = 0
        if grupos.exists():
            for i in grupos:
                grupo[cont] = grupos.values()[cont]
                usuarios_aux = Usuario_Grupos.objects.filter(id_grupo_id=grupo[cont]['id'])
                for datos in usuarios_aux:
                    if datos.id_usuario_id not in array_aux:
                        array_aux.append(datos.id_usuario_id)
                        alumnosInscritos += 1
                tareasCreadas += Tarea.objects.filter(grupo_id=grupo[cont]['id']).count()
                participantes = Usuario_Grupos.objects.filter(id_grupo_id=grupo[cont]['id']).count()
                pendientes = Usuario_Tareas.objects.filter(id_grupo_id=grupo[cont]['id'], estatus="entregado", calificacion__gt=0).count()
                enviadas = Usuario_Tareas.objects.filter(id_grupo_id=grupo[cont]['id']).count()
                evalPendientes += pendientes
                datosGrupos[cont] = {"nombre":grupo[cont]['grupos'],"participantes":participantes,"pendientes":pendientes,"enviadas":enviadas,"clave":grupo[cont]['pass_grupo']}
                cont += 1
        gruposActivos = Grupos.objects.filter(usuario_id=idUser,activo=1).count()
        return render(request, "views/partials/inicio_maestro.html", {'datos': datosUsuario, 'user': usuario, 'gruposM':gruposm, "totalGrupos":totalGrupos, "gruposActivos":gruposActivos, "tareasCreadas":tareasCreadas,
                "alumnosInscritos":alumnosInscritos,"evalPendientes":evalPendientes,"datosGrupos":datosGrupos})
    else:
        return redirect('/')
def alumno_view(request):
    """
        alumno_view despliega el template de alumno
    """
    if request.user.is_authenticated():
        username = request.user.username
        usuario = User.objects.get(username = username )
        datosUsuario = Usuarios.objects.get(user_id  = usuario.id)
        idUsuario = usuario.id
        idsGrupo = []
        usuario_grupo = Usuario_Grupos.objects.filter(id_usuario_id = idUsuario)
        for data in usuario_grupo:
            datos_grupo = Grupos.objects.filter(pk=data.id_grupo_id).first()
            if datos_grupo is not None:
                if datos_grupo.activo == True:
                    maestro = User.objects.filter(pk=datos_grupo.usuario_id).first()
                    nombre = maestro.first_name + " " + maestro.last_name
                    tareas_grupo = Usuario_Tareas.objects.filter(
                        id_grupo_id=data.id_grupo_id, id_usuario_id=idUsuario, estatus="pendiente").count()
                    tareas_curso = Usuario_Tareas.objects.filter(
                        id_grupo_id=data.id_grupo_id, id_usuario_id=idUsuario, estatus="curso").count()
                    idsGrupo.append([datos_grupo.pk, datos_grupo.grupos, nombre, tareas_grupo, tareas_curso])
        return render(request, "views/partials/inicio_alumno.html", {'idsGrupo': idsGrupo})
    else:
        return redirect('/')

        #json_string = json.dumps([{'mensaje':'Acceso' , 'idUser':usuario.id , 'tema':datosUsuario.tema_ide, 'correo':  usuario.username, "nombre" : usuario.first_name , "apellidos" : usuario.last_name , "expediente":datosUsuario.expediente }])
def admin_view(request):
    """
        admin_view despliega el template de admin
    """
    if request.user.is_authenticated():
        username = request.user.username
        usuario = User.objects.get(username = username )
        datosUsuario = Usuarios.objects.get(user_id  = usuario.id)
        arreglo_maestro = []
        maestro = Usuarios.objects.filter(tipoUser_id=2)
        for data in maestro:
            detalles = User.objects.filter(pk=data.user_id).first()
            arreglo_maestro.append([detalles.pk, detalles.first_name, detalles.last_name,
                                    data.expediente, detalles.username])      
        return render(request, "views/partials/inicio_admin.html", {'datos': datosUsuario, 'user': usuario, 'datos':arreglo_maestro})
    else:
        return redirect('/')
def perfil_view(request):
    """
        perfil_view despliega el template de perfil
    """
    if request.user.is_authenticated():
        username = request.user.username
        usuario = User.objects.get(username = username )
        arreglo_usuario = []
        user = User.objects.filter(pk=usuario.id).first()
        user_detail = Usuarios.objects.filter(user_id=user.pk).first()
        arreglo_usuario.append([user.pk, user.username, user.first_name, user.last_name,
                               user_detail.institucion, user_detail.expediente,
                               user_detail.semestre, user_detail.tema_ide])
        form= PerfilForm()
        form2 = PerfilPhotoForm()
        return render(request, "views/partials/perfil.html", {'datos': arreglo_usuario, 'form':form, 'form2':form2})
    else:
        return redirect('/')

def perfil_upload(request):
    """
        perfil_upload recibe el formulario del perfil
    """
    if request.user.is_authenticated():
        username = request.user.username
        usuario = User.objects.get(username = username )
        if request.method == 'POST':
            form = PerfilForm(request.POST)
            if form.is_valid():
                institucion = form.cleaned_data["institucion"]
                nombre = form.cleaned_data["first_name"]
                apellidos = form.cleaned_data["last_name"]
                semestre = form.cleaned_data["semestre"]
                password = form.cleaned_data["password"]
                tema = form.cleaned_data["tema"]
                datos = Usuarios.objects.filter(user_id=usuario.id).first()
                usuario.first_name = nombre
                usuario.last_name = apellidos
                datos.institucion = institucion
                datos.semestre = semestre
                datos.tema_ide = tema
                if password != "":
                    usuario.set_password(password)
                usuario.save()
                datos.save()
                return redirect('/perfil')
            else:
                arreglo_usuario = []
                user = User.objects.filter(pk=usuario.id).first()
                user_detail = Usuarios.objects.filter(user_id=user.pk).first()
                arreglo_usuario.append([user.pk, user.username, user.first_name, user.last_name,
                                       user_detail.institucion, user_detail.expediente,
                                       user_detail.semestre, user_detail.tema_ide])
                form2 = PerfilPhotoForm()
                return render(request, "views/partials/perfil.html", {'datos': arreglo_usuario, 'form':form, 'form2':form2, 'mensaje1':'Formulario invalido'}) 
        else:
            arreglo_usuario = []
            user = User.objects.filter(pk=usuario.id).first()
            user_detail = Usuarios.objects.filter(user_id=user.pk).first()
            arreglo_usuario.append([user.pk, user.username, user.first_name, user.last_name,
                                   user_detail.institucion, user_detail.expediente,
                                   user_detail.semestre, user_detail.tema_ide])
            form2 = PerfilPhotoForm()
            return render(request, "views/partials/perfil.html", {'datos': arreglo_usuario, 'form':form, 'form2':form2, 'mensaje1':'Actualizacion fallo'})
    else:
        return redirect('/')

def perfil_fotoUpload(request):
    """
        perfil_upload recibe el formulario del perfil
    """
    if request.user.is_authenticated():
        username = request.user.username
        usuario = User.objects.get(username = username )
        if request.method == 'POST':
            form2 = PerfilPhotoForm(request.POST, request.FILES)
            if form2.is_valid():
                email = username
                name = str("user.jpg")
                filename = os.path.join(BASE_DIR, 'static/perfil/'+email, name)
                archivo = request.FILES['file']
                f = open(filename, 'wb+')
                file_content = ContentFile(archivo.read())
                try:
                    for chunk in file_content.chunks():
                        f.write(chunk)
                    f.close()
                    return redirect('/perfil')
                except:
                    form = PerfilForm()
                    arreglo_usuario = []
                    user = User.objects.filter(pk=usuario.id).first()
                    user_detail = Usuarios.objects.filter(user_id=user.pk).first()
                    arreglo_usuario.append([user.pk, user.username, user.first_name, user.last_name,
                                           user_detail.institucion, user_detail.expediente,
                                           user_detail.semestre, user_detail.tema_ide])
                    return render(request, "views/partials/perfil.html", {'datos': arreglo_usuario, 'form':form, 'form2':form2, 'mensaje2':'Error Archivo'})
            else:
                form = PerfilForm()
                arreglo_usuario = []
                user = User.objects.filter(pk=usuario.id).first()
                user_detail = Usuarios.objects.filter(user_id=user.pk).first()
                arreglo_usuario.append([user.pk, user.username, user.first_name, user.last_name,
                                       user_detail.institucion, user_detail.expediente,
                                       user_detail.semestre, user_detail.tema_ide])
                return render(request, "views/partials/perfil.html", {'datos': arreglo_usuario, 'form':form, 'form2':form2, 'mensaje2':'Formulario invalido'})
        else:
            return perfil_view(request)
    else:
        return redirect('/')

def registro_normal(request):

    """
        Registro_normal obtiene el correo y password
        y lo registra en la base de datos.
    """
    if request.method == "POST":
        form1 = RegistroUForm(request.POST)
        form2 = RegistroIForm(request.POST)
        if form1.is_valid() and form2.is_valid():
            email = form1.cleaned_data['email']
            password = form1.cleaned_data['password']
            first_name = form1.cleaned_data['first_name']
            last_name = form1.cleaned_data['last_name']
            institucion= form2.cleaned_data['institucion']
            semestre = form2.cleaned_data['semestre']
            expediente = form2.cleaned_data['expediente']
            if not User.objects.filter(username=email).exists():
                user = User.objects.create_user(username=email, password=password, first_name = first_name,last_name = last_name)
                tipoUser = 1
                usuario = User.objects.get(username = email)
                newUser = Usuarios(user_id=usuario.id , institucion=institucion, semestre=semestre , expediente=expediente , estatus=0 , tipoUser_id=1)
                newUser.save(force_insert=True)
                nueva = os.path.join(BASE_DIR, 'static/perfil/'+email)
                antigua = os.path.join(BASE_DIR, 'static/perfil/default')
                os.makedirs(nueva, 0777)
                for file in os.listdir(antigua):
                    srcfile = os.path.join(BASE_DIR, 'static/perfil/default', file)
                    dstfile = os.path.join(BASE_DIR, 'static/perfil/'+email, file)
                    if os.path.isdir(srcfile):
                        dir_copy(srcfile, dstfile)
                    else:
                        shutil.copyfile(srcfile, dstfile)
                asunto = "Registro exitoso"
                mensaje = "Bienvenido al IDE, tu registro ha sido exitoso"
                remitente = [usuario.username]
                correo = enviarCorreo(asunto , mensaje , remitente)
                return redirect('/login')
            else:
                mensaje = "Este email ya esta en uso. Porfavor utiliza una cuenta de correo diferente."
                return registro_viewE(request, mensaje, form1, form2)
        else:
            mensaje = "Errores en los formularios"
            return registro_viewE(request, mensaje, form1, form2)
    else:
        mensaje = "Error de envio."
        return registro_viewE(request, mensaje, form1, form2)
                

def login_autentificar(request):

    """
        login_autentificar obtiene el usuario y password
        busca si se eencuentra registrado o no
        si esta registrado, crea una variable de session
        si no retorna un mensaje de error
    """
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['email']
            password = form.cleaned_data['password']
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                usuario = User.objects.get(username = username )
                datosUsuario = Usuarios.objects.get(user_id  = usuario.id)
                if datosUsuario.tipoUser_id == 1:
                    return redirect('/inicio_alumno')
                elif datosUsuario.tipoUser_id == 2:
                    return redirect('/inicio_maestro')
                elif datosUsuario.tipoUser_id == 3:
                    return redirect('/inicio_admin')
            else:
                mensaje = "Error en el inicio de sesion."
                return login_viewE(request, mensaje, form)
    else:
        return redirect('/')

def cerrar_sesion(request):
    """
        cerrar_sesion cierra la session actual
        eliminando la variable de session
    """
    if request.user.is_authenticated():
        logout(request)
        return redirect('/')



def enviarCorreo(asunto , mensaje , remitente):
    mail = EmailMessage(asunto , mensaje , settings.EMAIL_HOST_USER , remitente)
    mail.send()
    return "exito"

def sendMail(request):
    print "Entro"
    contenido = """<html>
      <head>
        <meta charset="utf-8">
        <title>Recuperar Contraseña</title>
      </head>
      <body>
        <div style="width: 929px;height: 1024px;background-color: #fff;position: absolute;">
          <div style="
            width: 590px;
            height: 715px;
            border-radius: 8px;
            background-color: #fff;
            border: 1px solid rgba(0,0,0,0.2);
            margin-left: 20px;
            margin-top: 20px;
            margin-left: 170px;
            margin-top: 65px;">
            <div style="
              width: 590px;
              height: 111px;
              object-fit: contain;
              background: #5b539e;
              border-top-left-radius: 8px;
              border-top-right-radius: 8px;">
              <img src="https://imgur.com/FPoecg4.png" class="bbllogo2" alt="" style="
                width: 150px;
                height: 81px;
                margin-left: 220px;
                margin-top: 15px;
                float: left;">
            </div>
            <div class="cuerpoPlantilla">
              <div class="Recuperar-contrasea" style="width: 349px;
              height: 32px;
              font-size: 32px;
              text-align: left;
              color: #302f60;
              margin-left: 52px;
              margin-top: 158px;">
                <p>Recuperar contraseña</p>
              </div>
              <div class="Has-solicitado-una-n" style="width: 449px;
              height: 130px;
              font-size: 16px;
              line-height: 1.63;
              letter-spacing: 0.4px;
              text-align: left;
              color: #3d4548;
              margin-top: 46px;
              margin-left: 51px;">
                <p>Has solicitado una nueva contraseña</p>
                <br>
                <p>Da click en el botón y genera una nueva para esta<br> cuenta...</p>
              </div>
              <button type="button" name="button" class="btnReestablecer" style="width: 201px;
              height: 44px;
              border-radius: 100px;
              background-color: #302f60;
              margin-top: 84px;
              margin-left: 194px;
              outline: none;
              border: none;">
                <p class="REESTABLECER" style="width: 172px;
                height: 16px;
                font-size: 16px;
                text-align: center;
                color: #fff;
                margin-left: 14px;
                margin-top: 10px;">REESTABLECER</p>
              </button>
              <p class="Gracias-por-registra" style="width: 333px;
              height: 20px;
              font-size: 16px;
              letter-spacing: 0.5px;
              text-align: center;
              color: #3d4548;
              margin-left: 129px;
              margin-top: 25px;">Gracias por registrarte a BangBang Lab</p>
            </div>
          </div>
          <p class="Copyrigth-2017-Ban" style="width: 424px;
          height: 16px;
          font-size: 13px;
          text-align: right;
          color: #8f9ca1;
          margin-left: 236px;
          margin-top: 47px;">Copyrigth © 2017 BangBang Lab. Todos los derechos reservados.</p>
        </div>
      </body>
    </html>"""
    mail = EmailMultiAlternatives('prueba' , 'prueba de correo' , settings.EMAIL_HOST_USER , to=['bandviceversa@gmail.com'])
    mail.attach_alternative(contenido, 'text/html')
    mail.send()
    return HttpResponse("ok")

def recuperacion_view(request):
    return render(request,"views/partials/recuperacion.html")

def nuevaContrasena_view(request):
    return render(request,"views/partials/nuevaContrasea.html")

def avisoNuevaContrasea_view(request):
    return render(request,"views/partials/avisoNuevaContrasea.html")

def evaluaciones(request, tipo):
    if request.user.is_authenticated():
        username = request.user.username
        usuario = User.objects.get(username = username )
        datos = Usuarios.objects.filter(user_id=usuario.id).first()
        if datos.tipoUser_id == 1:
            if tipo == "grupo":
                idsGrupo = []
                usuario_grupo = Usuario_Grupos.objects.filter(id_usuario_id = usuario.id)
                for data in usuario_grupo:
                    datos_grupo = Grupos.objects.filter(pk=data.id_grupo_id).first()
                    if datos_grupo is not None:
                        if datos_grupo.activo == True:
                            idsGrupo.append([datos_grupo.pk, datos_grupo.grupos])
                return render(request,"views/partials/evaluacionesAlumnoG.html", {'grupos':idsGrupo})
            elif tipo == "tarea":
                    array_tareas = []
                    tareasDeUsuario = Usuario_Tareas.objects.filter(id_usuario_id = usuario.id)
                    for tarea in tareasDeUsuario:
                        detalles_tarea = Tarea.objects.filter(pk = tarea.id_tarea_id).first()
                        detalles_grupo = Grupos.objects.filter(pk = detalles_tarea.grupo_id).first()
                        if detalles_grupo.activo == True:
                            array_tareas.append([detalles_tarea.pk,
                                tarea.estatus, detalles_tarea.nombre_tarea, detalles_grupo.grupos, detalles_grupo.pk])
                    return render(request,"views/partials/evaluacionesAlumnoT.html", {'grupos':array_tareas})
        elif datos.tipoUser_id == 2:
            if tipo == "grupo":
                grupos = []
                grupo = Grupos.objects.filter(usuario_id=usuario.id)
                for data in grupo:
                    nombre_grupo = data.grupos
                    if len(nombre_grupo) > 14:
                        nombre_grupo = nombre_grupo[:14] + "..."
                    if data.activo == True:
                        grupos.append([data.pk, nombre_grupo])
                return render(request,"views/partials/evaluacionesMaestroG.html", {'grupos':grupos})
            elif tipo == "tarea":
                tareas = []
                grupo = Grupos.objects.filter(usuario_id=usuario.id)
                for data in grupo:
                    tarea = Tarea.objects.filter(grupo_id=data.pk)
                    for data2 in tarea:
                        nombre_tarea = data2.nombre_tarea
                        tareas.append([data2.pk, nombre_tarea])
                return render(request,"views/partials/evaluacionesMaestroT.html", {'grupos':tareas})
            elif tipo == "alumno":
                alumno_array = []
                grupo = {}
                array_aux = []
                grupos = Grupos.objects.filter(usuario_id=usuario.id, activo=1)
                cont = 0
                if grupos.exists():
                    for i in grupos:
                        grupo[cont] = grupos.values()[cont]
                        usuarios_aux = Usuario_Grupos.objects.filter(id_grupo_id=grupo[cont]['id'])
                        for datos in usuarios_aux:
                            if datos.id_usuario_id not in array_aux:
                                usuario = User.objects.get(pk = datos.id_usuario_id)
                                alumno_array.append({"id":datos.id_usuario_id, "nombre":usuario.first_name+' '+usuario.last_name, "email":usuario.username})
                                array_aux.append(datos.id_usuario_id)
                        cont += 1
                return render(request,"views/partials/evaluacionesMaestroA.html", {'grupos':alumno_array})
    else:
        return redirect('/')

