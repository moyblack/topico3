"""ide URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from app.sistema import views
urlpatterns = [
    url(r'^$', views.login_view, name="login_view"),
    url(r'^login$', views.login_view, name="login_view"),
    url(r'^registro$', views.registro_view, name="registro_view"),
    url(r'^bienvenido$', views.bienvenido_view, name="bienvenido_view"),
    url(r'^recuperar$', views.recuperar_view, name="recuperar_view"),
    url(r'^registro/normal$', views.registro_normal, name="registro_normal"),
    url(r'^login/normal$', views.login_autentificar, name="login_autentificar"),
    url(r'^cerrar_sesion$', views.cerrar_sesion, name="cerrar_sesion"),
    url(r'^sendMail$', views.sendMail, name="sendMail"),
    url(r'^inicio_alumno$', views.alumno_view, name="alumno_view"),
    url(r'^inicio_maestro$', views.maestro_view, name="maestro_view"),
    url(r'^inicio_admin$', views.admin_view, name="admin_view"),
    url(r'^avisoNuevaContrasea_view$', views.avisoNuevaContrasea_view, name="avisoNuevaContrasea_view"),
    url(r'^perfil$', views.perfil_view, name="perfil_view"),
    url(r'^recuperacion_view$', views.recuperacion_view, name="recuperacion_view"),
    url(r'^nuevaContrasena_view$', views.nuevaContrasena_view, name="nuevaContrasena_view"),
    url(r'^perfil/upload$', views.perfil_upload, name="perfil_upload"),
    url(r'^perfil/fotoUpload$', views.perfil_fotoUpload, name="perfil_fotoUpload"),
    url(r'^evaluaciones/(?P<tipo>[\w\-]+)$', views.evaluaciones, name="evaluaciones"),
]
