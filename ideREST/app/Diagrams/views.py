from django.shortcuts import render
import lecturaPython
from django.http import HttpResponse, HttpResponseBadRequest
import json
from IDE.settings import BASE_DIR
import os

def getfile(exp, gpo, tarea, fileName):
    dirname = os.path.join(BASE_DIR,"media/documents/"+exp+"/"+gpo+"/"+tarea+"/")
    if not os.path.exists(os.path.join(dirname, str(fileName+".py"))):
        return 'Archivo no existe'
    else:
        try:
            project = Proyectos.objects.get(expediente=str(exp), grupo=str(gpo), ideTarea=str(tarea))
            if project.status == 0:
                return 'Proyecto No Accesible'
        except:
            for file in os.listdir(dirname):
                if file.startswith(str(fileName)):
                    f = open(os.path.join(dirname, str(fileName+".py")), "r")
                    code = str(f.readlines())
                    f.close()
                    return code

def getproject(exp, gpo, tarea):
    dirname = os.path.join(BASE_DIR,"media/documents/"+exp+"/"+gpo+"/"+tarea+"/")
    if not os.path.exists(dirname):
        return 'Nombre de Proyecto no encontrado'
    else:
        try:
            project = Proyectos.objects.get(expediente=str(exp), grupo=str(gpo), ideTarea=str(tarea))
            if project.status == 0:
                return 'Proyecto No Accesible'
            else:
                files = []
                for file in os.listdir(str(dirname)):
                    fileName = os.path.splitext(file)[0]
                    files.append(fileName)
                data = {"records":{"project":files}}
                return data
        except:
            files = []
            for file in os.listdir(str(dirname)):
                fileName = os.path.splitext(file)[0]
                files.append(fileName)
            data = {"records":{"project":files}}
            return data

# Create your views here.
def getCode(request, exp, gpo, tarea):
    arregloJson = ''
    try:
        response = getproject(exp, gpo, tarea)
        nuevoj=response["records"]
        nuevoj = nuevoj["project"]
        lector = lecturaPython
        if len(nuevoj) == 1:
            response = getfile(exp, gpo, tarea, nuevoj[0])
            lector.setCodigo(response)
            JsonCodigo = lector.obtenerJson()
            JsonCodigo=JsonCodigo[1:len(JsonCodigo)-1]
            arregloJson = arregloJson + ',' +JsonCodigo
        else:
            for i in range(0, len(nuevoj)-1):
                response = getfile(exp, gpo, tarea, nuevoj[i])
                lector.setCodigo(response)
                JsonCodigo = lector.obtenerJson()
                JsonCodigo=JsonCodigo[1:len(JsonCodigo)-1]
                arregloJson = arregloJson + ',' +JsonCodigo
        return HttpResponse("["+arregloJson+"]")
    except:
        return HttpResponse("<h1>No Respondio</h1>")

def setCode(code):
    print code
    return 0;


def prueba(request):
    return "Holamundo"
def getJson(request):
    try:
        return 0
    except:
        return 0
    return 0
