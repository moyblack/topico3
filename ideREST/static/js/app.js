/*
-Configuracion del IDE cargando la libreria del editor ACE, el controlador del IDE, los servicios y directivas
-Cambiamos la configuracion inciial angular para que no interfiera con la forma de mostrar datos de angular 
	por eso el interpolateProvider definiendo "[[ ]]" como la forma de mostrar datos
-Damos de alta los x-crsfToken para que al enviar formularios podamos mandar esos tokens garantizando la seguridad de el envio

*/
angular.module("IDE", ['ui.ace','IDE.contIde','IDE.services','IDE.directives'])
.config(function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[').endSymbol(']]');
})
.config(function($httpProvider){ 
    $httpProvider.defaults.headers.common['X-CSRFToken'] = 'csrf_token';
    $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
})
.constant('ipServer','http://127.0.0.1:8000')
