angular.module('IDE.services', [])
.factory('GetFiles', function($http, ipServer) {
//regresa los nombres de los archivos de un proyecto asugnado a un alumno en un grupo en un arreglo 
    var obtainfiles = function(exp,gpo,tarea) {
        return $http.get(ipServer+'/api/getproject/'+exp+'/'+gpo+'/'+tarea).then(function(response){
            var resp =response.data.records;
                return String(resp[0].project);
            }, function(response){
                var resp = "Ocurrio un problema"
                return resp;
            });
    };
     //regresa la configuracion del ide que se haya definido por el maestro en una tarea
    var obtainconfig = function(tarea) {
      return $http.get(ipServer+'/tareas/get_config_ide?idTarea='+tarea).then(function(response){
        return response;
     }, function(response){
        return response;
     });
    };
      //regresa las metricas pasadas si es que existen
    var getPastMetrics = function(exp,gpo,tarea) {
      return $http.get(ipServer+'/ide/getPastMetricas/'+exp+'/'+gpo+'/'+tarea).then(function(response){
        return response;
     }, function(response){
        return response;
     });
    };

     //regresa la suma de locs y deectos anteriores solo si hay almenos 1 registro
    var getProyeccion = function(exp,gpo,tarea) {
      return $http.get(ipServer+'/ide/getProyeccionDefectos/'+exp+'/'+gpo+'/'+tarea).then(function(response){
        return response;
     }, function(response){
        return response;
     });
    };

    //salir Ide
    var salirIde = function (exp, gpo, tarea)
    {
        return $http.get(ipServer+'/ide/salir/'+exp+'/'+gpo+'/'+tarea).then(function(response){
            return response;
            }, function(response){
                return response;
            });
    };

    //entregar
     var entregar = function (exp, gpo, tarea)
    {
        return $http.get(ipServer+'/ide/entregar/'+exp+'/'+gpo+'/'+tarea).then(function(response){
            return response;
            }, function(response){
                return response;
            });
    };

    //saveMetricas
     var saveMetricas = function (exp, gpo, tarea, variables)
    {
         return $http.post(ipServer+'/ide/insertmetricas/'+exp+'/'+gpo+'/'+tarea,{body:variables},headers = {'content-type': 'application/json'}).then(function(response){
            return response;
        }, function(response){
             return response;
        });
    };


    //savecode
    var savecode = function(exp, gpo, tarea, file, code)
    {
        return $http.post(ipServer+'/api/savecode/'+exp+'/'+gpo+'/'+tarea+'/'+file, code ).then(function(response){
        return response;
     }, function(response){
        return response;
     });
    };

    //obtiene el codigo de cada archivo segun el nombre del id del idUsuario/idGrupo/idTarea/nombredelarchivo
     var obtaincode = function(exp,gpo,tarea,filename) {
        return $http.get(ipServer+'/api/getfile/'+exp+'/'+gpo+'/'+tarea+'/'+filename).then(function(response){
            var resp =response.data.records;
            return String(resp[0].code);
            }, function(response){
                var resp = "Ocurrio un problema"
                return resp;
            });
    };

    //crea un archivo nuevo en el proyecto del alumno idUsuario/idGrupo/idTarea/nombredelarchivo
     var setNewTheme = function(exp, newTheme) {
       return $http.get(ipServer+'/ide/setNewTheme/'+exp+'/'+newTheme).then(function(response){
        return response;
     }, function(response){
        return response;
     });
    };

    //crea un archivo nuevo en el proyecto del alumno idUsuario/idGrupo/idTarea/nombredelarchivo
     var createfile = function(exp,gpo,tarea,filename) {
        return $http.get(ipServer+'/api/createfile/'+exp+'/'+gpo+'/'+tarea+'/'+filename).then(function(response){
                return response.data;
            }, function(response){
                return response.data;
            });
    };

     //borra un archivo en el proyecto del alumno idUsuario/idGrupo/idTarea/nombredelarchivo
    var deletefile = function(exp,gpo,tarea,filename) {
        return $http.get(ipServer+'/api/deletefile/'+exp+'/'+gpo+'/'+tarea+'/'+filename).then(function(response){
                return response.data;
            }, function(response){
                return response.data;
            });
    };
     //renombra un archivo en el proyecto del alumno idUsuario/idGrupo/idTarea/nombredelarchivo
    var renamefile = function(exp,gpo,tarea,filename,newFileName) {
        return $http.get(ipServer+'/api/renamefile/'+exp+'/'+gpo+'/'+tarea+'/'+filename+'/'+newFileName).then(function(response){
                return response.data;
            }, function(response){
                return response.data;
            });
    };



    
    return { obtainfiles: obtainfiles,  obtaincode:obtaincode, createfile: createfile, deletefile: deletefile, renamefile: renamefile, 
        obtainconfig: obtainconfig, savecode: savecode, entregar:entregar, salirIde:salirIde, saveMetricas: saveMetricas,
        getPastMetrics:getPastMetrics, getProyeccion:getProyeccion, setNewTheme: setNewTheme, };

})
.factory('Metricas',function () 
{ 

    var data = 
    {
        ContadorOperadoresDistintos : 0,
        ContadorOperandosDistintos : 0,
        SumaContadorOperadores : 0,
        SumaContadorOperandos : 0,
        vocabulario : 0,
        longitud : 0,
        volumen : 0,
        volumenPotencial : 0,
        nivel : 0,
        dificultad : 0,
        inteligencia : 0,
        esfuerzo : 0,
        tiempoTotalProduccion : 0,
        tiempoPruebas : 0,
        tiempoCorreciones : 0,
        tiempoTotalProducto : 0,
        LOCbaseOriginal : 0,
        LOCagregado : 0,
        LOCmodificado : 0, 
        LOCsuprimido : 0,
        locReutilizado : 0, //codigo pegado siempre 0
        nuevaReutilizacion : 0, //codigo pegado y cambiado siempre 0 no se puede pegar
        LocNuevoCambiante : 0,
        LocActualS:0,
        LocTotal : 0,
        defectos : 0,
        defectosS : 0,
        defectosCorregidos : 0,
        defectosNoCorregidos : 0,
        defectosPorTamano : 0,
        defectosPorHora : 0,
        defectosCorregidosHora : 0,
        defectosPorHoraEnPruebas : 0, 
        defectoscorregidosTotalCorrecciones : 0,
        densidadDeDefectos : 0, 
        ProyeccionFuturosDefectos : 0, //
        complejidadCiclo : 0,
    };

    return {
        getContadorOperadoresDistintos: function () 
        {
            return data.ContadorOperadoresDistintos;
        },
        setContadorOperadoresDistintos: function (newValue) 
        {
            data.ContadorOperadoresDistintos = newValue;
        },
         getContadorOperandosDistintos: function () 
        {
            return data.ContadorOperandosDistintos;
        },
        setContadorOperandosDistintos: function (newValue) 
        {
            data.ContadorOperandosDistintos = newValue;
        },
        getSumaContadorOperadores: function () 
        {
            return data.SumaContadorOperadores;
        },
        setSumaContadorOperadores: function (newValue) 
        {
            data.SumaContadorOperadores = newValue;
        },
        getSumaContadorOperandos: function () 
        {
            return data.SumaContadorOperandos;
        },
        setSumaContadorOperandos: function (newValue) 
        {
            data.SumaContadorOperandos = newValue;
        },
         getVocabulario: function () 
        {
            return data.vocabulario;
        },
        setVocabulario: function (newValue) 
        {
            data.vocabulario = newValue;
        },
        setVocabulario: function (operadoresDistintos, operandosDistintos) 
        {
            data.vocabulario = operadoresDistintos + operandosDistintos;
        },
        getLongitud: function () 
        {
            return data.longitud;
        },
        setLongitud: function (newValue) 
        {
            data.longitud = newValue;
        },
         setLongitud: function (sumaOperadores, sumaOperandos) 
        {
            data.longitud = sumaOperadores + sumaOperandos;
        },
         getVolumen: function () 
        {
            return data.volumen;
        },
        setVolumen: function (newValue) 
        {
            data.volumen = newValue;
        },
         setVolumen: function (longitud, vocabulario) 
        {
            data.volumen = longitud * Math.log2(vocabulario);
        },
         getVolumenPotencial: function () 
        {
            return data.volumenPotencial;
        },
        setVolumenPotencial: function (newValue) 
        {
            data.volumenPotencial = newValue;
        },
        setVolumenPotencial: function (operadoresDistintos, operandosDistintos) 
        {
            data.volumenPotencial = (operadoresDistintos + operandosDistintos) * ( Math.log2(operadoresDistintos + operandosDistintos));
        },
         getNivel: function () 
        {
            return data.nivel;
        },
        setNivel: function (newValue) 
        {
            data.nivel = newValue;
        },
        setNivel: function (volumenPotencial, volumen) 
        {
            data.nivel = volumenPotencial / volumen;
        },
         getDificultad: function () 
        {
            return data.dificultad;
        },
        setDificultad: function (newValue) 
        {
            data.dificultad = newValue;
        },
        setDificultad: function (n, nivel) 
        {
            data.dificultad = n/nivel;
        },
        getInteligencia: function () 
        {
            return data.inteligencia;
        },
        setInteligencia: function (newValue) 
        {
            data.inteligencia = newValue;
        },
        setInteligencia: function (nivel, volumen) 
        {
            data.inteligencia = nivel * volumen;
        },
        getEsfuerzo: function () 
        {
            return data.esfuerzo;
        },
        setEsfuerzo: function (newValue) 
        {
            data.esfuerzo = newValue;
        },
        setEsfuerzo: function (volumen, nivel) 
        {
            data.esfuerzo = volumen / nivel;
        },
        getTiempoTotalProduccion: function () 
        {
            return data.tiempoTotalProduccion;
        },
        setTiempoTotalProduccion: function (newValue) 
        {
            data.tiempoTotalProduccion = newValue;
        },
        getTiempoPruebas: function () 
        {
            return data.tiempoPruebas;
        },
        setTiempoPruebas: function (newValue) 
        {
            data.tiempoPruebas = newValue;
        },
        getTiempoCorreciones: function () 
        {
            return data.tiempoCorreciones;
        },
        setTiempoCorreciones: function (newValue) 
        {
            data.tiempoCorreciones = newValue;
        },
        getTiempoTotalProducto: function () 
        {
            return data.tiempoTotalProducto;
        },
        setTiempoTotalProducto: function (newValue) 
        {
            data.tiempoTotalProducto = newValue;
        },
        getLOCbaseOriginal: function () 
        {
            return data.LOCbaseOriginal;
        },
        setLOCbaseOriginal: function (newValue) 
        {
            data.LOCbaseOriginal = newValue;
        },
        getLOCagregado: function () 
        {
            return data.LOCagregado;
        },
        setLOCagregado: function (newValue) 
        {
            data.LOCagregado = newValue;
        },
        getLOCmodificado: function () 
        {
            return data.LOCmodificado;
        },
        setLOCmodificado: function (newValue) 
        {
            data.LOCmodificado = newValue;
        },
        getLOCsuprimido: function () 
        {
            return data.LOCsuprimido;
        },
        setLOCsuprimido: function (newValue) 
        {
            data.LOCsuprimido = newValue;
        },
        getLocReutilizado: function () 
        {
            return data.locReutilizado;
        },
        setLocReutilizado: function (newValue) 
        {
            data.locReutilizado = newValue;
        },
        getNuevaReutilizacion: function () 
        {
            return data.nuevaReutilizacion;
        },
        setNuevaReutilizacion: function (newValue) 
        {
            data.nuevaReutilizacion = newValue;
        },
        getLocNuevoCambiante: function () 
        {
            return data.LocNuevoCambiante;
        },
        setLocNuevoCambiante: function (newValue) 
        {
            data.LocNuevoCambiante = newValue;
        },
         getLocActualS: function () 
        {
            return data.LocActualS;
        },
        setLocActualS: function (newValue) 
        {
            data.LocActualS = newValue;
        },
        getLocTotal: function () 
        {
            return data.LocTotal;
        },
        setLocTotal: function (newValue) 
        {
            data.LocTotal = newValue;
        },
        getDefectos: function () 
        {
            return data.defectos;
        },
        setDefectos: function (newValue) 
        {
            data.defectos = newValue;
        },
        getDefectosS: function () 
        {
            return data.defectosS;
        },
        setDefectosS: function (newValue) 
        {
            data.defectosS = newValue;
        },
        getDefectosCorregidos: function () 
        {
            return data.defectosCorregidos;
        },
        setDefectosCorregidos: function (newValue) 
        {
            data.defectosCorregidos = newValue;
        },
        getDefectosNoCorregidos: function () 
        {
            return data.defectosNoCorregidos;
        },
        setDefectosNoCorregidos: function (newValue) 
        {
            data.defectosNoCorregidos = newValue;
        },
        getDefectosPorTamano: function () 
        {
            return data.defectosPorTamano;
        },
        setDefectosPorTamano: function (newValue) 
        {
            data.defectosPorTamano = newValue;
        },
        getDefectosPorHora: function () 
        {
            return data.defectosPorHora;
        },
        setDefectosPorHora: function (newValue) 
        {
            data.defectosPorHora = newValue;
        },
        getDefectosCorregidosHora: function () 
        {
            return data.defectosCorregidosHora;
        },
        setDefectosCorregidosHora: function (newValue) 
        {
            data.defectosCorregidosHora = newValue;
        },
        getDefectosPorHoraEnPruebas: function () 
        {
            return data.defectosPorHoraEnPruebas;
        },
        setDefectosPorHoraEnPruebas: function (newValue) 
        {
            data.defectosPorHoraEnPruebas = newValue;
        },
        getDefectoscorregidosTotalCorrecciones: function () 
        {
            return data.defectoscorregidosTotalCorrecciones;
        },
        setDefectoscorregidosTotalCorrecciones: function (newValue) 
        {
            data.defectoscorregidosTotalCorrecciones = newValue;
        },
        getDensidadDeDefectos: function () 
        {
            return data.densidadDeDefectos;
        },
        setDensidadDeDefectos: function (newValue) 
        {
            data.densidadDeDefectos = newValue;
        },
        getProyeccionFuturosDefectos: function () 
        {
            return data.ProyeccionFuturosDefectos;
        },
        setProyeccionFuturosDefectos: function (newValue) 
        {
            data.ProyeccionFuturosDefectos = newValue;
        },
        getComplejidadCiclo: function () 
        {
            return data.complejidadCiclo;
        },
        setComplejidadCiclo: function (newValue) 
        {
            data.complejidadCiclo = newValue;
        },
    };
})