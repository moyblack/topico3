angular.module('IDE.contIde', [])

.controller('ctrlIDE', function($scope, $http, $location, $timeout, GetFiles, $window, Metricas, $interval)
{
    $scope.aceLoaded = function (_editor)
    {
     	var _session = _editor.getSession();
    	var _renderer = _editor.renderer;

    	_editor.$blockScrolling = Infinity;
    	_editor.setShowPrintMargin(false);
    	// Events
     	_editor.on('mousedown', function(e) {
      	var row = e.getDocumentPosition().row;
      	var col = e.getDocumentPosition().column;
      	var token = _editor.session.getTokenAt(row, col);
      	if(token != null)
      	{
      		$scope.STInfo(String(token.type));
      	}
    	});
     	//detecta cualquier cambio en el editor
     	_editor.on("change", function(e)
     	{
       		$scope.uptdateCodigo(String(_editor.session.getValue()));
       		$scope.LOCbaseActual = _editor.session.getLength();
      		var codigo = _editor.getValue();
      		var currline = _editor.getSelectionRange().start.row;
      		var lineaActual = _editor.session.getLine(currline);
      		//////////////////////////////////////////////////////////////////////////////
        	//Contador de operaciones
      		$scope.aCO = {}; // -> arrayContadorOperadores
      		//guarda las variables
      		var tiposDeVariable=[];
       		lineaActual = lineaActual.replace(/^\s+|\s+$/g, "");
       		if(lineaActual.charAt(0).trim() != "#")// si no es un comentario
       		{ 
         		if(~lineaActual.indexOf("=") && !~lineaActual.indexOf("=="))
         		{ // detecta la asignación de un valor
           			var variable = lineaActual.split("=")[0].trim(); // obtiene todo lo que está antes de un signo de igual
             		//para obtener los tipos de variables
             		var tipoVariable = lineaActual.split("=")[1].trim(); // obtiene todo lo que está después de un signo de igual
             		var charAux = tipoVariable.charAt(0);
             		if((charAux == "'" || charAux == '"') && !~tiposDeVariable.indexOf('string')) tiposDeVariable.push("string");
             		else if((charAux == "[") && !~tiposDeVariable.indexOf('list')) tiposDeVariable.push("list");
             		else if((charAux == "(") && !~tiposDeVariable.indexOf('tuple')) tiposDeVariable.push("tuple");
             		else if((charAux == "{") && !~tiposDeVariable.indexOf('dictionary')) tiposDeVariable.push("dictionary");
             		else if((!isNaN(charAux)) && !~tiposDeVariable.indexOf('numeric')) tiposDeVariable.push("numeric");
           			var varTemp = variable.split(" ", -1); //-> obtiene cualquier cosa antes de un signo de igual
           			varTemp = varTemp.pop();
           			var a = $scope.operandos.indexOf(varTemp);  //busca la variable en el arreglo existente
             		$scope.operandos2.push(varTemp);
             		if(!~a && varTemp != "")                 // si no existe aún la variable, la agrega
                 		$scope.operandos.push(varTemp);
         		}
         		else
         		{
           			//console.log("No variables found");
           		}
       		}
       		tiposDeVariable.sort();
       		Metricas.setContadorOperandosDistintos(tiposDeVariable.length);
      		var contador;
      		//esto busca las veces que se ha usado una variable guardada y las cuenta
      		var aux = "";
      		var cantidadOperandos = 0;
      		$scope.ContadorOperandos = [];
      		for(i = 0; i < $scope.operandos.length; i++ )
      		{
        		aux = $scope.operandos[i];
        		cantidadOperandos = countInstances(codigo, aux);
        		$scope.ContadorOperandos.push(cantidadOperandos);
      		}
        	/////////////////////////////////////////////////////
        	//cada enter
            document.onkeyup = function(e)
            {
                var keycode = (e === null) ? window.event.keyCode : e.which;
                switch(keycode)
   				{
      				case 8: // backspace
      				if($scope.LOCbaseActual < Metricas.getLocActualS())
      				{
      					if(Metricas.getLocActualS() > 0)
      					{
                    	Metricas.setLOCsuprimido(Metricas.getLOCsuprimido() +(Metricas.getLocActualS() - $scope.LOCbaseActual))
                    	}
              }
      				break; 
      				case 46: // delete
      				if($scope.LOCbaseActual < Metricas.getLocActualS())
      				{
                    	if(Metricas.getLocActualS() > 0)
      					{
                    	Metricas.setLOCsuprimido(Metricas.getLOCsuprimido() +(Metricas.getLocActualS() - $scope.LOCbaseActual))
                    	}
                  	}
      				break;
      				case 13: // enter
      				if($scope.LOCbaseActual > $scope.maximoLineas)
      				{
                    	alert("Lineas excedidas");
                  	}
                  	else 
                  	{
                    	$scope.saveCode();
                    	Metricas.setLocActualS($scope.LOCbaseActual);
                    	 //loc agregado                                 
						if(Metricas.getLOCbaseOriginal()< $scope.LOCbaseActual)
						{
              				Metricas.setLOCagregado(Metricas.getLOCagregado()+($scope.LOCbaseActual - Metricas.getLOCbaseOriginal()));
						}
          				//locModificado
          				if(Metricas.getLOCbaseOriginal() > 0 )
          				{
          					if($scope.LOCbaseActual  == Metricas.getLOCbaseOriginal())
          					{
          						Metricas.setLOCmodificado(Metricas.getLOCmodificado()++);
          					}   
          				}
          				Metricas.setLocNuevoCambiante(Metricas.getLocNuevoCambiante()+(Metricas.getLOCagregado() + Metricas.getLOCmodificado()))
          				Metricas.setLocTotal( Metricas.getLocTotal()+ ($scope.LOCbaseActual - Metricas.getLOCsuprimido()));
                  	}
      				break;
      				default:
     				break;
   				}
            }
            document.oncontextmenu = new Function("return false");
			function disableSelectCopy(e) 
			{
			 var pressedKey = String.fromCharCode(e.keyCode).toLowerCase();
			 if (e.ctrlKey && (pressedKey == "c" || pressedKey == "x" || pressedKey == "v" || pressedKey == "a")) {
			 	return false;
			 }
			}
			document.onkeydown = disableSelectCopy;
	        /////////////////////////////////////////////////////
	      	$scope.aCO['ContadorFindMas']        = countInstances(codigo,"+");
	      	$scope.aCO['ContadorFindMenos']      = countInstances(codigo,"-");
	      	$scope.aCO['ContadorFindPor']        = countInstances(codigo,"*");
	      	$scope.aCO['ContadorFindEntre']      = countInstances(codigo,"/");
	      	$scope.aCO['ContadorFindPotencia']   = countInstances(codigo,"**");
	      	$scope.aCO['ContadorFindModulo']     = countInstances(codigo,"%");
	      	$scope.aCO['ContadorFindOR']         = countInstances(codigo," or ");
	      	$scope.aCO['ContadorFindAND']        = countInstances(codigo," and ");
	      	$scope.aCO['ContadorFindNOT']        = countInstances(codigo," not ");
	      	$scope.aCO['ContadorFindCompara']    = countInstances(codigo,"==");
	     	   $scope.aCO['ContadorFindAsigna']     = countInstances(codigo,"=");
	      	$scope.aCO['ContadorFindMayor']      = countInstances(codigo,">");
	      	$scope.aCO['ContadorFindMenor']      = countInstances(codigo,"<");
	      	$scope.aCO['ContadorFindDiferente']  = countInstances(codigo,"!=");
	      	$scope.aCO['ContadorFindMayorIgual'] = countInstances(codigo,">=");
	      	$scope.aCO['ContadorFindMenorIgual'] = countInstances(codigo,"<=");
	      	imprimir();
	      	function countInstances(str, value) 
	      	{
	        	return (str.split(value).length - 1);
	      	}
      	
	      //Complejidad Ciclomática
	      if(lineaActual.charAt(0).trim() != "#")
	      {
	        var complejidadIf    = countInstances(codigo, "if");
	        var complejidadElse  = countInstances(codigo, "else");
	        var complejidadFor   = countInstances(codigo, "for");
	        var complejidadWhile = countInstances(codigo, "while");
	      }
	      Metricas.setComplejidadCiclo(complejidadIf + complejidadWhile + complejidadElse + complejidadFor + 1);
	      function imprimir()
	      {
	          //reset de valores
	          Metricas.setSumaContadorOperadores(0);
	          Metricas.setSumaContadorOperandos(0);
	          Metricas.setContadorOperadoresDistintos(0);
	          Metricas.setVocabulario(0);
	          Metricas.setLongitud(0);
	          Metricas.setVolumen(0);

	          if($scope.aCO['ContadorFindCompara']   >=1) $scope.aCO['ContadorFindAsigna'] -= 2*$scope.aCO['ContadorFindCompara'];
	          if($scope.aCO['ContadorFindDiferente'] >=1) $scope.aCO['ContadorFindAsigna'] -= $scope.aCO['ContadorFindDiferente'];
	          if($scope.aCO['ContadorFindPotencia']  >=1) $scope.aCO['ContadorFindPor']    -= 2*$scope.aCO['ContadorFindPotencia'];
	          if($scope.aCO['ContadorFindMayorIgual']>=1){$scope.aCO['ContadorFindAsigna'] -= $scope.aCO['ContadorFindMayorIgual'];
	                                               $scope.aCO['ContadorFindMayor']  -= $scope.aCO['ContadorFindMayorIgual'];}
	          if($scope.aCO['ContadorFindMenorIgual']>=1){$scope.aCO['ContadorFindAsigna'] -= $scope.aCO['ContadorFindMenorIgual'];
	                                               $scope.aCO['ContadorFindMenor']  -= $scope.aCO['ContadorFindMenorIgual']};
	         for( var contador in $scope.aCO ){
	              Metricas.setSumaContadorOperadores($scope.aCO[contador]);
	              if($scope.aCO[contador] != 0) Metricas.setContadorOperadoresDistintos((Metricas.getContadorOperadoresDistintos()+ 1));
	          }
	           for(i = 0; i < $scope.ContadorOperandos.length; i++ ){
	            Metricas.setSumaContadorOperandos($scope.ContadorOperandos[i]);
	          }
	          Metricas.setVocabulario(Metricas.getContadorOperadoresDistintos(), Metricas.getContadorOperandosDistintos());
	          Metricas.setLongitud(Metricas.getSumaContadorOperadores(), Metricas.getSumaContadorOperandos());
	          Metricas.setVolumen(Metricas.getLongitud(),Metricas.getVocabulario());
	          Metricas.setVolumenPotencial(Metricas.getContadorOperadoresDistintos(), Metricas.getContadorOperandosDistintos());
	          Metricas.setNivel(Metricas.getVolumenPotencial(), Metricas.getVolumen());
	          Metricas.setDificultad(1 , Metricas.getNivel());
	          Metricas.setInteligencia(Metricas.getNivel(), Metricas.getVolumen());
	          Metricas.setEsfuerzo(Metricas.getVolumen(),Metricas.getNivel());
	    	    Metricas.setProyeccionFuturosDefectos((1000 * $scope.defectosAnteriores) / $scope.locsanteriores)
	        }
    	})
  }
  $scope.aceChanged = function (_editor) {

  }
  $scope.init = function(exp, gpo, tarea, tema)
  {
    //Esconder ventana de archivos
    $scope.shwAdmin = true;
    $scope.habracorrecciones = false;
    $scope.exp = exp;
    $scope.gpo = gpo;
    $scope.tarea = tarea;
    $scope.currentTheme  = tema;

     $scope.themes = [{id:0, theme:"chrome"},{id:1, theme:"clouds"},{id:2, theme:"crimson_editor"},{id:3, theme:"dawn"},{id:4, theme:"dreamweaver"},
    {id:5, theme:"eclipse"},{id:6, theme:"github"},{id:7, theme:"solarized_light"},{id:8, theme:"textmate"},{id:9, theme:"tomorrow"},{id:10, theme:"xcode"},
    {id:11, theme:"kuroir"},{id:12, theme:"katzenmilch"},{id:13, theme:"ambiance"},{id:14, theme:"chaos"},{id:15, theme:"cobalt"},{id:16, theme:"idle_fingers"},
    {id:17, theme:"kr_theme"},{id:18, theme:"merbivore"},{id:19, theme:"merbivore_soft"},{id:20, theme:"mono_industrial"},{id:21, theme:"clouds_midnight"},
    {id:22, theme:"monokai"},{id:23, theme:"pastel_on_dark"},{id:24, theme:"solarized_dark"},{id:25, theme:"terminal"},{id:26, theme:"tomorrow_night"},
    {id:27, theme:"tomorrow_night_blue"},{id:28, theme:"tomorrow_night_bright"},{id:29, theme:"tomorrow_night_eighties"},{id:30, theme:"twilight"},
    {id:31, theme:"vibrant_ink"},
  ];    
    $scope.operandos = []; // -> arrayOperandos
    $scope.operandos2 = []; // -> arrayOperandos
    var _session = null;
    $scope.tokenInformation = "";

    $scope.Proyeccion();
    $scope.getPastMetrics();
    $scope.getConf();
    $scope.getFiles();

    $scope.blockedit =true;
    $scope.puedeeditar = true;
  }
  $scope.actTheme = function()
  {
    $scope.editor.theme = $scope.selectedTheme.theme;
    $scope.currentTheme = $scope.selectedTheme.id;
     var recieved_data = GetFiles.setNewTheme($scope.exp, $scope.currentTheme);
        recieved_data.then(
            function(fulfillment){
               
            }, function(){
                 //sweetAlert("Error", "Ocurrio un problema", "error");
        });
  }
  $scope.split = function()
  {
    if ($scope.editor.splitMode)
    {
        $scope.editor.splitMode = false;
        $scope.create();
    }
    else
    {
        $scope.editor.splitMode = true
        $scope.dany();
    }

  }
  $scope.create =function()
  {
    cv = document.getElementById('gfx_holder')
    cv.parentNode.removeChild(cv)
      document.getElementById('preview').style.display="none"
  }
  $scope.dany = function()
  {
    canv = document.createElement('div');
    canv.id="gfx_holder"
    document.getElementById('preview').style.display="block"
    document.querySelector('.editor').appendChild(canv)

    ajaxUML('http://127.0.0.1:8000/diagram/getnotification_dos/'+$scope.exp+'/'+$scope.gpo+'/'+$scope.tarea);
  }
  $scope.letraG = function()
  {
    $scope.editor.fontS = 24;
  }

  $scope.letraN = function()
  {
    $scope.editor.fontS = 16;
  }

  $scope.letraC =  function()
  {
    $scope.editor.fontS = 10;
  }
   $scope.STInfo = function(tokenInfo)
   {
     $scope.$apply(function () {
            $scope.tokenInformation = tokenInfo;
        });
   }
  $scope.saveCode = function()
  {
    var code = $scope.codigo;
    var variablesToSend = {c: $scope.codigo};
    var recieved_data = GetFiles.savecode($scope.exp,$scope.gpo, $scope.tarea, String($scope.files[$scope.tab - 1]), variablesToSend);
        recieved_data.then(
            function(fulfillment){
            	$scope.saveMetricas();
       			$scope.dany();
       			$scope.create();
               
            }, function(){
                 //sweetAlert("Error", "Ocurrio un problema", "error");
        });
  }
    $scope.getFiles = function()
    {
        var recieved_data = GetFiles.obtainfiles($scope.exp,$scope.gpo, $scope.tarea);

        recieved_data.then(
            function(fulfillment){
                if(String(fulfillment) != "Ocurrio un problema")
                        {
                            var files = fulfillment;
                            $scope.files = files.split(",");
                            $scope.numFiles = $scope.files.length;
                            $scope.tab=1;
                            $timeout($scope.getConf() , 2000);
                            $scope.getCodes();
                        }
                        else
                        {
                            sweetAlert("Aviso", String(fulfillment), "error");
                        }
            }, function(){
                 //sweetAlert("Error", "Ocurrio un problema", "error");
        });
    }
    $scope.getPastMetrics = function()
    {
        var recieved_data = GetFiles.getPastMetrics($scope.exp,$scope.gpo, $scope.tarea);

        recieved_data.then(
            function(fulfillment){
                $scope.met = fulfillment;
                if($scope.met.data.mensaje != "unregistered")
                {
                  Metricas.setTiempoTotalProduccion($scope.met.data.mensaje['12'][12]);
                  Metricas.setTiempoPruebas($scope.met.data.mensaje['13'][13]);
                  Metricas.setTiempoCorreciones($scope.met.data.mensaje['14'][14]);
                  Metricas.setTiempoTotalProducto($scope.met.data.mensaje['15'][15]);
                  Metricas.setLOCbaseOriginal($scope.met.data.mensaje['16'][16]);
                  Metricas.setLOCagregado($scope.met.data.mensaje['17'][17]);
                  Metricas.setLOCmodificado($scope.met.data.mensaje['18'][18]);
                  Metricas.setLOCsuprimido($scope.met.data.mensaje['19'][19]);
                  Metricas.setLocReutilizado($scope.met.data.mensaje['20'][20]);
                  Metricas.setNuevaReutilizacion($scope.met.data.mensaje['21'][21]);
                  Metricas.setLocNuevoCambiante($scope.met.data.mensaje['22'][22]);
                  Metricas.setLocTotal($scope.met.data.mensaje['23'][23]);
                  Metricas.setDefectos($scope.met.data.mensaje['24'][24]);
                  Metricas.setDefectosCorregidos($scope.met.data.mensaje['25'][25]);
                  $scope.startT = parseInt(Metricas.getTiempoTotalProduccion()); // initTimer
                  $scope.tiempoCorreciones =parseInt(Metricas.getTiempoCorreciones());//tiemrCorrecciones
                }
                else
                {
                  Metricas.setLOCbaseOriginal($scope.LOCbaseActual); 
                }
            }, function(){
                 //sweetAlert("Error", "Ocurrio un problema", "error");
        });
    }
    $scope.Proyeccion = function()
    {
       var recieved_data = GetFiles.getProyeccion($scope.exp,$scope.gpo, $scope.tarea);

        recieved_data.then(
            function(fulfillment){
                $scope.met = fulfillment;
                if($scope.met.data.mensaje != "missing")
                {
                    $scope.defectosAnteriores = $scope.met.data.mensaje['0']['sumDefects']; //getdefectosant
                    $scope.locsanteriores = $scope.met.data.mensaje['0']['sumLocs']; //getlocsanteriores
                }
                else
                {
                    $scope.defectosAnteriores = 0; //getdefectosant
                    $scope.locsanteriores = 0; //getlocsanteriores
                }
             }, function(){
                 //sweetAlert("Error", "Ocurrio un problema", "error");
        });
    }
    $scope.getConf = function()
    {
      var recieved_data = GetFiles.obtainconfig($scope.tarea);

      recieved_data.then(
          function(fulfillment){
              $scope.conf = fulfillment;
              var editor = function() {
                this.theme = $scope.themes[$scope.currentTheme].theme;
                this.mode = 'python';
                this.opacity = 100;
                this.useWrapMode = true;
                this.gutter = true;
                this.splitMode = false;
                this.fontS =16;
                this.snippets = $scope.conf.data.snippets;
                this.bAuto = $scope.conf.data.autocompletado_basico;
                this.lAuto = $scope.conf.data.autocompletado_live;
                this.minL = Infinity;
                this.hihlightAL = $scope.conf.data.highlihtActiveLine;
                this.behaveE = $scope.conf.data.bahaviousEnabled;
                $scope.showToken = $scope.conf.data.infoToken;
                $scope.selectedTheme = $scope.themes[$scope.currentTheme];
                $scope.maximoLineas = $scope.conf.data.maxL;
              };
              $scope.editor = new editor();
          }, function(){
               //sweetAlert("Error", "Ocurrio un problema", "error");
      });
    }
    $scope.getCodes = function()
    {
        var recieved_code = GetFiles.obtaincode($scope.exp,$scope.gpo, $scope.tarea, String($scope.files[$scope.tab - 1]));
                recieved_code.then(
                    function(fulfillment){
                        if(String(fulfillment) != "Ocurrio un problema")
                        {
                            $scope.codigo = fulfillment;

                        }
                        else
                        {
                            sweetAlert("Aviso", String(fulfillment), "error");
                        }

                    });
    }
    $scope.changeTab = function (value)
    {
        $scope.tab = value;
        var recieved_code = GetFiles.obtaincode($scope.exp,$scope.gpo, $scope.tarea, String($scope.files[$scope.tab - 1]));
                recieved_code.then(
                    function(fulfillment){
                            $scope.codigo = fulfillment;
                    });
    }
    $scope.uptdateCodigo = function (code)
    {
        $scope.codigo = code;
    }
    $scope.nuevoArchivo = function(filename)
    {
        var recieved_data = GetFiles.createfile($scope.exp,$scope.gpo, $scope.tarea, filename);
                recieved_data.then(
                    function(fulfillment){
                        if(String(fulfillment) != "Archivo Creado")
                        {
                           sweetAlert("Aviso", String(fulfillment), "error");
                        }
                        else
                        {
                            $scope.getFiles();
                        }

                    });

        $scope.newFileName ='';
    }
    $scope.eliminarArchivo = function(filename)
    {
        var recieved_data = GetFiles.deletefile($scope.exp,$scope.gpo, $scope.tarea, filename);
                recieved_data.then(
                    function(fulfillment){
                        if(String(fulfillment) != "Archivo Borrado")
                        {
                           sweetAlert("Aviso", String(fulfillment), "error");
                        }
                        else
                        {
                            $scope.getFiles();
                        }

                    });
    }
    $scope.editarNombre = function(fileName)
    {
        $scope.tempFilename = fileName;
    }
    $scope.guardarNombre = function(fileName)
    {
       var recieved_data = GetFiles.renamefile($scope.exp,$scope.gpo, $scope.tarea,  $scope.tempFilename,fileName);

                recieved_data.then(
                    function(fulfillment){
                        if(String(fulfillment) != "Nombre de Archivo Cambiado")
                        {
                           sweetAlert("Aviso", String(fulfillment), "error");
                        }
                        else
                        {
                            $scope.getFiles();
                        }

                    });
        $scope.tempFilename ='';
    }
    $scope.showAdmin = function()
    {
        $scope.shwAdmin = false;
    }
    $scope.SalirAdminProyecto = function()
    {
        $scope.shwAdmin = true;
    }
    $scope.compilar = function ()
    {
      $http.get('http://127.0.0.1:8000/api/getpath/'+$scope.exp+'/'+$scope.gpo+'/'+$scope.tarea).then(function(response){
        var resp =response.data.records;
          var path = resp[0].path;
          $http.post("http://127.0.0.1:8000/compiler/analisis", {dir:path}).then(function(data){
            var resp = data.data;
            console.log(resp[0]);
            document.getElementById("analisis").innerHTML = resp[0].analisis;
            document.getElementById("compilar").innerHTML = resp[0].compilacion;
            Metricas.setTiempoPruebas(Metricas.getTiempoPruebas()+ resp[0].tiempo);
            Metricas.setDefectos(Metricas.getDefectos()+ resp[0].errores);
            $scope.habracorrecciones = true;
            if(Metricas.getDefectosS() > 0)
            {
            	var defectoscorr = (Metricas.getDefectosS() - Metricas.getDefectos());
            	if(defectoscorr < Metricas.getDefectos())
            	{
            		Metricas.setDefectosCorregidos(Metricas.getDefectosCorregidos() + defectoscorr);
            	}
            	if(Metricas.getDefectosCorregidos() > 0)
            	{
            		Metricas.setDefectosNoCorregidos(Metricas.getDefectos() - Metricas.getDefectosCorregidos());
            		 if(Metricas.getTiempoTotalProduccion() > 0)
            		{
            			Metricas.setDefectosCorregidosHora(Metricas.getDefectosCorregidos() / (Metricas.getTiempoTotalProduccion() * 60));
            		}
            		if(Metricas.getTiempoCorreciones() > 0)
            		{
            			Metricas.setDefectoscorregidosTotalCorrecciones(Metricas.getDefectosCorregidos() / (Metricas.getTiempoCorreciones() * 60));
            		}
            	}
            }
            Metricas.setDefectosPorTamano(Metricas.getDefectos()/ $scope.LOCbaseActual);
            if(Metricas.getTiempoTotalProduccion() > 0)
            {
            	Metricas.setDefectosPorHora(Metricas.getDefectos() / (Metricas.getTiempoTotalProduccion() * 60));

            }
            Metricas.setDefectosPorHoraEnPruebas(Metricas.getDefectos()/(Metricas.getTiempoPruebas()*60));
            Metricas.setDensidadDeDefectos((1000 * Metricas.getDefectos())/$scope.LOCbaseActual);
		        Metricas.setDefectosS(Metricas.getDefectos());
          }, function(data){
            console.log(data);
          });
      }, function(response){
        var resp = "Ocurrio un problema"
        //sweetAlert("Error", "Ocurrio un problema", "error");
      });
    }

    //timer por minuto
    $interval(function () {
    	if($scope.habracorrecciones == true)
    	{
    		if(Metricas.getLocBaseActualS() == $scope.LOCbaseActual)
    		{
    			$scope.tiempoCorreciones = $scope.tiempoCorreciones + 1;
    			Metricas.setTiempoCorreciones($scope.tiempoCorreciones);
    		}
    		else
    		{
    			$scope.habracorrecciones = false;
    		}
    	}
    	else
    	{
    		$scope.startT = $scope.startT + 1;
    		 Metricas.setTiempoTotalProduccion($scope.startT);
    	}
    	Metricas.setTiempoTotalProducto(Metricas.getTiempoTotalProduccion() + Metricas.getTiempoPruebas() + Metricas.getTiempoCorreciones());   
    }, 60000);

    //funcion que recupera todas las metricas y las guara en la base de datos
    $scope.saveMetricas = function()
  {
    var variablesToSend_aux = [
              parseFloat(Metricas.getContadorOperadoresDistintos()),
							parseFloat(Metricas.getContadorOperandosDistintos()),
							parseFloat(Metricas.getSumaContadorOperadores()),
							parseFloat(Metricas.getSumaContadorOperandos()),
							parseFloat(Metricas.getVocabulario()),
							parseFloat(Metricas.getLongitud()),
							parseFloat(Metricas.getVolumen()),
							parseFloat(Metricas.getVolumenPotencial()),
							parseFloat(Metricas.getNivel()),
							parseFloat(Metricas.getDificultad()),
							parseFloat(Metricas.getInteligencia()),
							parseFloat(Metricas.getEsfuerzo()),
							parseFloat(Metricas.getTiempoTotalProduccion()), //13
							parseFloat(Metricas.getTiempoPruebas()),
							parseFloat(Metricas.getTiempoCorreciones()),
							parseFloat(Metricas.getTiempoTotalProducto()),
							parseFloat(Metricas.getLOCbaseOriginal()),
							parseFloat(Metricas.getLOCagregado()),
							parseFloat(Metricas.getLOCmodificado()),
							parseFloat(Metricas.getLOCsuprimido()),
							parseFloat(Metricas.getLocReutilizado()),
							parseFloat(Metricas.getNuevaReutilizacion()),
							parseFloat(Metricas.getLocNuevoCambiante()),
							parseFloat(Metricas.getLocTotal()), //24
							parseFloat(Metricas.getDefectos()),
							parseFloat(Metricas.getDefectosCorregidos()),
							parseFloat(Metricas.getDefectosNoCorregidos()),
							parseFloat(Metricas.getDefectosPorTamano()),
							parseFloat(Metricas.getDefectosPorHora()),
							parseFloat(Metricas.getDefectosCorregidosHora()),
							parseFloat(Metricas.getDefectosPorHoraEnPruebas()),
							parseFloat(Metricas.getDefectoscorregidosTotalCorrecciones()),
							parseFloat(Metricas.getDensidadDeDefectos()), //33
							parseFloat(Metricas.getProyeccionFuturosDefectos()),
							parseFloat(Metricas.getComplejidadCiclo()),
                          ];
    for(i=0; i<variablesToSend_aux.length;i++){
      if(isNaN(variablesToSend_aux[i])){
        variablesToSend_aux[i] = 0;
      }
    }
    var variablesToSend = [{
                            "met1": variablesToSend_aux[0],
                            "met2": variablesToSend_aux[1],
                            "met3": variablesToSend_aux[2],
                            "met4": variablesToSend_aux[3],
                            "met5": variablesToSend_aux[4],
                            "met6": variablesToSend_aux[5],
                            "met7": variablesToSend_aux[6],
                            "met8": variablesToSend_aux[7],
                            "met9": variablesToSend_aux[8],
                            "met10": variablesToSend_aux[9],
                            "met11": variablesToSend_aux[10],
                            "met12": variablesToSend_aux[11],
                            "met13": variablesToSend_aux[12],
                            "met14": variablesToSend_aux[13],
                            "met15": variablesToSend_aux[14],
                            "met16": variablesToSend_aux[15],
                            "met17": variablesToSend_aux[16],
                            "met18": variablesToSend_aux[17],
                            "met19": variablesToSend_aux[18],
                            "met20": variablesToSend_aux[19],
                            "met21": variablesToSend_aux[20],
                            "met22": variablesToSend_aux[21],
                            "met23": variablesToSend_aux[22],
                            "met24": variablesToSend_aux[23],
                            "met25": variablesToSend_aux[24],
                            "met26": variablesToSend_aux[25],
                            "met27": variablesToSend_aux[26],
                            "met28": variablesToSend_aux[27],
                            "met29": variablesToSend_aux[28],
                            "met30": variablesToSend_aux[29],
                            "met31": variablesToSend_aux[30],
                            "met32": variablesToSend_aux[31],
                            "met33": variablesToSend_aux[32],
                            "met34": variablesToSend_aux[33],
                            "met35": variablesToSend_aux[34],
                          }];
       var saved_data = GetFiles.saveMetricas($scope.exp,$scope.gpo, $scope.tarea, variablesToSend);
                saved_data.then(
                    function(fulfillment)
                    {
                      var resp =fulfillment;
                    }, function(){
                      sweetAlert("Error", "Las metricas no se guardaron correctamente", "error");
                  });
    }
  //sales del ide entreando el proyecto sin posibilidad de poder editarlo
  $scope.entregar = function()
  {
    $scope.saveMetricas();
    $scope.saveCode();
     var recieved_data = GetFiles.entregar($scope.exp,$scope.gpo, $scope.tarea);
                recieved_data.then(
                    function(fulfillment)
                    {
      					     window.location.href = "/tareas/get_tareas_alumno_grupo/"+$scope.gpo; 
                    });
  }
  //sales del ide con la posibilidad de seguir trabajando despues
  $scope.salirIde = function()
  {
    $scope.saveMetricas();
    $scope.saveCode();
     var recieved_data = GetFiles.salirIde($scope.exp,$scope.gpo, $scope.tarea);
                recieved_data.then(
                    function(fulfillment)
                    {
      					     window.location.href = "/tareas/get_tareas_alumno_grupo/"+$scope.gpo; 
                    });
  }
});