/**
 * editorUI: Holds the editor's UI managment
 */
window.editorUI = {
  createFile: function (filename) {
    var $file = $('<li class="file" data-name="'+ filename +'">');
    var $fileLink = $('<div class="filename" data-name="'+ filename +'">' + filename + '</div>');
    var $removeButton = $('<div class="remove-file" data-name="' + filename + '">&times;</div>');
    var $renameButton = $('<div class="rename-file" data-name="' + filename + '">&#x21c6;</div>');

    $file.append($fileLink);
    $file.append($renameButton);
    $file.append($removeButton);
    $('#file-list').append($file);

    texto = "Archivos";
    sidebar_text = $("#texto-archivos");
    sidebar_text.empty();
    sidebar_text.append(texto);
    img = $("#create-file-button");
    source = "/static/ide-files/images/sidebar-add-file.png";

    img.attr("src", "");
    img.attr("src", source);
    img.attr("id", "");
    img.attr("id", "img-archivos");
  },
  selectFile: function (filename) {
    $('#file-list').find('.file-selected').removeClass('file-selected');
    $('#file-list').find('a[data-name="' + filename + '"]').addClass('file-selected');
    $("#file-name").html(filename);
    this.showEditor();
  },
  restoreElements: function ($file) {
    $file.find('input').remove();
    $file.find('.accept-rename').remove();
    $file.find('.cancel-rename').remove();
    $file.find('.filename').show();
    $file.find('.remove-file').show();
    $file.find('.rename-file').show();
  },
  showEditor: function () {
    $('#editor').css({ 'visibility': 'visible' });
  },
  hideEditor: function () {
    $('#editor').css({ 'visibility': 'hidden' });
  },
  startEventListeners: function () {
    $('#sidebar-wrapper').on('click', '#create-file-button', function () {
      var filename = $('#create-file-input').val().trim();
      $('#create-file-input').val(''); // Clear new file input
      var check = editor.addFile(filename);
      if (check) {
        editor.selectFile(filename);
      }
      else{
        texto = "Archivos";
        sidebar_text = $("#texto-archivos");
        sidebar_text.empty();
        sidebar_text.append(texto);
        img = $("#create-file-button");
        source = "/static/ide-files/images/sidebar-add-file.png";

        img.attr("src", "");
        img.attr("src", source);
        img.attr("id", "");
        img.attr("id", "img-archivos");
      }
      REPLIT.editor.focus();
    })
    // Rename file
    .on('click', '.rename-file', function renameFile () {
      var $file = $(this).parent();
      var oldFilename = $file.attr('data-name');
      $file.empty();
      $file.append('<div class="filename" data-name="'+ oldFilename +'"><input placeholder="New filename" value = "' + oldFilename + '" class="bg-dark text-white no-border"></div>');
      $file.append('<div class="accept-rename" data-name= "' + oldFilename + '">&#10003;</div>');
      $file.append('<div class="cancel-rename" data-name="' + oldFilename + '">&times;</div>');
    })
    .on('click', '.accept-rename', function () {
        var $file = $(this).parent();
        var oldFilename = $file.attr('data-name');
        var newFilename = $file.find('.filename').find('input').val();
        var check = editor.renameFile(oldFilename, newFilename);

        if (!check) {
          return;
        }

        $file.attr('data-name', newFilename);
        $file.empty();
        $file.append('<div class="filename" data-name="'+ newFilename +'">' + newFilename + '</div>');
        $file.append('<div class="rename-file" data-name="' + newFilename + '">&#x21c6;</div>');
        $file.append('<div class="remove-file" data-name="' + newFilename + '">&times;</div>');
    })
    .on('click', '.cancel-rename', function () {
        var $file = $(this).parent();
        var filename = $file.attr("data-name");

        $file.empty();
        var $fileLink = $('<div class="filename" data-name="'+ filename +'">' + filename + '</div>');
        var $removeButton = $('<div class="remove-file" data-name="' + filename + '">&times;</div>');
        var $renameButton = $('<div class="rename-file" data-name="' + filename + '">&#x21c6;</div>');

        $file.append($fileLink);
        $file.append($renameButton);
        $file.append($removeButton);
    })
    // Remove file
    .on('click', '.remove-file', function removeFile () {
      var filename = $(this).attr('data-name');
      var check = editor.removeFile(filename);

      if (!check) {
        return;
      }

      $(this).parent().remove();

      filename = $('#sidebar-wrapper').find('li').first().find('.filename').text().trim();
      // Hide the editor when no file available
      if (filename.length == 0) {
        editorUI.hideEditor();
        return;
      }

      editor.selectFile(filename);
    })
    // Select file
    .on('click', '.file', function selectFile () {
      var filename = $(this).attr('data-name');

      editor.selectFile(filename);
    })

    .on('click', '#img-archivos', function showAddFile(){
      texto = $("#texto-archivos");
      img = $("#img-archivos");
    	input = '<input id="create-file-input" type="text" name="input" placeholder="Nuevo Archivo" class="bg-dark text-white no-border">';
      source = "/static/ide-files/images/sidebar-check.png";
    	texto.empty();
      texto.append(input);

      img.attr("src", "");
      img.attr("src", source);
      img.attr("id", "");
      img.attr("id", "create-file-button");
      $("#create-file-input").focus()
    })

    .on('keyup', '#create-file-input', function manageEnter(){
      if (event.keyCode === 13) {
        $("#create-file-button").click();
      }
    });
  }
}
