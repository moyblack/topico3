/**
 * FileManager: Holds the editor's state of each file
 */
function FileManager () {
  this.state = {};
  this.codigosAnalizados = [];
  this.codigofinal = [];
  this.imports = [];
}

/**
 * Filemanager's operations
 */
FileManager.prototype.addFile = function (filename, createSession) {
  if (filename.length == 0) {
    return false;
  }

  if (this.state.hasOwnProperty(filename)) {
    return false;
  }
  newSession = createSession();
  newSession.setOptions({
    mode: "ace/mode/python",
    tabSize: 4,
    useSoftTabs: true
  });
  this.state[filename] = newSession;
  return true;
};

FileManager.prototype.removeFile = function (filename) {
  if (!this.state.hasOwnProperty(filename)) {
    return false;
  }

  delete this.state[filename];

  return true;
};

FileManager.prototype.renameFile = function (oldFilename, newFilename) {
  if (newFilename.length <= 0) {
    return false;
  }

  if (oldFilename == newFilename) {
    return false;
  }

  if (!this.state.hasOwnProperty(oldFilename)) {
    return false;
  }

  this.state[newFilename] = this.state[oldFilename];
  delete this.state[oldFilename];

  return true;
}

FileManager.prototype.selectFile = function (filename) {
  if (!this.state.hasOwnProperty(filename)) {
    return false;
  }

  return this.state[filename];
}

FileManager.prototype.getFiles = function(){
  return this.state;
}

FileManager.prototype.esArchivoDeUsuario = function(name){
  for(names in this.state){
    if(name == names){
      return true;
    }
  }
  return false;
}

FileManager.prototype.getCode = function(){
  this.codigosAnalizados = [];
  for(file in this.state){
    if(!this.estaAnalizado(file.toString())){
      this.agregarCodigo(file);
      this.codigosAnalizados.push(file);
    }
  }
  codeToCompile = "";
  for(var i = 0; i < this.imports.length; i++){
    codeToCompile = codeToCompile + this.imports[i] + "\n";
  }
  for(var i = 0; i < this.codigofinal.length; i++){
    codeToCompile = codeToCompile + this.codigofinal[i] + "\n";
  }
  return codeToCompile;
}

FileManager.prototype.estaAnalizado = function(nombre){
  for(var indiceAnalizados = 0; indiceAnalizados < this.codigosAnalizados.length; indiceAnalizados++){
    if(this.codigosAnalizados[indiceAnalizados].toString() == nombre) return true;
  }
  return false;
}

FileManager.prototype.agregarCodigo = function(nombre){
  this.codigofinal = [];
  this.imports = [];
  var codigo = this.state.valueOf(nombre)[nombre].getValue();
  var lines  = codigo.split('\n');
  for(var i = 0; i < lines.length; i++){
    var line = lines[i].split(' ');
    if(line[0].toString() == "from" || line[0].toString() == "import"){
      if(this.esArchivoDeUsuario(line[1])){
        if(line[0].toString() == "from"){
          this.agregarModulosPorNombre(line[1].toString(), this.getModulesFromImports(lines[i].toString()));
        }
        else{
          console.log("Agregando" + line[1]);
          this.agregarCodigoPorNombre(line[1].toString());
        }
      }
      else {
        this.imports.push(lines[i]);
      }
    }
    else{
      this.codigofinal.push(lines[i]);
    }
  }
}

FileManager.prototype.agregarCodigoPorNombre = function(nombreArchivo){
  var codigo = this.state.valueOf(nombreArchivo)[nombreArchivo].getValue();
  var lines  = codigo.split('\n');
  for(var i = 0; i < lines.length; i++){
    var line = lines[i].split(' ');
    if(line[0].toString() == "from" || line[0].toString() == "import"){
      if(this.esArchivoDeUsuario(line[1])){
        if(line[0].toString() == "from"){
          this.agregarModulosPorNombre(line[1].toString(), this.getModulesFromImports(lines[i].toString()));
        }
        else{
          this.agregarCodigoPorNombre(line[1].toString());
        }
      }
      else {
        this.imports.push(lines[i]);
      }
    }
  }
  var structure = this.getStructure(nombreArchivo);

  this.codigofinal.push("class " + nombreArchivo + ":");//creamos una clase con el nombre de el archivo

  for(var v = 0; v < structure.contextCode.length; v++){
    this.codigofinal.push("    " + structure.contextCode[v]);//Agregamos las lineas de contexto
  }

  for(var v = 0; v < structure.functions.length; v++){
    this.codigofinal.push("    @staticmethod");//Agregamos las lineas de contexto
    for(var i = 0; i < structure.functions[v].codeOfMethod.length; i++){
      this.codigofinal.push("    " + structure.functions[v].codeOfMethod[i]);//Agregamos las lineas de metodos
    }
  }

  for(var v = 0; v < structure.classes.length; v++){
    for(var i = 0; i < structure.classes[v].codeOfClass.length; i++){
      this.codigofinal.push("    " + structure.classes[v].codeOfClass[i]);//Agregamos las lineas de metodos
    }
  }
  this.codigosAnalizados.push(nombreArchivo);
}

FileManager.prototype.agregarModulosPorNombre = function(nameFile, modulos){
  var arrayModulos = modulos.split(',');
  var lastLineAdded = "";
  var codigo = this.state.valueOf(nameFile)[nameFile].getValue();
  var lines  = codigo.split('\n');
  for(var i = 0; i < lines.length; i++){
    var line = lines[i].split(' ');
    if(line[0].toString() == "from" || line[0].toString() == "import"){
      if(this.esArchivoDeUsuario(line[1])){//El import pertenece a los archivos de usuarios
        if(line[0].toString() == "from"){
          console.log("Agregando" + line[1] + line[3]);
          this.agregarModulosPorNombre(line[1].toString(), this.getModulesFromImports(lines[i].toString()));
        }
        else{
          console.log("Agregando" + line[1] );
          this.agregarCodigoPorNombre(line[1].toString());
        }
      }
      else {//el import no pertenece a los archivos del usuario
        this.imports.push(lines[i]);
      }
    }
  }
  var structure = this.getStructure(nameFile);
  for(var v = 0; v < structure.contextCode.length; v++){
    this.codigofinal.push(structure.contextCode[v]);//Agregamos las lineas de contexto
  }

  for(var v = 0; v < structure.functions.length; v++){
    if(arrayModulos.indexOf(structure.functions[v].nameOfMethod) >= 0){//Si el metodo esta en los modulos a importar
      for(var i = 0; i < structure.functions[v].codeOfMethod.length; i++){
        this.codigofinal.push(structure.functions[v].codeOfMethod[i]);//Agregamos las lineas de metodos
      }
    }
  }

  for(var v = 0; v < structure.classes.length; v++){
    if(arrayModulos.indexOf(structure.classes[v].nameOfClass) >= 0){//Si el metodo esta en los modulos a importar
      if(structure.classes[v].inheritance != ""){//La clase hereda de otra clase
        for(var j = 0; j < structure.classes.length; j++){//recorremos otra vez las clases hasta dar con la clase de la que hereda
          if(structure.classes[v].inheritance == structure.classes[j].nameOfClass){
            for(var i = 0; i < structure.classes[j].codeOfClass.length; i++){
              this.codigofinal.push(structure.classes[j].codeOfClass[i]);//Agregamos las lineas de metodos
            }
          }
        }
      }
      for(var i = 0; i < structure.classes[v].codeOfClass.length; i++){
        this.codigofinal.push(structure.classes[v].codeOfClass[i]);//Agregamos las lineas de metodos
      }
    }
  }
  this.codigosAnalizados.push(nameFile);
}

/*
* Funcion para obtener los imports de una lineas
*/
FileManager.prototype.getModulesFromImports = function(linewm){
  var lineWithMod = linewm.split(" ", 3);
  var modules = linewm;
  for(var index = 0; index < lineWithMod.length; index++){
    modules = modules.replace(lineWithMod[index], "");//Quitamos las palabras from,*nombre del archivo* e import
  }
  return modules.replace(/\s/g,'');//Quitamos todos los espacios en blanco de la linea
}

FileManager.prototype.getStructure = function(nameOfFile){
  var fileCode = {
    nameFile: nameOfFile,
    classes:[],
    functions:[],
    contextCode:[]
  }
  var linesOfClass = [];
  var linesOfMethod = [];
  var herencia = "";
  var codigo = this.state.valueOf(nameOfFile)[nameOfFile].getValue();
  var lines  = codigo.split('\n');
  for(var i = 0; i < lines.length; i++){
    var line = lines[i].split(' ');
    if(line[0].toString() == "class"){//la linea es una clase
      linesOfClass = [];
      herencia = "";
      var clase = lines[i].toString().substring(6);//convertimos la linea class clase: -> clase:
      clase = clase.replace(/\s/g,'');//Quitamos todos los espacios en blanco de la linea
      if(clase.match(/\(/) && clase.match(/\)/)){//Extraemos la herencia de la clase
        herencia = clase.substring((clase.match(/\(/)["index"] + 1), clase.match(/\)/)["index"])
      }
      if(clase.match(/:/))clase = clase.substring(0, clase.match(/:/)["index"]); //Quitamos los dos puntos para lograr un nombreClase() o nombreClase
      if(clase.match(/\(/))clase = clase.substring(0, clase.match(/\(/)["index"]);//Quitamos los parentesis en case de tenerlos
      var claseHeredada = line[1].toString().split("(");//clase del tipo Clase(Padre):
      linesOfClass.push(lines[i]);
      var lastLineAdded = lines[i].toString();
      i++;//agregamos la lines *class clase():* y pasamos a la siguiente linea
      while(i < lines.length){//recorresmos las lineas de la clase
        if((lines[i].toString().substr(0, 1) == " ") ||
        (lines[i].toString().substr(0, 1) == "  ") ||
        (lines[i].toString().substr(0, 1) == '\t') ||
        (lines[i].toString().substr(0, 1) == '') ||
        (lines[i].toString().substr(0, 1) == "    ")){//comprobamos que hay tab es decir, es codigo de la clase
          linesOfClass.push(lines[i]);//agregamos lineas en caso de pertenecer a la clase
          var lastLineAdded = lines[i].toString();
          i++;
        }
        else{
          break;//Detenemos el while en caso de que se acaben todas las lineas de la clase
        }
      }
      fileCode.classes.push({nameOfClass:clase, codeOfClass:linesOfClass, inheritance: herencia});
      i--;//volvemos una linea debido a que el for principal va a avanzar a la linea actual para analizarla
    }
    else if(line[0].toString() == "def"){//la linea es una funcion
      linesOfMethod = [];
      var clase = lines[i].toString().substring(4);//convertimos la linea def metodo: -> metodo:
      if(clase.match(/:/))clase = clase.substring(0, clase.match(/:/)["index"]); //Quitamos los dos puntos para lograr un nombreClase() o nombreClase
      if(clase.match(/\(/))clase = clase.substring(0, clase.match(/\(/)["index"]);//Quitamos los parentesis en case de tenerlos
      console.log(clase);
      linesOfMethod.push(lines[i]);
      var lastLineAdded = lines[i].toString();
      i++;//agregamos la lines *class clase():* y pasamos a la siguiente linea
      while(i < lines.length){//recorresmos las lineas de la clase
        if((lines[i].toString().substr(0, 1) == " ") ||
        (lines[i].toString().substr(0, 1) == "  ") ||
        (lines[i].toString().substr(0, 1) == '\t') ||
        (lines[i].toString().substr(0, 1) == '') ||
        (lines[i].toString().substr(0, 1) == "    ")){//comprobamos que hay tab es decir, es codigo de la clase
          linesOfMethod.push(lines[i]);//agregamos lineas en caso de pertenecer a la clase
          var lastLineAdded = lines[i].toString();
          i++;
        }
        else{
          break;//Detenemos el while en caso de que se acaben todas las lineas de la clase
        }
      }
      fileCode.functions.push({nameOfMethod:clase, codeOfMethod:linesOfMethod});
      i--;//volvemos una linea debido a que el for principal va a avanzar a la linea actual para analizarla
    }
    if(lastLineAdded != lines[i].toString())fileCode.contextCode.push(lines[i].toString());//agregamos la linea en caso de que sea diferente de la ultima lina añadida
  }
  return fileCode;
};
