(function() {
  self.JSREPLEngine = (function() {
    function JSREPLEngine(input, output, result, error, sandbox, ready) {
      this.sandbox = sandbox;
      this.virtual_machine = new this.sandbox.QBasic.VirtualMachine({
        print: output,
        input: input,
        result: result,
        error: error
      });
      ready();
    }

    JSREPLEngine.prototype.Eval = function(command) {
      var e;
      try {
        return this.virtual_machine.run(command, (function(_this) {
          return function() {
            if (_this.virtual_machine.stack.length) {
              return _this.virtual_machine.cons.result(_this.virtual_machine.stack.pop().toString());
            } else {
              return _this.virtual_machine.cons.result('');
            }
          };
        })(this));
      } catch (error1) {
        e = error1;
        return this.virtual_machine.cons.error(e.message);
      }
    };

    JSREPLEngine.prototype.EvalSync = function(command) {
      var ret;
      ret = null;
      this.virtual_machine.run(command, (function(_this) {
        return function() {
          if (_this.virtual_machine.stack.length) {
            return ret = _this.virtual_machine.stack.pop();
          }
        };
      })(this));
      return ret;
    };

    JSREPLEngine.prototype.GetNextLineIndent = function(command) {
      var countBlocks, i, lines, parser, tokenizer;
      this.sandbox.QBasic.Program.prototype.createParser();
      parser = this.sandbox.QBasic.Program.parser;
      if (parser.parse(command + '\n') === !null) {
        return false;
      }
      tokenizer = parser.tokenizer;
      lines = (function() {
        var j, len, ref, results;
        ref = command.split('\n');
        results = [];
        for (j = 0, len = ref.length; j < len; j++) {
          i = ref[j];
          results.push(i + '\n');
        }
        return results;
      })();
      countBlocks = function(lines, partial) {
        var first_token, j, len, line, open_blocks, second_token, token, top_block;
        if (partial == null) {
          partial = false;
        }
        open_blocks = [];
        for (j = 0, len = lines.length; j < len; j++) {
          line = lines[j];
          if (parser.parse(line)) {
            continue;
          }
          tokenizer.setText(line);
          token = tokenizer.nextToken(0, 0);
          first_token = token.text;
          token = tokenizer.nextToken(0, token.locus.position + token.text.length);
          second_token = token.text;
          top_block = open_blocks[open_blocks.length - 1];
          switch (first_token) {
            case 'SUB':
            case 'FUNCTION':
            case 'FOR':
            case 'IF':
            case 'SELECT':
            case 'WHILE':
              open_blocks.push(first_token);
              break;
            case 'DO':
              open_blocks.push(second_token === 'WHILE' ? 'DOWHILE' : 'DO');
              break;
            case 'ELSE':
              if (partial && open_blocks.length === 0) {
                open_blocks.push('IF');
              } else if (top_block !== 'IF') {
                return -1;
              }
              break;
            case 'WEND':
              if (top_block === 'WHILE') {
                open_blocks.pop();
              } else {
                return -1;
              }
              break;
            case 'FOR':
              if (top_block === 'NEXT') {
                open_blocks.pop();
              } else {
                return -1;
              }
              break;
            case 'LOOP':
              if (second_token === 'WHILE' || second_token === 'UNTIL') {
                if (top_block === 'DO') {
                  open_blocks.pop();
                } else {
                  return -1;
                }
              } else {
                if (top_block === 'DOWHILE') {
                  open_blocks.pop();
                } else {
                  return -1;
                }
              }
              break;
            case 'END':
              if (top_block === second_token) {
                open_blocks.pop();
              } else {
                return -1;
              }
          }
        }
        return open_blocks.length;
      };
      if (countBlocks(lines) <= 0) {
        return false;
      } else {
        if (countBlocks([lines.slice(-1)[0]], true) > 0) {
          return 1;
        } else {
          return 0;
        }
      }
    };

    return JSREPLEngine;

  })();

}).call(this);
