(function() {
  var SCOPE_OPENERS,
    indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

  SCOPE_OPENERS = ['FOR', 'WHILE', 'UNTIL', 'LOOP', 'IF', 'POST_IF', 'SWITCH', 'WHEN', 'CLASS', 'TRY', 'CATCH', 'FINALLY'];

  self.JSREPLEngine = (function() {
    function JSREPLEngine(input, output, result1, error, sandbox, ready) {
      this.result = result1;
      this.error = error;
      this.sandbox = sandbox;
      this.inspect = this.sandbox.console.inspect;
      this.CoffeeScript = this.sandbox.CoffeeScript;
      this.sandbox.__eval = this.sandbox["eval"];
      ready();
    }

    JSREPLEngine.prototype.Eval = function(command) {
      var compiled, e, result;
      try {
        compiled = this.CoffeeScript.compile(command, {
          globals: true,
          bare: true
        });
        result = this.sandbox.__eval(compiled);
        return this.result(result === void 0 ? '' : this.inspect(result));
      } catch (error) {
        e = error;
        return this.error(e);
      }
    };

    JSREPLEngine.prototype.RawEval = function(command) {
      var compiled, e, result;
      try {
        compiled = this.CoffeeScript.compile(command, {
          globals: true,
          bare: true
        });
        result = this.sandbox.__eval(compiled);
        return this.result(result);
      } catch (error) {
        e = error;
        return this.error(e);
      }
    };

    JSREPLEngine.prototype.GetNextLineIndent = function(command) {
      var all_tokens, e, i, index, j, last_line, last_line_tokens, len, len1, next, ref, scopes, token;
      last_line = command.split('\n').slice(-1)[0];
      if (/([-=]>|[\[\{\(]|\belse)$/.test(last_line)) {
        return 1;
      } else {
        try {
          all_tokens = this.CoffeeScript.tokens(command);
          last_line_tokens = this.CoffeeScript.tokens(last_line);
        } catch (error) {
          e = error;
          return 0;
        }
        try {
          this.CoffeeScript.compile(command);
          if (/^\s+/.test(last_line)) {
            return 0;
          } else {
            for (index = i = 0, len = all_tokens.length; i < len; index = ++i) {
              token = all_tokens[index];
              next = all_tokens[index + 1];
              if (token[0] === 'REGEX' && token[1] === '/(?:)/' && next[0] === 'MATH' && next[1] === '/') {
                return 0;
              }
            }
            return false;
          }
        } catch (error) {
          e = error;
          scopes = 0;
          for (j = 0, len1 = last_line_tokens.length; j < len1; j++) {
            token = last_line_tokens[j];
            if (ref = token[0], indexOf.call(SCOPE_OPENERS, ref) >= 0) {
              scopes++;
            } else if (token.fromThen) {
              scopes--;
            }
          }
          if (scopes > 0) {
            return 1;
          } else {
            return 0;
          }
        }
      }
    };

    return JSREPLEngine;

  })();

}).call(this);
