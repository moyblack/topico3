(function() {
  var BLOCK_OPENERS, TOKENS,
    indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

  self.JSREPLEngine = (function() {
    function JSREPLEngine(unused_input, output, result1, error, sandbox, ready) {
      var bufferError, printOutput;
      this.result = result1;
      this.error = error;
      this.Ruby = sandbox.Ruby;
      sandbox.print = (function() {});
      this.error_buffer = [];
      printOutput = (function(_this) {
        return function(chr) {
          if (chr != null) {
            return output(String.fromCharCode(chr));
          }
        };
      })(this);
      bufferError = (function(_this) {
        return function(chr) {
          if (chr != null) {
            return _this.error_buffer.push(String.fromCharCode(chr));
          }
        };
      })(this);
      this.Ruby.initialize(null, printOutput, bufferError);
      ready();
    }

    JSREPLEngine.prototype.Eval = function(command) {
      var e, result;
      this.error_buffer = [];
      try {
        result = this.Ruby["eval"](command);
        return this.result(this.Ruby.stringify(result));
      } catch (error) {
        e = error;
        if (typeof e !== 'number') {
          return this.error('Internal error: ' + e);
        } else if (this.error_buffer.length) {
          return this.error(this.error_buffer.join(''));
        } else {
          return this.error('Unknown error.');
        }
      }
    };

    JSREPLEngine.prototype.RawEval = function(command) {
      return this.Eval(command);
    };

    JSREPLEngine.prototype.GetNextLineIndent = function(command) {
      var braces, brackets, i, j, last_line_changes, len, len1, levels, line, parens, ref, ref1, token;
      levels = 0;
      parens = 0;
      braces = 0;
      brackets = 0;
      last_line_changes = 0;
      ref = command.split('\n');
      for (i = 0, len = ref.length; i < len; i++) {
        line = ref[i];
        last_line_changes = 0;
        ref1 = line.match(TOKENS) || [];
        for (j = 0, len1 = ref1.length; j < len1; j++) {
          token = ref1[j];
          if (indexOf.call(BLOCK_OPENERS, token) >= 0) {
            levels++;
            last_line_changes++;
          } else if (token === '(') {
            parens++;
            last_line_changes++;
          } else if (token === '{') {
            braces++;
            last_line_changes++;
          } else if (token === '[') {
            brackets++;
            last_line_changes++;
          } else if (token === 'end') {
            levels--;
            last_line_changes--;
          } else if (token === ')') {
            parens--;
            last_line_changes--;
          } else if (token === ']') {
            braces--;
            last_line_changes--;
          } else if (token === '}') {
            brackets--;
            last_line_changes--;
          }
          if (levels < 0 || parens < 0 || braces < 0 || brackets < 0) {
            return false;
          }
        }
      }
      if (levels > 0 || parens > 0 || braces > 0 || brackets > 0) {
        if (last_line_changes > 0) {
          return 1;
        } else {
          return 0;
        }
      } else {
        return false;
      }
    };

    return JSREPLEngine;

  })();

  BLOCK_OPENERS = ["begin", "module", "def", "class", "if", "unless", "case", "for", "while", "until", "do"];

  TOKENS = /\s+|\d+(?:\.\d*)?|"(?:[^"]|\\.)*"|'(?:[^']|\\.)*'|\/(?:[^\/]|\\.)*\/|[-+\/*]|[<>=]=?|:?[a-z@$][\w?!]*|[{}()\[\]]|[^\w\s]+/ig;

}).call(this);
