(function() {
  var RESULT_SIZE;

  RESULT_SIZE = 5;

  self.JSREPLEngine = (function() {
    function JSREPLEngine(input, output, result, error, sandbox, ready) {
      this.sandbox = sandbox;
      this.printed = false;
      this.finished = false;
      this.inputting = false;
      this.lines = 0;
      this.sandbox._init();
      this.sandbox._error = (function(_this) {
        return function(e) {
          _this.finished = true;
          return error(e);
        };
      })(this);
      this.sandbox._print = (function(_this) {
        return function(str) {
          _this.printed = true;
          return output(str);
        };
      })(this);
      this.sandbox._prompt = (function(_this) {
        return function() {
          if (--_this.lines === 0 && !_this.inputting && !_this.finished) {
            return _this.sandbox._finish();
          }
        };
      })(this);
      this.sandbox._input = (function(_this) {
        return function(callback) {
          if (_this.finished) {
            return;
          }
          _this.inputting = true;
          return input(function(result) {
            var chr, i, len;
            for (i = 0, len = result.length; i < len; i++) {
              chr = result[i];
              _this.sandbox.inbuf.push(chr.charCodeAt(0));
            }
            _this.sandbox.inbuf.push(13);
            _this.inputting = false;
            return callback();
          });
        };
      })(this);
      this.sandbox._finish = (function(_this) {
        return function() {
          var top;
          if (_this.finished) {
            return;
          }
          _this.sandbox.inbuf = [];
          top = _this.sandbox._stacktop(RESULT_SIZE + 1);
          if (top.length) {
            if (top.length > RESULT_SIZE) {
              top[0] = '...';
            }
            result(top.join(' '));
          } else {
            if (_this.printed) {
              output('\n');
            }
            result('');
          }
          return _this.finished = true;
        };
      })(this);
      ready();
    }

    JSREPLEngine.prototype.Eval = function(command) {
      var e;
      this.printed = false;
      this.finished = false;
      this.inputting = false;
      this.lines = command.split('\n').length;
      try {
        return this.sandbox._run(command);
      } catch (error1) {
        e = error1;
        this.sandbox._error(e);
      }
    };

    JSREPLEngine.prototype.EvalSync = function(command) {};

    JSREPLEngine.prototype.GetNextLineIndent = function(command) {
      var countParens, parens_in_last_line;
      countParens = (function(_this) {
        return function(str) {
          var depth, i, len, ref, token;
          depth = 0;
          ref = str.split(/\s+/);
          for (i = 0, len = ref.length; i < len; i++) {
            token = ref[i];
            switch (token) {
              case ':':
                ++depth;
                break;
              case ';':
                --depth;
            }
          }
          return depth;
        };
      })(this);
      if (countParens(command) <= 0) {
        return false;
      } else {
        parens_in_last_line = countParens(command.split('\n').slice(-1)[0]);
        if (parens_in_last_line > 0) {
          return 1;
        } else if (parens_in_last_line < 0) {
          return parens_in_last_line;
        } else {
          return 0;
        }
      }
    };

    return JSREPLEngine;

  })();

}).call(this);
