/*
 * BiwaScheme 0.6.2.dev - R6RS Scheme in JavaScript
 *
 * Copyright (c) 2007-2012 Yutaka HARA (http://www.biwascheme.org/)
 * Licensed under the MIT license.
 */

var BiwaScheme = BiwaScheme || {};

BiwaScheme.Version = "0.6.2.dev";
BiwaScheme.GitCommit = "6645c10157e7b5772cef433e97918aa8351785ac";
