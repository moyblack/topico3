(function() {
  var $, ALLOWED_IN_MODAL, ANIMATION_DURATION, FIRST_LOAD, KEY_ESCAPE, PAGES,
    indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

  $ = jQuery;

  ANIMATION_DURATION = 300;

  KEY_ESCAPE = 27;

  FIRST_LOAD = true;

  PAGES = {
    workspace: {
      id: 'content-workspace',
      min_width: 500,
      width: 1000,
      max_width: 3000,
      path: '/'
    },
    languages: {
      id: 'content-languages',
      min_width: 1080,
      width: 1080,
      max_width: 1400,
      path: '/languages'
    },
    examples: {
      id: 'content-examples',
      min_width: 1000,
      width: 1000,
      max_width: 1400,
      path: '/examples'
    },
    help: {
      id: 'content-help',
      min_width: 1000,
      width: 1000,
      max_width: 1400,
      path: '/help'
    },
    about: {
      id: 'content-about',
      min_width: 600,
      max_width: 600,
      width: 600,
      path: '/about'
    },
    DEFAULT: 'workspace'
  };

  ALLOWED_IN_MODAL = ['help', 'about', 'languages'];

  $.extend(REPLIT, {
    PAGES: PAGES,
    modal: false,
    Modal: function(modal) {
      this.modal = modal;
    },
    LoadExamples: function(file, container, callback) {
      var $examples_container;
      $examples_container = $('#examples-' + container);
      $('.example-group').remove();
      return $.get(file, (function(_this) {
        return function(contents) {
          var code, example_element, index, name, raw_examples, results, total;
          raw_examples = contents.split(/\*{60,}/);
          index = 0;
          total = Math.floor(raw_examples.length / 2);
          results = [];
          while (index + 1 < raw_examples.length) {
            name = raw_examples[index].replace(/^\s+|\s+$/g, '');
            code = raw_examples[index + 1].replace(/^\s+|\s+$/g, '');
            example_element = $("<div class=\"example-group example-" + total + "\">\n  <div class=\"example-group-header\">" + name + "</div>\n  <code>" + code + "</code>\n</div>");
            $examples_container.append(example_element);
            example_element.click(function() {
              return callback($('code', this).text());
            });
            results.push(index += 2);
          }
          return results;
        };
      })(this));
    },
    page_stack: [],
    changing_page: false,
    OpenPage: function(page_name, callback) {
      var current_page, done, index, lang_name, new_title, outerWidth, page;
      if (callback == null) {
        callback = $.noop;
      }
      if (this.modal && indexOf.call(ALLOWED_IN_MODAL, page_name) < 0) {
        return;
      }
      page = PAGES[page_name];
      current_page = this.page_stack[this.page_stack.length - 1];
      if (!page || current_page === page_name) {
        return this.changing_page = false;
      } else if (this.changing_page) {
        $('.page').stop(true, true);
        this.$container.stop(true, true);
        this.changing_page = false;
        return this.OpenPage(page_name);
      } else {
        this.changing_page = true;
        lang_name = 'Python';//Asignamos por defecto python
        /*EN estas lineas se cambia el titulo de la pagina*/
        if (page_name !== 'workspace') {
          new_title = page.$elem.find('.content-title').hide().text();
          alert("" + new_title);
          REPLIT.changeTitle(new_title);
        } else {
          REPLIT.changeTitle(lang_name);
        }
        this.min_content_width = this.ISMOBILE ? document.documentElement.clientWidth - 2 * this.RESIZER_WIDTH : page.min_width;
        this.max_content_width = page.max_width;
        if (FIRST_LOAD && page_name === 'workspace') {
          FIRST_LOAD = false;
          page.width = document.documentElement.clientWidth - this.DEFAULT_CONTENT_PADDING;
        }
        this.content_padding = document.documentElement.clientWidth - page.width;
        index = this.page_stack.indexOf(page_name);
        if (index > -1) {
          this.page_stack.splice(index, 1);
        }
        this.page_stack.push(page_name);
        outerWidth = page.width;
        if (page_name !== 'workspace') {
          outerWidth += 2 * this.RESIZER_WIDTH;
        }
        done = (function(_this) {
          return function() {
            _this.changing_page = false;
            page.$elem.focus();
            return callback();
          };
        })(this);
        if (current_page) {
          PAGES[current_page].width = $('.page:visible').width();
          if (current_page === 'workspace') {
            PAGES[current_page].width += 2 * this.RESIZER_WIDTH;
          }
          return PAGES[current_page].$elem.fadeOut(ANIMATION_DURATION, (function(_this) {
            return function() {
              return _this.$container.animate({
                width: outerWidth
              }, ANIMATION_DURATION, function() {
                page.$elem.css({
                  width: page.width,
                  display: 'block',
                  opacity: 0
                });
                _this.OnResize();
                return page.$elem.animate({
                  opacity: 1
                }, ANIMATION_DURATION, done);
              });
            };
          })(this));
        } else {
          this.$container.css({
            width: outerWidth
          });
          page.$elem.css({
            width: page.width,
            display: 'block'
          });
          this.OnResize();
          return done();
        }
      }
    },
    CloseLastPage: function() {
      var closed_page;
      if (this.changing_page) {
        return;
      }
      if (this.page_stack.length <= 1) {
        return Router.navigate('/');
      } else {
        closed_page = this.page_stack[this.page_stack.length - 1];
        Router.navigate(PAGES[this.page_stack[this.page_stack.length - 2]].path);
        return this.page_stack.splice(this.page_stack.indexOf(closed_page), 1);
      }
    }
  });

  $(function() {
    var $body, name, settings;
    for (name in PAGES) {
      settings = PAGES[name];
      settings.$elem = $("#" + settings.id);
      if (REPLIT.ISMOBILE && name !== 'workspace') {
        settings.width = 0;
      }
    }
    $body = $('body');
    $body.delegate('.page-close', 'click', function() {
      return REPLIT.CloseLastPage();
    });
    $(window).keydown(function(e) {
      if (e.which === KEY_ESCAPE && $('.page:visible') !== '#content-workspace') {
        return REPLIT.CloseLastPage();
      }
    });
    return $('#content-languages').keypress(function(e) {
      var letter;
      if (e.shiftKey || e.ctrlKey || e.metaKey) {
        return;
      }
      letter = String.fromCharCode(e.which).toLowerCase();
      return $('#content-languages li a').each(function() {
        if ($('em', $(this)).text().toLowerCase() === letter) {
          this.click();
          return false;
        }
      });
    });
  });

}).call(this);
