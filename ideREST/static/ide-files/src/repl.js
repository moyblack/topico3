(function() {
  var $;
  $ = jQuery;
  $.extend(REPLIT, {
    editor:null,
    font_size:12,
    milesimas:0,
    Init: function() {
      this.jsrepl = new JSREPL({
        input: $.proxy(this.InputCallback, this),
        output: $.proxy(this.OutputCallback, this),
        result: $.proxy(this.ResultCallback, this),
        error: $.proxy(this.ErrorCallback, this),
        progress: $.proxy(this.OnProgress, this),
        timeout: {
          time: 12000,
          callback: (function(_this) {
            return function() {
              var a, code;
              if (a = confirm('The program is taking too long to finish. Do you want to stop it?')) {
                code = _this.editor.getSession().getValue();
                _this.LoadLanguage(_this.current_lang.system_name, function() {
                  return _this.editor.getSession().setValue(code);
                });
              }
              return a;
            };
          })(this)
        }
      });
      this.jqconsole = this.$consoleContainer.jqconsole('', '   ', '.. ');
      this.$console = this.$consoleContainer.find('.jqconsole');
      this.$editor = this.$editorContainer.find('#editor-widget');
      if (!this.ISMOBILE) {
        mode = 'ace/mode/python';
        this.editor = ace.edit('editor-widget');
        this.editor.getSession().setMode("ace/mode/python");
        this.editor.setTheme("ace/theme/ambiance");
        this.editor.setOptions({fontSize: this.font_size + "pt"});
        this.$run.click((function(_this) {
          return function() {
            if (_this.jqconsole.state === 2) {
              _this.jqconsole.AbortPrompt();
              return _this.Evaluate(_this.editor.getSession().getValue());
            }
          };
        })(this));
        this.editor.on('focus', function(event, editor) {
           REPLIT.editor.env.onResize();
         });
        this.editor.commands.addCommand({
          name: 'run',
          bindKey: {
            win: 'Ctrl-Return',
            mac: 'Command-Return',
            sebder: 'editor'
          },
          exec: (function(_this) {
            return function() {
              _this.$run.click();
              return setTimeout((function() {
                return _this.editor.focus();
              }), 0);
            };
          })(this)
        });
        this.editor.commands.addCommand({
          name: 'save',
          bindKey: {
            win: 'Ctrl-S',
            mac: 'Command-S',
            sebder: 'editor'
          },
          exec: (function(_this) {
            return function() {
              return $('#button-save').click();
            };
          })(this)
        });
        this.editor.commands.addCommand({
          name: 'new-file',
          bindKey: {
            win: 'Ctrl-F',
            mac: 'Command-F',
            sebder: 'editor'
          },
          exec: (function(_this) {
            return function(){
              if(REPLIT.menu_desplegado == 0)
              {
                $("#menu-toggle").click();
                $("#img-archivos").click();
              }
              else
              {
                $("#img-archivos").click();
              }
              return;
            }
          })(this)
        });
      }
      this.current_lang = null;
      this.current_lang_name = null;
      return this.inited = true;
    },
    LoadLanguage: function(lang_name, callback) {
      var EditSession, UndoManager, ace_mode, ace_mode_ajax, close, i, index, len, open, ref, ref1, session, textMode;
      if (callback == null) {
        callback = $.noop;
      }
      this.$this.trigger('language_loading', [lang_name]);
      this.current_lang = this.jsrepl.getLangConfig(lang_name.toLowerCase());
      this.current_lang.system_name = lang_name;
      /*if (!this.ISMOBILE) {
        EditSession = ace.createEditSession('');
        UndoManager = ace.UndoManager;
        session = ace.createEditSession('');
        session.setUndoManager(new UndoManager);
        ace_mode = "ace/mode/python";
        if (ace_mode != null) {
          ace_mode_ajax = jQuery.Deferred().resolve();
          session.setMode("ace/mode/python");
          this.editor.setSession(session);
        } else {
          ace_mode_ajax = jQuery.Deferred().resolve();
          session.setMode("ace/mode/python");
          this.editor.setSession(session);
        }
      }*/
      this.jqconsole.Reset();
      ref = this.current_lang.matchings;
      for (index = i = 0, len = ref.length; i < len; index = ++i) {
        ref1 = ref[index], open = ref1[0], close = ref1[1];
        this.jqconsole.RegisterMatching(open, close, 'matching-' + index);
      }
      this.jqconsole.RegisterShortcut('Z', (function(_this) {
        return function() {
          _this.jqconsole.AbortPrompt();
          return _this.StartPrompt();
        };
      })(this));
      this.jqconsole.RegisterShortcut('C', (function(_this) {
        return function() {
          _this.LoadLanguage(_this.current_lang.system_name, null);
          return;
        };
      })(this));
      /*this.jqconsole.RegisterShortcut('L', (function(_this) {
        return function() {
          return _this.OpenPage('languages');
        };
      })(this));
      this.jqconsole.RegisterShortcut('G', (function(_this) {
        return function() {
          return _this.OpenPage('examples');
        };
      })(this));
      this.jqconsole.RegisterShortcut('H', (function(_this) {
        return function() {
          return _this.OpenPage('help');
        };
      })(this));
      this.jqconsole.RegisterShortcut('S', (function(_this) {
        return function() {
          return $('#button-save').click();
        };
      })(this));*/
      this.jqconsole.RegisterShortcut('A', (function(_this) {
        return function() {
          return _this.jqconsole.MoveToStart();
        };
      })(this));
      this.jqconsole.RegisterShortcut('E', (function(_this) {
        return function() {
          return _this.jqconsole.MoveToEnd();
        };
      })(this));
      this.jqconsole.RegisterShortcut('K', (function(_this) {
        return function() {
          return _this.jqconsole.Clear();
        };
      })(this));
      return this.jsrepl.loadLanguage(lang_name.toLowerCase(), (function(_this) {
        return function() {
          return $.when(ace_mode_ajax).then(function() {
            _this.StartPrompt();
            _this.$this.trigger('language_loaded', [lang_name]);
            _this.jqconsole.Write(_this.Languages[lang_name.toLowerCase()].header + '\n');
            _this.editor.focus();
            return callback();
          });
        };
      })(this));
    },
    ResultCallback: function(result) {
      console.log("MS" + this.milesimas);
      if (result) {
        if (result[-1] !== '\n') {
          result = result + '\n';
        }
        this.jqconsole.Write('> ' + result, 'result');
      }
      this.StartPrompt();
      this.$this.trigger('result', [result]);
      this.jqconsole.Write("Tiempo de ejecución: " + this.milesimas + " milesimas \n", 'output');
      return this.$this.trigger('output', ['lel']);
    },
    ErrorCallback: function(error) {
      if (typeof error === 'object') {
        error = error.message;
      }
      if (error[-1] !== '\n') {
        error = error + '\n';
      }
      this.jqconsole.Write(String(error), 'error');
      this.StartPrompt();
      console.log(error);
      return this.$this.trigger('error', [error]);
    },
    OutputCallback: function(output, cls) {
      console.log(output);
      console.log(cls);
      if (output) {
        this.jqconsole.Write(output, cls);
        this.$this.trigger('output', [output]);
        return void 0;
      }
    },
    InputCallback: function(callback) {
      this.jqconsole.Input((function(_this) {
        return function(result) {
          var e;
          try {
            callback(result);
            return _this.$this.trigger('input', [result]);
          } catch (error1) {
            e = error1;
            return _this.ErrorCallback(e);
          }
        };
      })(this));
      this.$this.trigger('input_request', [callback]);
      return void 0;
    },
    Evaluate: function(command) {
      if (command) {
        this.jsrepl["eval"](command);
        console.log("MS" + this.milesimas);
        this.milesimas = 0;
        return this.$this.trigger('eval', [command]);
      } else {
        return this.StartPrompt();
      }
    },
    StartPrompt: function() {
      return this.jqconsole.Prompt(true, $.proxy(this.Evaluate, this), this.jsrepl.checkLineEnd, true);
    }
  });

  $(function() {
    return REPLIT.Init();
  });

}).call(this);
