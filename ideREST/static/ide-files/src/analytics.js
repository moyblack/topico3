(function() {
  var $;

  $ = jQuery;

  $.extend(REPLIT, {
    language_start_time: null,
    InitAnalytics: function() {
      var first_script, ga_script;
      window._gaq.push(['_setAccount', 'UA-25629695-1']);
      window._gaq.push(['_setDomainName', 'none']);
      window._gaq.push(['_setAllowLinker', true]);
      window._gaq.push(['_trackPageview']);
      ga_script = document.createElement('script');
      ga_script.type = 'text/javascript';
      ga_script.async = true;
      ga_script.src = 'http://www.google-analytics.com/ga.js';
      first_script = document.getElementsByTagName('script')[0];
      return first_script.parentNode.insertBefore(ga_script, first_script);
    }
  });

  $(function() {
    window._gaq = window._gaq || [];
    $(window).load(REPLIT.InitAnalytics);
    REPLIT.$this.bind('language_loading', function(_, system_name) {
      REPLIT.language_start_time = Date.now();
      return window._gaq.push(['_trackEvent', 'language', 'loading', system_name]);
    });
    REPLIT.$this.bind('language_loaded', function(_, system_name) {
      var event, time_taken;
      if (REPLIT.language_start_time != null) {
        time_taken = Date.now() - REPLIT.language_start_time;
        event = REPLIT.loading_saved_lang ? 'reloaded' : 'loaded';
        REPLIT.loading_saved_lang = false;
        return window._gaq.push(['_trackEvent', 'language', event, system_name, time_taken]);
      }
    });
    $('#button-languages').click(function() {
      return window._gaq.push(['_trackEvent', 'button', 'languages']);
    });
    $('#button-examples').click(function() {
      return window._gaq.push(['_trackEvent', 'button', 'examples']);
    });
    $('#button-save').click(function() {
      return window._gaq.push(['_trackEvent', 'button', 'save']);
    });
    $('#button-help').click(function() {
      return window._gaq.push(['_trackEvent', 'button', 'help']);
    });
    $('#link-about').click(function() {
      return window._gaq.push(['_trackEvent', 'link', 'about']);
    });
    $('#link-source-code').click(function() {
      return window._gaq.push(['_trackEvent', 'link', 'source']);
    });
    $('#language-about-link').click(function() {
      return window._gaq.push(['_trackEvent', 'link', 'language']);
    });
    return $('#language-engine-link').click(function() {
      return window._gaq.push(['_trackEvent', 'link', 'engine']);
    });
  });

}).call(this);
