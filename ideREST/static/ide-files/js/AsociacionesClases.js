var jsonAsoc = [
  {
    "source": "Telefono",
    "target": "PC",
    "type": "herencia"
  },
  {
    "source": "Camara",
    "target": "Clase",
    "type": "agregacion"
  },
  {
    "source": "PC",
    "target": "Celular",
    "type": "composicion"
  },
  {
    "source": "Camara",
    "target": "PC",
    "type": "composicion"
  },
  {
    "source": "Telefono",
    "target": "Celular",
    "type": "asociacion"
  }
]

var sourceAsoc = [];
var targetAsoc = [];
var typeAsoc = [];
jsonAsoc.forEach(function(element, i){
  sourceAsoc[i] = element.source;
  targetAsoc[i] = element.target;
  typeAsoc[i] = element.type;
  //console.log(sourceAsoc[i]);
  //console.log(targetAsoc[i]);
  //console.log(typeAsoc[i]);

  cy.add([
    {group: "edges",
      data:{
        source: sourceAsoc[i],
        target: targetAsoc[i],
        faveColor: '#5b539e',
        strength: 1
      },
      classes: typeAsoc[i],

    }

  ]);


});
