jsonClases =[
  {
    "nombre": "Telefono",
    "metodos": [
      "_init(self)",
      "telefonear(self)",
      "colgar(self)"
    ],
    "atributos": [
      "tel1",
      "tel2",
      "tel3"
    ]
  },
  {
    "nombre": "Celular",
    "metodos": [
      "_init(self)",
      "llamar(self)",
      "colgar(self)"
    ],
    "atributos": [
      "cel1",
      "cel2",
      "cel3"
    ]
  },
  {
    "nombre": "PC",
    "": "cel4",
    "metodos": [
      "_init(self)",
      "prender(self)"
    ],
    "atributos": [
      "pc1",
      "pc2",
      "pc3"
    ]
  },
  {
    "nombre": "Camara",
    "metodos": [
      "_init(self)",
      "foto(self)",
      ""
    ],
    "atributos": [
      "cam1",
      "cam2",
      "cam3"
    ]
  },
  {
    "nombre": "Clase",
    "metodos": [
      "_init(self)",
      "foto(self)",
      ""
    ],
    "atributos": [
      "cam1",
      "cam2",
      "cam3"
    ]
  }
];

/**
** Generar el canvas para diagramas
** Estilos para los nodos y asociaciones
**
**
**/
var cy = cytoscape({

  container: document.getElementById('cy'), // container to render in

  layout: {
    name: 'cose',
    padding: 10
  },
  minZoom: 1e-50,
  maxZoom: 1e50,
  wheelSensitivity: .2,
  motionBlur: false,
  // estilo de las clases y conexiones
  style: cytoscape.stylesheet()
    .selector('node')
      .css({
        'shape': 'data(faveShape)',
        'width': '50',
        'content': 'data(name)',
        //'text-halign': 'center',
        //'text-valign': 'center',
        'font-size': '15',
        'text-outline-width': 1,
        'text-outline-color': 'data(faveColor)',
        'background-color': 'data(faveColor)',
        'color': '#fff'
      })
    .selector(':selected')
      .css({
        'border-width': 1,
        'border-color': '#333'
      })
    .selector('node.claseP')
      .css({
        'text-valign': 'center'
      })
    .selector('node.atributos').css({
      'text-valign': 'center',
      'font-size': '10',
      'background-color' : '#817ABA',
      'padding-top': '1px',
      'padding-left': '1px',
      'padding-bottom': '1px',
      'padding-right': '1px',
      //'events': 'no',
    })
    .selector('node.metodos').css({
      'text-valign': 'center',
      'font-size': '10',
      'background-color' : '#000000',
      'padding-top': '1px',
      'padding-left': '1px',
      'padding-bottom': '1px',
      'padding-right': '1px',
      //'events': 'no',
    })
    .selector('edge')
      .css({
        'curve-style': 'bezier',
        'opacity': 0.666,
        'width': 'mapData(strength, 1, 100, 2, 6)',
        //'target-arrow-shape': 'triangle',
        //'source-arrow-shape': 'circle',
        'line-color': 'data(faveColor)',
        'source-arrow-color': 'data(faveColor)',
        'target-arrow-color': 'data(faveColor)'
      })
    .selector('edge.herencia')
      .css({
        'line-style': 'solid',
        'target-arrow-shape': 'triangle'
      })
    .selector('edge.agregacion')
      .css({
        'line-style': 'solid',
        'target-arrow-shape': 'diamond',
        'target-arrow-fill':'hollow'
      })
      .selector('edge.composicion')
        .css({
          'line-style': 'solid',
          'target-arrow-shape': 'diamond',
          'target-arrow-fill':'filled'
      })
      .selector('edge.asociacion')
        .css({
          'target-arrow-shape': 'triangle-backcurve',
          'target-arrow-fill':'hollow',
          'source-arrow-shape': 'none',
        })
        .selector('edge.asociacionSimple')
          .css({
            'target-arrow-shape': 'none',
            'source-arrow-shape': 'none',
          }),
});

cy.nodes().nonorphans()
  .on('grab', function(){ this.ungrabify(); })
  .on('free', function(){ this.grabify(); })
;

cy.on('tap', 'node', function(evt){
  var node = evt.target;
  //console.log( 'Click ' + node.id() );
});

cy.resize()
