temas = [
  "ambiance",
  "chaos",
  "chrome",
  "clouds",
  "clouds_midnight",
  "cobalt",
  "crimson_editor",
  "dawn",
  "dracula",
  "dreamweaver",
  "eclipse",
  "github",
  "gob",
  "gruvbox",
  "idle_fingers",
  "iplastic",
  "katzenmilch",
  "kr_theme",
  "kuroir",
  "merbivore",
  "mono_industrial",
  "monokai",
  "pastel_on_dark",
  "solarized_dark",
  "solarized_light",
  "sqlserver",
  "terminal",
  "tomorrow",
  "twilight",
  "xcode"
];

(function(){
  for(i = 0; i < temas.length; i++){
    element = document.createElement("option");
    element.textContent = temas[i];
    element.value = temas[i];
    $("#theme-select").append(element);
  }
}).call(this);

;

$("#theme-select").change(function(){
  tema = $("#theme-select").val();
  tema = "ace/theme/" + tema;
  REPLIT.editor.setTheme(tema);
});

$("#btn-consola").click(function(){
  if(REPLIT.mostrarConsola == 0)
  {
    REPLIT.mostrarConsola = 1;
    REPLIT.mostrarDiagramas = 0;
    REPLIT.split_ratio = 0.5;
    REPLIT.pixeles_botones = 10;
    REPLIT.OnResize();
  }
  else
  {
    REPLIT.mostrarConsola = 0;
    REPLIT.split_ratio = 1;
    REPLIT.pixeles_botones = 20;
    REPLIT.OnResize();
  }
});

$("#btn-diagramas").click(function(){
  if(REPLIT.mostrarDiagramas == 0)
  {
    REPLIT.mostrarDiagramas = 1;
    REPLIT.mostrarConsola = 0;
    REPLIT.split_ratio = 0.5;
    REPLIT.pixeles_botones = 10;
    REPLIT.OnResize();
  }
  else
  {
    REPLIT.mostrarDiagramas = 0;
    REPLIT.split_ratio = 1;
    REPLIT.pixeles_botones = 20;
    REPLIT.OnResize();
  }
  cy.resize();
});

$("#btn-sizeless").click(function(){
  if((REPLIT.font_size - 1) >= 5){
    REPLIT.font_size -= 1;
    REPLIT.editor.setOptions({fontSize: REPLIT.font_size + "pt"});
  }
});
$("#btn-sizeplus").click(function(){
  if((REPLIT.font_size + 1) <= 20){
    REPLIT.font_size += 1;
    REPLIT.editor.setOptions({fontSize: REPLIT.font_size + "pt"});
  }
});

$("#lel").click(function(){
  codigo = editor.fileManager.getCode();
  session = ace.createEditSession('');
  session.setMode("ace/mode/python");
  session.setValue(codigo);
  console.log(session.getValue());
  REPLIT.Evaluate(session.getValue());
});

(async function(){
  setInterval(function(){
      REPLIT.milesimas++;
    }, 1);
}).call(this)
