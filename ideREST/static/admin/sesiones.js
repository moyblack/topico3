// Funcion para control de sesiones
$( document ).ready(function() {
  var url = $(this).attr('title');
  if(localStorage.getItem('tipo') == null){
    // var tipo=localStorage.getItem('tipo');
    window.location.href = '/';
  }
  else{
    // window.location.href = 'login.html';
    var tipo=localStorage.getItem('tipo');
  }
  if(url == 'Inicio Alumno' && tipo == 'maestro'){
    window.location.href = 'inicio_maestro';
  }else if (url == 'Inicio Maestro' && tipo == 'estudiante') {
    window.location.href = 'inicio_alumno';
  }else if (url == 'Inicio Alumno' && tipo == 'admin') {
    window.location.href = 'inicio_admin';
  }
});
