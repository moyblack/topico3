ipServer = 'http://127.0.0.1:8000';
/*
*funcion de logueo con facebook
*/
$(function(){
    $("#btn_show_registro").click(function(){
        $("#info_cont").css("float","right");
        $("#cont_registro").css("display","block");
        $("#cont_login").css("display","none");
        $("#btn_show_registro").css("display","none");
        $("#btn_show_login").css("display","inline");
    });

    $("#btn_show_login").click(function(){
        $("#info_cont").css("float","left");
        $("#cont_registro").css("display","none");
        $("#cont_login").css("display","block");
        $("#btn_show_registro").css("display","inline");
        $("#btn_show_login").css("display","none");
    });
});
/**
* function Registro
* Obtiene los datos del formulario y los manda por ajax
* Si la respuesta es Ok redirecciona a completar-registro
* Si es respuesta incorrecta entonces manda un mensaje de error
*/
$(document).ready(function(){
  $("form#registro").submit(function(){
    var password = $("#reg_pass").val();
    var email = $("#reg_email").val();
    if(password != "" && email != ""){
      if(password != "" && email != ""){
       $.ajax({
          url: ipServer+'/registro/normal',
          type: 'GET',
          datatype : "application/json",
          contentType: "application/json; charset=utf-8",
          data:{password:password, email:email},
          success: function(data){
            if(data.mensaje == "Ok"){
              localStorage.setItem('correoRegistro' , email);
              window.location.href = 'registro/completar_registro_view';
            }else{
              notificacion(data.mensaje);
            }
          },
        });
      }else{
        notificacion("Correo no Valido");
      }
      }else{
      notificacion("Completar Datos");
    }
  });
});
/**
* function Login
* Obtiene los datos del formulario y los manda por ajax
* Si la respuesta es Acceso redirecciona al index
* Si es respuesta incorrecta entonces manda un mensaje de usuario o contraseña incorrectos

$(document).ready(function(){
  $("form#login").submit(function(){
    var password = $("#log_pass").val();
    var email = $("#log_email").val();
    if(password != "" && email != ""){
      if(password != "" && email != ""){
        $.ajax({
          url: ipServer+'/login/normal',
          type: 'GET',
          //data:{password:password, email:email, csrfmiddlewaretoken: token},
          data:{password:password, email:email},
          success: function(data){
            if(data.json.mensaje == "Acceso"){
              localStorage.setItem('idUser', data.json.idUser)
              localStorage.setItem('correoSesion', data.json.correo)
              localStorage.setItem('tipo', data.json.tipo)
              localStorage.setItem('nombre', data.json.nombre)
              localStorage.setItem('apellidos', data.json.apellidos)
              localStorage.setItem('expediente' , data.json.expediente)
              localStorage.setItem('ide' , data.json.tema)
              localStorage.setItem('tipoLogin', 'normal')
            }else{
              notificacion("DATOS INCORRECTOS");
            }
          },
        });
      }else{
        notificacion("CORREO NO VALIDO");
      }
      }else{
      notificacion("COMPLETAR DATOS");
    }
  });
});
*/
/**
* function loginFacebook
* autentifica si esta logeado a facebook
* verifica si ya esta registrado
* si esta registrado, redirigir a la pagina principal
* si no esta registrado, completar el registro
*/
$(document).ready(function(){
  $(".face").click(function(){
    comprobar()
    //si ya esta conectado con su cuenta de facebook
    if(localStorage.getItem('estadoSesionFace') == 'connected')
    {
      verificarRegistroFace()
    }//si no esta conectado con su cuenta de facebook
    else if(localStorage.getItem('estadoSesionFace') =='not-connected')
    {
      conectar()
      if(localStorage.getItem('estadoSesionFace') == 'conection-refused')
      {
        notificacion('INICIO SESION CANCELADO')
      }
      else if (localStorage.getItem('estadoSesionFace') == 'connected') {
        verificarRegistroFace()
      }

    }
  });
});
/**
* function logingmail
* autentifica si esta logeado a gmail
* verifica si ya esta registrado
* si esta registrado, redirigir a la pagina principal
* si no esta registrado, completar el registro
*/
$(document).ready(function(){
  $(".google").click(function(){
    gapi.auth2.getAuthInstance().signIn();
    updateSigninStatus(true)
    //si ya esta conectado con su cuenta de facebook
    if(localStorage.getItem('estadoSesionGmail') == 'connected')
    {
      verificarRegistroGmail()
    }//si no esta conectado con su cuenta de facebook
    else if(localStorage.getItem('estadoSesionGmail') =='not-connected')
    {
      handleSignIn()

      if (localStorage.getItem('estadoSesionGmail') == 'connected') {
        verificarRegistroGmail()
      }

    }
  });
});
/**
* cerrar sesiones
*/
$(document).ready(function(){
  $("#logoutface").click(function(){
      cerrarSesionFacebook()
  });
});
$(document).ready(function(){
  $("#logoutgmail").click(function(){
      handleSignOut()
  });
});
