ipServer = 'http://127.0.0.1:8000';
/*
* imprime formulario para el registro del maestro 
*/
function addMaestro(){
    cont = '<div class="notif_sup"><img src="../static/views/img/cancel.svg" onclick="closeNoti();" alt="">'+
            '<h1>Nuevo Maestro</h1></div><div class="notif_cont">'+
                '<input type="text" id="nombreMaestro" placeholder="Nombre(s)">'+
                '<input type="text" id="apellidoMaestro" placeholder="Apellidos">'+
                '<input type="email" id="emailMaestro" placeholder="Correo Electronico">'+
                '<input type="text" id="expedienteMaestro" placeholder="Expediente">'+
                '<input type="password" id="passwordMaestro" placeholder="Contraseña Provisional">'+
                '<input type="password" id="repasswordMaestro" placeholder="Repetir Contraseña Provisional">'+
            '</div><div class="noti_footer">'+
            '<button onclick="agregarMaestro();">Confirmar</button><h4 onclick="addMaestroCorreo();">¿Maestro Ya Registrado?</h4></div>';
    $("#notificaciones").html(cont);
    desplegarNoti();
}
/*
* imprime el formulario para agregar maestros registrados
*/
function addMaestroCorreo(){
    cont = '<div class="notif_sup"><img src="../static/views/img/cancel.svg" onclick="closeNoti();" alt="">'+
            '<h1>Agregar Maestro Registrado</h1></div><div class="notif_cont">'+
                '<input type="email" id="correoMaestro" placeholder="Email Maestro">'+
            '</div><div class="noti_footer">'+
            '<button onclick="agregarMaestroCorreo();">Confirmar</button><h4 onclick="closeNoti();">cancelar</h4></div>';
    $("#notificaciones").html(cont);
    desplegarNoti();
}
/*
* jala los datos del formulario para agregar maestros registrados
* registra los datos a traves de ajax
*/
function agregarMaestroCorreo(){
    notificacion("PROCESANDO...");
    email = $("#correoMaestro").val();
    if(email != ""){
        $.ajax({
           url: ipServer+'/maestro/registrar_maestro_correo',
           type: 'GET',
           data: {email:email},
           success: function(data){
               if(data == "OK"){
                    closeNoti();
                    maestrosView();
                }else{
                    notificacion("NO EXISTE EL USUARIO");
                }
           },
         });

   }else{
       notificacion("COMPLETAR CAMPOS");
   }
}
/*
* jala los datos del formulario de registro de maestro
* los ingresa en la BD a traves de ajax
*/
function agregarMaestro(){
    nombre = $("#nombreMaestro").val();
    apellido = $("#apellidoMaestro").val();
    email = $("#emailMaestro").val();
    expediente = $("#expedienteMaestro").val();
    password = $("#passwordMaestro").val();
    repassword = $("#repasswordMaestro").val();
    if(nombre != "" && apellido != "" && email != "" && expediente !=  "" && password != "" && repassword != ""){
        if(password == repassword){
            notificacion("PROCESANDO....");
            $.ajax({
               url: ipServer+'/maestro/registrar_maestro',
               type: 'GET',
               data: {nombre:nombre, apellido:apellido, email:email, expediente:expediente, password:password},
               success: function(data){
                   if(data == "OK"){
                        closeNoti();
                        maestrosView();
                    }else{
                        notificacion("USUARIO EXISTENTE");
                    }
               },
             });
       }else{
           notificacion("PASSWORD NO COINCIDE");
       }

   }else{
       notificacion("COMPLETAR CAMPOS");
   }
}
/*
* imprime una lista de los maestros registrados 
*/
function maestrosView(){
    cont = '<div class="superior_maestro_admin"><h1>Maestros</h1>'+
            '<div onclick="addMaestro();"><img src="../static/views/img/add.svg"><h3>Agregar</h3></div></div>';
    $.ajax({
       url: ipServer+'/maestro/obtener_maestros',
       type: 'GET',
       success: function(data){
         for(datos in data){
             cont += '<div class="maestros_grupo"><div class="miembro"><img src="'+ipServer+'/static/perfil/'+data[datos][4]+'/user.jpg"'+
                 'class="fotoPerfil"><h1>'+data[datos][1]+'</h1>'+
            '<h4>'+data[datos][2]+'</h4><h5>'+data[datos][3]+'</h5><img onclick=\'cambiarMaestro(\"'+data[datos][0]+'\")\'" src="../static/views/img/cancel.svg" class="del"></div></div>';
         }
        $("#contenido").css("display","block");
        $("#contenido").html(cont);
       },
     });
}
/*
* registra el cambio de maestro en la BD
*/
function cambiarMaestro(idMaestro){
    $.ajax({
       url: ipServer+'/maestro/cambiar_maestros',
       type: 'GET',
       data: {idMaestro:idMaestro},
       success: function(data){
           if(data == "OK"){
              maestrosView();
            }else{
                notificacion("ERROR");
            }
       },
     });
}
